import React, { useState, useEffect } from 'react';
import { View, Text, Button, StyleSheet, TextInput, TouchableOpacity, ScrollView, AsyncStorage, Picker, Image } from 'react-native';
//import Picker from '../Screen/Components/Picker'
//import { RadioButton } from 'react-native-paper';
import Icon from 'react-native-vector-icons/Ionicons';
import IconIcon from 'react-native-vector-icons/FontAwesome';
import axios from 'axios';

import ImagePicker from 'react-native-image-crop-picker';
import MultiSelect from 'react-native-multiple-select';
import { Dropdown } from 'react-native-material-dropdown';
import Spinner from 'react-native-loading-spinner-overlay';
import { ImageBackground } from 'react-native';
const CreatTrainingBasicInformation = ({ navigation }) => {
    const [token, setToken] = React.useState('');
    const [imagepath, setImagepath] = useState('')
    const [imagepath2, setImagepath2] = useState('')
    const [loading, setLoading] = useState(false);
    const [loading1, setLoading1] = useState(true);
    const [loading2, setLoading2] = useState(false);
    const [loading3, setLoading3] = useState(false);
    const [loading4, setLoading4] = useState();
    const [loading5, setLoading5] = useState(false);
    //  const [checked, setChecked] = React.useState('0');
    //ERROR FIELDS CHECKING
    // const  [TOPICERROR,setTOPICERROR] = useState('');
    // topicValidator()
    // {
    //     if(TEXTFIELDS.topic="")
    //     {
    //         setTEXTFIELDS({TOPICERROR:"Topic is Mandetory"})
    //     }else{
    //         setTEXTFIELDS({TOPICERROR:""})
    //     }
    // }
    const [CATAGORY, setCATAGORY] = React.useState([]);
    //const [CATAGORYERROR, setCATAGORYERROR] = useState('');

    const [KEYWORDS, setKEYWORDS] = React.useState('');
    const [PICKKEYWORDS, setPICKKEYWORDS] = React.useState([]);
    const [selectedItems, setSelectedItems] = useState([]);


    const [SUBCATAGORY, setSUBCATAGORY] = React.useState([]);
    const [LANGUAGE, setLANGUAGE] = React.useState([]);
    const [AGEGROUP, setAGEGROUP] = React.useState([]);


    const [selectOneImg, setSelectOneImg] = useState('');
    // const [resdata4, setData4] = useState([]);
    const singleImageChange = () => {
        //alert("2nd function");
        let imgval = selectOneImg;
        let img = <Image source={require('../../icon/click.png')} style={{ height: 30, width: 30, margin: 5 }} />
        if (imgval === '') {
            //for()
            return (setSelectOneImg('img'))
        } else {
            setSelectOneImg('')
        }
        // return <View>{img}</View>;
        // return setSelectOneImg(img)
    }
    console.log(selectOneImg);
    const [SELECTCATAGORY, setSELECTCATAGORY] = useState({
        selectLabel: '',
    })
    const [SELECTSUBCATAGORY, setSELECTSUBCATAGORY] = useState({
        selectLabel3: '',
    })
    const [SELECTLENGUAGE, setSELECTLENGUAGE] = useState({
        selectLabel4: '',
    })
    const [SELECTAGE, setSELECTAGE] = useState({
        selectLabel5: '',
    })

    const takePhotoFromGallery = () => {
        // alert('take from gallery')
        ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true,


        }).then(image => {

            setImagepath(image.path)
            setLoading5(true)
            //setImagepath2(image.filename)
            console.log(image.filename);
        });
    }


    const onSelectedItemsChange = (selectedItems) => {
        // Set Selected Items
        setSelectedItems(selectedItems);
        setKEYWORDS(selectedItems)
        //  alert(JSON.stringify(KEYWORDS))
        // console.log(LanguageSpoken)
        //  console.log(selectedItems)
    };

    var selectkeywords = [];
    let langua = PICKKEYWORDS.map((myValue, myIndex) => {
        // console.log('myValue: ' + myValue.name)
        selectkeywords.push({
            ["id"]: myValue.id,
            ["name"]: myValue.name
        });
        //  return (
        //      <Picker.Item label={myValue.name} value={myValue.id} key={myIndex} />
        //  )
    });

    const creatTrainingbasicinfo = () => {
        // alert(token);
        setLoading(true);
        // setLoding(false)
        // alert(data.email+data.password)
        axios.defaults.baseURL = 'https://appleskool.com/preview/appleskool_code/api/get-training';
        axios.defaults.headers.post['Content-Type'] =
            'application/json;charset=utf-8';
        axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';
        axios.defaults.headers.post['Access-Control-Allow-Headers'] = '*';
        //alert(global.userToken)
        const bodyParameters = {
            "jsonrpc": "2.0",
            "params": {
                "categories": "",
                "languages": "",
                "Age_group": "",
                "keywords": ""
            }
        };
        axios
            .post('https://appleskool.com/preview/appleskool_code/api/get-training',
                bodyParameters,
                {
                    headers: {
                        Authorization: `Bearer ${token}`,
                        // Authorization: `Bearer  ${userToken}`,global.userToken
                    },
                },
            )
            .then(
                res => {
                    setLoading(false);
                    //alert(JSON.stringify(PICKKEYWORDS));
                    //global.Me = me.data.firstname + me.data.lastname;
                    setCATAGORY(res.data.categories);
                    setLANGUAGE(res.data.languages);
                    setAGEGROUP(res.data.age_group);
                    setPICKKEYWORDS(res.data.keywords);
                    //setSELECTCATAGORY({ selectLabel: res.data.categories });
                    //setSELECTLENGUAGE({ selectLabel4: res.data.languages });
                    setSELECTAGE({ selectLabel5: res.data.age_group });

                },
                error => {
                    setLoading(false);
                    // alert("Create profile info api error")
                },
            );
    };

    let GETCATAGORY = CATAGORY.map((myValue, myIndex) => {
        //  console.log('myValue: ' + myValue.state)
        return (
            <Picker.Item label={myValue.name} value={myValue.id} key={myIndex} />
        )
    });

    let GETSUBCATAGORY = SUBCATAGORY.map((myValue, myIndex) => {
        //  console.log('myValue: ' + myValue.city_name)
        return (
            <Picker.Item label={myValue.name} value={myValue.id} key={myIndex} />
        )
    });

    let GETLANGUAGE = LANGUAGE.map((myValue, myIndex) => {
        //  console.log('myValue: ' + myValue.city_name)
        return (
            <Picker.Item label={myValue.name} value={myValue.id} key={myIndex} />
        )
    });

    let GETAGE = AGEGROUP.map((myValue, myIndex) => {
        //  console.log('myValue: ' + myValue.city_name)
        return (
            <Picker.Item label={myValue.age} value={myValue.id} key={myIndex} />
        )
    });

    const [CATAGORYIMAGE, setCATAGORYIMAGE] = React.useState([]);
    const [Modoftreaining, setModoftreaining] = React.useState([{
        label: 'Offline',
        value: '0',
    }, {
        label: 'Online',
        value: '1',
    }, {
        label: 'Recorded',
        value: '2',
    }]);
    const [SELECTMode, setSELECTMode] = useState({
        selectModeOf: '',
    })
    const [Leveloftraining, setLeveloftraining] = React.useState([{
        label: 'Beginner',
        value: 'B',
    }, {
        label: 'Intermediate',
        value: 'I',
    }, {
        label: 'Expert',
        value: 'E',
    }]);
    const [SELECTLEVELOFTRAINING, setSELECTLEVELOFTRAINING] = useState({
        selectLevelOfTrain: '',
    })

    const [key, setKey] = useState('0'); // const [gender, setGender] = useState(‘male’);
    const [option1, setOption1] = useState(true) // const [maleCheck, setMaleCheck) = useState(true);
    const [femaleCheck, setFemaleCheck] = useState(false);
    const [resdata11, setData11] = useState([]);
    const RadioButton = props => {
        return (
            <TouchableOpacity style={styles.circle} onPress={props.onPress}>
                {props.checked ? (<View style={styles.checkedCircle} />) : (<View />)}
            </TouchableOpacity>
        )
    };
    const radioHandler = () => {
        //logic goes here
        if (femaleCheck) {
            setFemaleCheck(false); //for avoiding multiple checks at a time;
            setKey('0');
            setOption1(true);
            setData11('0')
            setLoading3(false)
            // value=
        } else {
            setOption1(false);
            setFemaleCheck(true)
            setKey('1');
            setData11('1')
            setLoading3(true)
        }
    }
    const radioHandler2 = () => {
        if (option1) {
            setOption1(false);
            setKey('1');
            setFemaleCheck(true);
            setData11('1')
            setLoading3(true)
            // value=
        } else {
            setKey('0');
            setFemaleCheck(false);
            setOption1(true)
            setData11('0')
            setLoading3(true)
        }
    }

    const selectSUBCATAGORY = (id) => {
        // alert(id);
        setSELECTCATAGORY({ selectLabel: id })
        axios.defaults.baseURL = 'https://appleskool.com/preview/appleskool_code/api/get-training-subcategory';
        axios.defaults.headers.post['Content-Type'] =
            'application/json;charset=utf-8';
        axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';
        axios.defaults.headers.post['Access-Control-Allow-Headers'] = '*';
        axios
            .post(
                'https://appleskool.com/preview/appleskool_code/api/get-training-subcategory', {
                "jsonrpc": "2.0",
                "params": {
                    "category_id": id,
                }
            }
            )
            .then((response) => {
                setSUBCATAGORY(response.data.result.sub_category);
                setCATAGORYIMAGE(response.data.result.images)
                //alert(JSON.stringify(CATAGORYIMAGE))

                // console.log(JSON.stringify(CATAGORYIMAGE))
                //navigation.navigate('ScreenCreatProfileSubjectSkill')

            }, (error) => {
                // alert('state ERROR', [
                //     { text: 'Okay' }
                // ]);
                console.log(error);
            });
        //alert("State ERROR")
    }


    const [imageCg, setImageCg] = useState();

    const pushImage = (id) => {
        //catagoryimagesset22.push(id);
        setImageCg(id)
        setLoading4(true)
        //alert(imageCg)
    }

    // let langua23 = CATAGORYIMAGE.map((myValue, myIndex) => {
    //     console.log('myValue: ' + myValue.image)
    //     catagoryimagesset.push({
    //         ["id"]: myValue.id,
    //         ["name"]: myValue.image
    //     });
    // });

    const [TEXTFIELDS, setTEXTFIELDS] = React.useState({
        topic: '',
        TOPICERROR: '',
        TOPICERRORONSUBMIT: '',
        noofparticipants: '',
        NOOFPARTICIPANTSERRORSUBMIT: '',
        fees: '',
        discount: '',
        trainingaddress: '',
        classs: '',
        board: '',
        trainingoutcomes: '',
        trainingrequirements: '',
        description: '',
        oflineaddress: '',
        onlineURL: '',
    });
    const TopictextInputChange = (val) => {
        setTEXTFIELDS({
            ...TEXTFIELDS,
            topic: val,
        });
    }
    const AddressextInputChange = (val) => {
        setTEXTFIELDS({
            ...TEXTFIELDS,
            oflineaddress: val,
        });
    }
    const LinktextInputChange = (val) => {
        setTEXTFIELDS({
            ...TEXTFIELDS,
            onlineURL: val,
        });
    }
    const noofparticipantstextInputChange = (val) => {
        setTEXTFIELDS({
            ...TEXTFIELDS,
            noofparticipants: val,
        });
    }
    const feestextInputChange = (val) => {
        setTEXTFIELDS({
            ...TEXTFIELDS,
            fees: val,
        });
    }
    const discounttextInputChange = (val) => {
        setTEXTFIELDS({
            ...TEXTFIELDS,
            discount: val,
        });
    }
    const trainingaddresstextInputChange = (val) => {
        setTEXTFIELDS({
            ...TEXTFIELDS,
            trainingaddress: val,
        });
    }
    const classtextInputChange = (val) => {
        setTEXTFIELDS({
            ...TEXTFIELDS,
            classs: val,
        });
    }
    const boardtextInputChange = (val) => {
        setTEXTFIELDS({
            ...TEXTFIELDS,
            board: val,
        });
    }
    const trainingoutcomestextInputChange = (val) => {
        setTEXTFIELDS({
            ...TEXTFIELDS,
            trainingoutcomes: val,
        });
    }
    const trainingrequirementstextInputChange = (val) => {
        setTEXTFIELDS({
            ...TEXTFIELDS,
            trainingrequirements: val,
        });
    }
    const descriptiontstextInputChange = (val) => {
        setTEXTFIELDS({
            ...TEXTFIELDS,
            description: val,
        });
    }


    const [Topic, setTopic] = useState('');
    const [Categoryid, setCategoryid] = useState('');
    const [Subcategoryid, setSubcategoryid] = useState('');
    const [Classtype, setClasstype] = useState('');
    const [Noofparticipants, setNoofparticipants] = useState('');
    const [Fees, setFees] = useState('');
    const [Discount, setDiscount] = useState('');
    const [Languageid, setLanguageid] = useState('');
    const [Trainingtype, setTrainingtype] = useState('');
    const [Trainingaddress, setTrainingaddress] = useState([]);
    const [Trainingimage, setTrainingimage] = useState([]);
    const [Categoryimageid, setCategoryimageid] = useState([]);
    const [Traininglevel, setTraininglevel] = useState([]);
    const [Class, setClass] = useState([]);
    const [Board, setBoard] = useState([]);
    const [Recipientstype, setRecipientstype] = useState([]);
    const [Trainingoutcomes, setTrainingoutcomes] = useState('');
    const [Trainingrequirements, setTrainingrequirements] = useState([]);
    const [Description, setDescription] = useState('');
    const [Keywords, setKeywords] = useState('');


    var bodyFormData = new FormData();

    // bodyFormData.append('topic', 'JAVASCREPT22')
    bodyFormData.append('topic', TEXTFIELDS.topic)

    //bodyFormData.append('category_id', '5')
    bodyFormData.append('category_id', SELECTCATAGORY.selectLabel)

    //bodyFormData.append('sub_category_id', '10')
    bodyFormData.append('sub_category_id', SELECTSUBCATAGORY.selectLabel3)

    //bodyFormData.append('class_type', '1')
    bodyFormData.append('class_type', resdata11)

    //bodyFormData.append('no_of_participants', '55')
    bodyFormData.append('no_of_participants', TEXTFIELDS.noofparticipants)

    //bodyFormData.append('fees', '555')
    bodyFormData.append('fees', TEXTFIELDS.fees)

    //bodyFormData.append('discount', '10')
    bodyFormData.append('discount', TEXTFIELDS.discount)

    // bodyFormData.append('language_id', '2')
    bodyFormData.append('language_id', SELECTLENGUAGE.selectLabel4)

    //bodyFormData.append('training_type', '1')
    bodyFormData.append('training_type', SELECTMode.selectModeOf)

    //bodyFormData.append('training_address', 'https://docs.google.com/document/abhijit')
    bodyFormData.append('training_address', TEXTFIELDS.oflineaddress)

    //bodyFormData.append('training_image', '')

    if(imagepath == ''){
        bodyFormData.append('category_image_id', imageCg)

    }else{
        bodyFormData.append('training_image', {
            uri: imagepath,
            name: 'selfie.jpg',
            type: 'image/jpg'
        })
    }
   

    
    //bodyFormData.append('category_image_id', {
    //     uri: imagepath,
    //     name: 'selfie.jpg',
    //     type: 'image/jpg'
    // })

    //bodyFormData.append('training_level', 'B')
    bodyFormData.append('training_level', SELECTLEVELOFTRAINING.selectLevelOfTrain)

    //bodyFormData.append('class', '6')
    bodyFormData.append('class', TEXTFIELDS.classs)

    //bodyFormData.append('board', 'WB')
    bodyFormData.append('board', TEXTFIELDS.board)

    //bodyFormData.append('recipients_type', '3')
    bodyFormData.append('recipients_type', SELECTAGE.selectLabel5)

    //bodyFormData.append('training_outcomes', 'greatgreat')
    bodyFormData.append('training_outcomes', TEXTFIELDS.trainingoutcomes)

    //bodyFormData.append('training_requirements', 'bestbest')
    bodyFormData.append('training_requirements', TEXTFIELDS.trainingrequirements)

    //bodyFormData.append('description', 'biggest offerbiggest offer')
    bodyFormData.append('description', TEXTFIELDS.description)

    //bodyFormData.append('keywords', '[{"key":0,"value":"2"},{"key":1,"value":"7"},{"key":1,"value":"8"}]')
    bodyFormData.append('keywords', JSON.stringify(KEYWORDS))


    const createTrainingbasicinfoupdate = async () => {
        // alert("createTrainingbasicinfoupdate")
        if (TEXTFIELDS.topic == '' || SELECTCATAGORY.selectLabel == ''|| SELECTSUBCATAGORY.selectLabel3 == '' || SELECTMode.selectModeOf == '' || TEXTFIELDS.noofparticipants == '' || TEXTFIELDS.fees =='' || SELECTLENGUAGE.selectLabel4 =='' || SELECTAGE.selectLabel5 =='' || TEXTFIELDS.fees =='' || TEXTFIELDS.discount =='' || TEXTFIELDS.discount =='' || SELECTLEVELOFTRAINING.selectLevelOfTrain =='' || TEXTFIELDS.classs =='' || TEXTFIELDS.board =='' || KEYWORDS =='' || imagepath =='' || TEXTFIELDS.trainingoutcomes =='' ||TEXTFIELDS.trainingrequirements =='' || TEXTFIELDS.description =='') {
            alert('Enter Valid Details')
        } else {
            try {
                // alert(token)
                // alert(imageCg+"--" + TEXTFIELDS.topic + "--" + SELECTMode.selectModeOf + "--" + SELECTLEVELOFTRAINING.selectLevelOfTrain + "--" + SELECTCATAGORY.selectLabel + "--" + SELECTSUBCATAGORY.selectLabel3 + "--" + TEXTFIELDS.noofparticipants + "--" + TEXTFIELDS.fees + "--" + TEXTFIELDS.discount + "--" + SELECTLENGUAGE.selectLabel4 + "--" + TEXTFIELDS.classs + "--" + TEXTFIELDS.board + "--" + TEXTFIELDS.trainingoutcomes + "-" + TEXTFIELDS.trainingrequirements + "" + TEXTFIELDS.description + "" + JSON.stringify(KEYWORDS)+ "" + JSON.stringify(imagepath))
                // console.log(JSON.stringify(imagepath))
                //alert(Authorization)
                await axios({
                    method: 'post',
                    url: 'https://appleskool.com/preview/appleskool_code/api/create-training',
                    data: bodyFormData,
                    headers: {
                        'Content-Type': 'multipart/form-data',
                        'Authorization': 'Bearer' + token
                    }
                }).then(function (response2) {
                    //handle success
                    // console.log(response);
                    console.log(JSON.stringify(response2))
                    navigation.navigate("CreatTrainingSchedule")
                })
                    .catch(function (response2) {
                        //handle error
                        // navigation.navigate("ScreenCreatProfileSubjectSkill")
                        //alert(JSON.stringify(response.data.error))
                        // console.log(response2);
                    });
                let rjx = /^[a-zA-Z]+$/
                let isValid = rjx.test(TEXTFIELDS.topic)
                let isValid2 = rjx.test(TEXTFIELDS.noofparticipants)
                //let isValid3 = rjx.test(CATAGORYERROR)
                if (!isValid) {
                    setTEXTFIELDS({ TOPICERRORONSUBMIT: "Topic Mandetory" })
                } else {
                    setTEXTFIELDS({ TOPICERRORONSUBMIT: "" })
                    }
                // if (!isValid2) {
                //     setTEXTFIELDS({ NOOFPARTICIPANTSERRORSUBMIT: "no if particepants Mandetory" })
                // } else {
                //     setTEXTFIELDS({ NOOFPARTICIPANTSERRORSUBMIT: "" })
                // }
                // if (!isValid3) {
                //     setCATAGORYERROR({ CATAGORYERROR: "Catagory Mandetory" })
                // } else {
                //     setCATAGORYERROR({ CATAGORYERROR: "" })
                // }
                // console.warn(isValid)
            } catch (err2) {
                console.log(err2)
            }
        }
      
    }

    useEffect(() => {
        if (token) {
            creatTrainingbasicinfo()
            selectSUBCATAGORY()
        } else {
            AsyncStorage.getItem('UID123', (err, result) => {
                setToken(result);
                // setTimeout(()=>{
                //    
                // }, 3000);
            })
        }
    }, [token])
    return (
        <View style={styles.containerOut}>
            <Spinner
                //visibility of Overlay Loading Spinner
                visible={loading}
                color={"black"}
                overlayColor={'rgba(255,255,255, 1)'}
                //Text with the Spinner
                textContent={'Loading...'}
            //Text style of the Spinner Text
            //textStyle={styles.spinnerTextStyle}
            />
            <ScrollView>
                <View style={{ marginTop: 30, marginLeft: 20, marginBottom: 10 }}>
                    <Text style={{ fontSize: 22, fontWeight: 'bold' }}>Creat Training</Text>
                </View>
                <View style={{ marginLeft: 20 }}>
                    <Text style={{ fontSize: 16, fontWeight: 'bold' }}>Add information for your training</Text>
                </View>
                <View style={styles.container}>
                    <View style={{ alignItems: 'center' }}><Text style={{ color: "#70d85b", }}>____________________</Text><Text style={{ color: "#000000", paddingTop: 20, fontSize: 9 }}>Basic Information</Text><View style={{ position: 'absolute' }}><View style={{ height: 33, width: 33, backgroundColor: "#70d85b", borderRadius: 50, paddingLeft: 3.7, paddingTop: .6 }}><Icon name="lock-closed-outline" size={25} color="#ffffff" /></View></View></View>

                    <View style={{ alignItems: 'center' }}><Text style={{ color: "#749182", }}>______________________</Text><Text style={{ color: "#000000", paddingTop: 20, fontSize: 9 }}>Schedule</Text><View style={{ backgroundColor: "749182", paddingBottom: 20, position: 'absolute' }}><View style={{ height: 33, width: 33, backgroundColor: "#b5b5b5", borderRadius: 50, paddingLeft: 7.8, paddingTop: 4 }}><IconIcon name="user" size={23} color="#ffffff" backgroundColor="#ffffff" /></View></View></View>

                    <View style={{ alignItems: 'center' }}><Text style={{ color: "#749182", }}>______________________</Text><Text style={{ color: "#000000", paddingTop: 20, fontSize: 9 }}>Get Certified</Text><View style={{ backgroundColor: "749182", paddingBottom: 20, position: 'absolute' }}><View style={{ height: 33, width: 33, backgroundColor: "#b5b5b5", borderRadius: 50, paddingLeft: 7.8, paddingTop: 7 }}><IconIcon name="calendar" size={18} color="#ffffff" /></View></View></View>
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Topic</Text>
                </View>
                <View style={styles.action}>
                    <TextInput
                        placeholder="Topic Name"
                        // onBlur = {() => topicValidator()}
                        style={styles.textInput}
                        autoCapitalize="none"
                        onChangeText={(val) => TopictextInputChange(val)}
                    />
                </View>
                <Text>{TEXTFIELDS.TOPICERROR}</Text>
                <Text style={{ fontSize: 15, color: '#900', marginLeft: 10, marginTop: -10 }}>{TEXTFIELDS.TOPICERRORONSUBMIT}</Text>

                <View style={{ margin: 10 }}>
                    <Text>Category</Text>
                </View>
                <View style={{ borderRadius: 5, borderColor: "#131313", borderWidth: .5, color: "#131313", marginVertical: 10, marginHorizontal: 10 }}>
                    <Picker
                        selectedValue={SELECTCATAGORY.selectLabel}
                        //selectedValue={resdata7}
                        mode="dropdown"
                        onValueChange={(value) => selectSUBCATAGORY(value)}

                    >
                        {GETCATAGORY}
                    </Picker>
                </View>
                {/* <Text style={{ fontSize: 15, color: '#900', marginLeft: 10, marginTop: -10 }}>{CATAGORYERROR}</Text> */}
                <View style={{ margin: 10 }}>
                    <Text>Sub Category</Text>
                </View>
                <View style={{ borderRadius: 5, borderColor: "#131313", borderWidth: .5, color: "#131313", marginVertical: 10, marginHorizontal: 10 }}>
                    <Picker
                        selectedValue={SELECTSUBCATAGORY.selectLabel3}
                        mode="dropdown"
                        onValueChange={(value) => setSELECTSUBCATAGORY({ selectLabel3: value })}
                    >
                        {GETSUBCATAGORY}
                    </Picker>
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Mode of Training</Text>
                </View>
                <View style={{ height: 45, borderRadius: 5, borderColor: "#131313", borderWidth: .5, color: "#131313", marginVertical: 10, marginHorizontal: 10 }}>
                    <Dropdown
                        style={{ paddingStart: 10, height: 25, marginTop: -20 }}
                        baseColor={'#ffffff'}
                        labelFontSize={0}

                        value={'0'}
                        onChangeText={
                            (dropdownitemvalue) => {

                                setSELECTMode({ selectModeOf: dropdownitemvalue })
                                if (dropdownitemvalue === '0') {
                                    //  alert(dropdownitemvalue)
                                    setLoading1(true)
                                    setLoading2(false)
                                    setLoading3(true)
                                } else if (dropdownitemvalue === '1') {
                                    setLoading1(false)
                                    setLoading2(true)
                                    setLoading3(true)
                                }
                                else if (dropdownitemvalue === '2') {
                                    setLoading1(false)
                                    setLoading2(false)
                                    setLoading3(false)
                                }
                            }}
                        // label='Favorite Fruit'
                        data={Modoftreaining}
                    />
                </View>
                {loading1 == true ?
                    <View visible={loading1}>
                        <View style={{ margin: 10 }}>
                            <Text>Address</Text>
                        </View>
                        <View style={styles.action}>
                            <TextInput
                                placeholder="Topic Name"
                                style={styles.textInput}
                                autoCapitalize="none"
                                onChangeText={(val) => AddressextInputChange(val)}
                            />
                        </View>
                    </View> :
                    <View></View>
                }
                {loading2 == true ?
                    <View visible={loading2}>
                        <View style={{ margin: 10 }}>
                            <Text>URL</Text>
                        </View>
                        <View style={styles.action}>
                            <TextInput
                                placeholder="Topic Name"
                                style={styles.textInput}
                                autoCapitalize="none"
                                onChangeText={(val) => AddressextInputChange(val)}
                            />
                        </View>
                    </View> :
                    <View></View>
                }
                {loading3 == true ?
                    <View visible={loading3}>
                        <View style={{ margin: 10 }}>
                            <Text>Traing Type</Text>
                        </View>
                        {/* <View style={{ flexDirection: 'column' }}>
                    <RadioButton.Group style={{ flexDirection: 'column' }}>
                        <View style={{ flexDirection: 'row', paddingLeft: 50 }}>

                            <View style={{ flexDirection: 'row' }}>

                                <RadioButton value="Male" />
                                <Text style={{ paddingTop: 8 }}>One to One</Text>
                            </View>
                            <View style={{ flexDirection: 'row' }}>

                                <RadioButton value="Female" />
                                <Text style={{ paddingTop: 8 }}>One to Many</Text>
                            </View>
                        </View>
                    </RadioButton.Group>
                </View> */}
                        {/* <View style={{ flexDirection: 'row', paddingLeft: 50 }}>
                    <View style={{ flexDirection: 'row' }}>
                        <RadioButton
                            value="0"
                            status={checked === '0' ? 'checked' : 'unchecked'}
                            onPress={() => setChecked('0')}
                        />
                        <Text style={{ paddingTop: 8 }}>One to One</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <RadioButton
                            value="1"
                            status={checked === '1' ? 'checked' : 'unchecked'}
                            onPress={() => setChecked('1')}
                        />
                        <Text style={{ paddingTop: 8 }}>One to Many</Text>
                    </View>
                </View> */}
                        <View style={{ flexDirection: 'row', paddingLeft: 50 }}>
                            <Text>One TO One:</Text>
                            <RadioButton checked={option1} onPress={radioHandler} />
                            <Text>One To Many:</Text>
                            <RadioButton checked={femaleCheck} onPress={radioHandler2} />
                        </View>
                    </View> :
                    <View></View>
                }



                <View style={{ margin: 10 }}>
                    <Text>Training Recipients Type</Text>
                </View>

                <View style={{ borderRadius: 5, borderColor: "#131313", borderWidth: .5, color: "#131313", marginVertical: 10, marginHorizontal: 10 }}>
                    <Picker
                        selectedValue={SELECTAGE.selectLabel5}
                        //selectedValue={resdata7}
                        mode="dropdown"
                        onValueChange={(value) => setSELECTAGE({ selectLabel5: value })}
                    >
                        {GETAGE}
                    </Picker>
                </View>
                {loading3 == true ? <View><View style={{ margin: 10 }}>
                    <Text>No of Perticipants</Text>
                </View>
                <View style={styles.action}>
                    <TextInput
                        //placeholder="Your name"
                        style={styles.textInput}
                        autoCapitalize="none"
                        keyboardType='number-pad'
                        onChangeText={(val) => noofparticipantstextInputChange(val)}
                    />
                </View>
                <Text style={{ fontSize: 15, color: '#900', marginLeft: 10 }}>{TEXTFIELDS.NOOFPARTICIPANTSERRORSUBMIT}</Text></View> : <View></View>}
                
                <View style={{ margin: 10 }}>
                    <Text>Language</Text>
                </View>
                <View style={{ borderRadius: 5, borderColor: "#131313", borderWidth: .5, color: "#131313", marginVertical: 10, marginHorizontal: 10 }}>
                    <Picker
                        selectedValue={SELECTLENGUAGE.selectLabel4}
                        //selectedValue={resdata7}
                        mode="dropdown"
                        onValueChange={(value) => setSELECTLENGUAGE({ selectLabel4: value })}
                    >
                        {GETLANGUAGE}
                    </Picker>
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Training Fees</Text>
                </View>
                <View style={styles.action}>
                    <TextInput
                        // placeholder="Your name"
                        style={styles.textInput}
                        autoCapitalize="none"
                        keyboardType='number-pad'
                        onChangeText={(val) => feestextInputChange(val)}
                    />
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Discount(%)</Text>
                </View>
                <View style={styles.action}>
                    <TextInput
                        placeholder="Discount"
                        style={styles.textInput}
                        autoCapitalize="none"
                        keyboardType='number-pad'
                        onChangeText={(val) => discounttextInputChange(val)}
                    />
                </View>

                <View style={{ margin: 10 }}>
                    <Text>Level of Training</Text>
                </View>
                <View style={{ height: 45, borderRadius: 5, borderColor: "#131313", borderWidth: .5, color: "#131313", marginVertical: 10, marginHorizontal: 10 }}>
                    <Dropdown
                        style={{ paddingStart: 10, height: 25, marginTop: -20 }}
                        baseColor={'#ffffff'}
                        labelFontSize={0}
                        value={'B'}
                        onChangeText={
                            (dropdownitemvalue) => {
                                setSELECTLEVELOFTRAINING({ selectLevelOfTrain: dropdownitemvalue })
                            }}
                        // label='Favorite Fruit'
                        data={Leveloftraining}
                    />
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Class If Any</Text>
                </View>
                <View style={styles.action}>
                    <TextInput
                        placeholder="Class if Any"
                        style={styles.textInput}
                        autoCapitalize="none"
                        onChangeText={(val) => classtextInputChange(val)}
                    />
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Board If Any</Text>
                </View>
                <View style={styles.action}>
                    <TextInput
                        placeholder="Board if Any"
                        style={styles.textInput}
                        autoCapitalize="none"
                        onChangeText={(val) => boardtextInputChange(val)}
                    />
                </View>
                <View style={{ margin: 10 }}>
                    <MultiSelect
                        hideTags
                        items={selectkeywords}
                        uniqueKey="id"
                        onSelectedItemsChange={onSelectedItemsChange}
                        selectedItems={selectedItems}
                        selectText="Key Words"
                        searchInputPlaceholderText="Search Language..."
                        onChangeInput={(text) => (text)}
                        // languageSpokenName
                        tagRemoveIconColor="#CCC"
                        tagBorderColor="#CCC"
                        tagTextColor="#CCC"
                        selectedItemTextColor="#dc2359"
                        selectedItemIconColor="#CCC"
                        itemTextColor="#000000"
                        displayKey="name"
                        searchInputStyle={{ color: '#CCC' }}
                        submitButtonColor="#dc2359"
                        submitButtonText="Submit"
                        hideSubmitButton='true'
                    />
                </View>
                <View style={{ margin: 10 }}>
                    <Text >Select From AppleSkool Image Gallery
                       </Text>
                </View>

                {/* <View >
                    <Image style={{ marginLeft: 10, height: 80, width: 80 }} source={{ uri: 'https://appleskool.com/preview/appleskool_code/storage/app/public/category/' + catagoryimagesset }} />
                </View> */}
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                    <ScrollView showsHorizontalScrollIndicator={false} horizontal>
                        {
                            CATAGORYIMAGE.map((item, key) => (
                                <View key={key} >
                                    <View >
                                        <TouchableOpacity
                                            onPress={() => {
                                                pushImage(item.id)
                                                singleImageChange()
                                            }}
                                        >
                                            <View style={{}}>
                                                <ImageBackground source={{ uri: 'https://appleskool.com/preview/appleskool_code/storage/app/public/category/' + item.image }} style={{ height: 110, width: 110, margin: 5 }} >
                                                    {/* <Text style={{fontSize:20,color:'#ffffff'}}>{selectOneImg}</Text> */}
                                                    {/* {loading4 == true ? <Image visible={loading4} source={require('../../icon/click.png')} style={{ color: '#9000', height: 30, width: 30, margin: 5 }} /> : <Image />} */}
                                                </ImageBackground>
                                                {/* <Text style={{fontSize:20,color:'#900'}}>{selectOneImg}</Text> */}
                                            </View>
                                        </TouchableOpacity>
                                    </View>

                                </View>
                            ))
                        }
                        <Text style={{ fontSize: 20, color: '#ffffff' }}>{selectOneImg}</Text>
                    </ScrollView>
                </View>

                {/* <View>
                    <Text>{selectOneImg}</Text>
                </View> */}
                {/* <View>
                <Button
                onPress={() => singleImageChange()}
                />
                </View> */}
                {/* <TouchableOpacity style={{
                    height: 35, justifyContent: 'center', alignItems: 'center', width: '33%', padding: 1, backgroundColor: '#d51c6d', borderRadius: 5
                }}
                    //onPress={() => navigation.navigate("CreatTrainingSchedule")}
                    onPress={() => singleImageChange()}
                >
                    <Text style={{ fontSize: 13, color: 'white', fontWeight: 'bold' }}>CHANGE IMG</Text>
                </TouchableOpacity> */}

                <View style={{ margin: 10 }}>
                    <Text style={{ fontWeight: 'bold', fontSize: 15, textAlign: 'center' }}>
                        Or
                    </Text>
                </View>
                <View style={{ margin: 10 }}>
                    <Text>
                        Upload Your Own Images</Text>
                </View>
                {loading5 == true ?<View >
                    <TouchableOpacity>
                        <View style={{}}>
                            <Image source={{ uri: imagepath }} style={{ height: 110, width: 110, margin: 5 }} />
                            {loading4 == false ? <Image visible={loading4} source={require('../../icon/click.png')} style={{ color: '#9000', height: 30, width: 30, margin: 5 }} /> : <Image />}
                        </View>
                    </TouchableOpacity>
                </View> : <View></View> }
                
                <View style={{ margin: 10 }}>
                    <TouchableOpacity style={{
                        height: 45, width: '100%',
                        padding: 5,
                        backgroundColor: '#e6a000', borderRadius: 5
                    }}
                        onPress={takePhotoFromGallery}
                    // value={IMage}
                    // value={imagepath2}
                    >
                        <View style={{ flexDirection: 'row', alignItems: 'stretch', justifyContent: 'space-between' }}>
                            <Text style={{ fontSize: 13, color: 'white', alignItems: 'flex-start', justifyContent: 'flex-start', paddingTop: 8 }}>
                                Upload your photos
                                {/* {IMage} */}
                            </Text>
                            <View style={{ paddingTop: 6, paddingRight: 10 }}>
                                <IconIcon name="upload" size={23} color="#ffffff" />
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Training Outcomes</Text>
                </View>
                <View style={{ margin: 10 }}>
                    <View style={{ height: 140, borderWidth: 1, borderColor: '000000', borderRadius: 5 }}>
                        <Text style={{ fontSize: 17, color: '000000', padding: 5, fontWeight: 'bold' }}>B I</Text>
                        <View style={styles.lineStyle} />
                        <TextInput
                            placeholder="Write hear..."
                            style={styles.textInput}
                            autoCapitalize="none"
                            multiline={true}
                            onChangeText={(val) => trainingoutcomestextInputChange(val)}
                        />
                    </View>
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Requirment for taking training</Text>
                </View>
                <View style={{ margin: 10 }}>
                    <View style={{ height: 140, borderWidth: 1, borderColor: '000000', borderRadius: 5 }}>
                        <Text style={{ fontSize: 17, color: '000000', padding: 5, fontWeight: 'bold' }}>B I</Text>
                        <View style={styles.lineStyle} />
                        <TextInput
                            placeholder="Write hear..."
                            style={styles.textInput}
                            autoCapitalize="none"
                            multiline={true}
                            onChangeText={(val) => trainingrequirementstextInputChange(val)}
                        />
                    </View>
                </View>
                {/* <View>
                <TextInput
                            placeholder="Write hear..."
                            style={{height:90}}
                            autoCapitalize="none"
                            multiline={true}
                        // onChangeText={(val) => textFirstName(val)}
                        />
                </View> */}
                <View style={{ margin: 10 }}>
                    <Text>Description(Optional)</Text>
                </View>
                <View style={{ margin: 10 }}>
                    <View style={{ height: 140, borderWidth: 1, borderColor: '000000', borderRadius: 5 }}>
                        <Text style={{ fontSize: 17, color: '000000', padding: 5, fontWeight: 'bold' }}>B I</Text>
                        <View style={styles.lineStyle} />
                        <TextInput
                            placeholder="Write hear..."
                            style={styles.textInput}
                            autoCapitalize="none"
                            multiline={true}
                            onChangeText={(val) => descriptiontstextInputChange(val)}
                        />
                    </View>
                </View>
                <View style={styles.lineStyle2} />
                <View style={{ margin: 10, }}>
                    <View style={{ flexDirection: 'row', paddingTop: 30, alignItems: 'flex-end', justifyContent: 'flex-end', marginRight: 5 }}>
                        <TouchableOpacity style={{
                            height: 35, justifyContent: 'center', alignItems: 'center', width: '25%',
                            padding: 1,
                            backgroundColor: '#fffff', borderRadius: 5, marginLeft: 30, borderRadius: 5, borderWidth: 1, borderColor: '#7300ff', marginRight: 15, marginLeft: 30
                        }} onPress={() => navigation.navigate('HomeScreen')}>
                            <Text style={{ fontSize: 13, color: '#7300ff', fontWeight: 'bold' }}>Discard</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{
                            height: 35, justifyContent: 'center', alignItems: 'center', width: '33%', padding: 1, backgroundColor: '#d51c6d', borderRadius: 5
                        }}
                            //onPress={() => navigation.navigate("CreatTrainingSchedule")}
                            onPress={() => createTrainingbasicinfoupdate()}
                        >
                            <Text style={{ fontSize: 13, color: 'white', fontWeight: 'bold' }}>Save & Continue</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        </View>
    );
};

export default CreatTrainingBasicInformation;

const styles = StyleSheet.create({
    containerOut: {
        flex: 1,
        backgroundColor: '#ffffff'
        // alignItems: 'center',
        // justifyContent: 'center'
    },
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 10,
        paddingBottom: 15,
    },
    action: {
        //flexDirection: 'row',
        marginRight: 10,
        marginLeft: 10,
        borderWidth: 0.6,
        borderRadius: 5,
        borderColor: '#505050',
        //padding: 5
    },
    textInput: {
        //flex: 1,
        //marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        color: '#05375a',
        height: 40
    },
    lineStyle: {
        borderWidth: .4,
        borderColor: 'black',
        //margin: 15,
        //marginTop: 25
    },
    lineStyle2: {
        borderWidth: .4,
        borderColor: 'black',
        margin: 15,
        marginTop: 25
    },
    circle: {
        height: 20,
        width: 20,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#ACACAC',
        alignItems: 'center', // To center the checked circle…
        justifyContent: 'center',
        marginHorizontal: 10
    },
    checkedCircle: {
        width: 14,
        height: 14,
        borderRadius: 7,
        backgroundColor: '#fa0562' // You can set it default or with yours one…
    },
});

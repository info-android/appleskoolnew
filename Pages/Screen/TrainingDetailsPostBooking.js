import React, { Component, useState, useEffect } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  TextInput,
  TouchableOpacity,
  TouchableHighlight, AsyncStorage,
  Image,
  ScrollView,
  SafeAreaView
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { Card } from "react-native-elements";
import Spinner from 'react-native-loading-spinner-overlay';
import axios from 'axios';
const TrainingDetailsPostBookingPage = ({ navigation, route }) => {
  const [loading, setLoading] = React.useState(false);
  const [token, setToken] = React.useState('');
  const [training, setTraining] = useState('');
  const [results, setresults] = useState('');
  const [lang, setLang] = useState('');
  const [ageGrp, setAge] = useState('');
  const [descr, setDescr] = useState('');
  const [related, setRelated] = useState([]);
  const [subcategory, setSubcategory] = React.useState([
    { id: 0, name: 'Rabin Manna', data: '1,456', img: require('../../icon/instructor2.png') },
    { id: 1, name: 'Rabin Manna', data: '10 Years', img: require('../../icon/instructor2.png') },
    { id: 2, name: 'Rabin Manna', data: '40 Hours', img: require('../../icon/instructor2.png') },
    { id: 3, name: 'Rabin Manna', data: '18', img: require('../../icon/instructor2.png') },
    { id: 4, name: 'Rabin Manna', data: '10', img: require('../../icon/instructor2.png') },
  ])
  const [topsearch, setTopsearch] = React.useState([
    { id: 0, name: 'What you will learn' }, { id: 1, name: 'Requirements' }, { id: 2, name: 'Course Schedule' }, { id: 3, name: 'Video' }, { id: 4, name: 'Documents' },
    { id: 5, name: 'Reference-link' },
  ])
  const { Data } = route.params
  //alert("data>>>>>>>>" + JSON.stringify(Data))
  useEffect(() => {
    if (token) {
      HomeApi()
    } else {
      AsyncStorage.getItem('UID123', (err, result) => {
        setToken(result);
        // setTimeout(()=>{
        //    
        // }, 3000);
      })
    }
  }, [token])

  const regex = /(<([^>]+)>)/ig;

  const HomeApi = () => {

    setLoading(true);
    // setLoding(false)
    // alert(data.email+data.password)
    axios.defaults.baseURL = 'https://appleskool.com/preview/appleskool_code/api';
    axios.defaults.headers.post['Content-Type'] =
      'application/json;charset=utf-8';
    axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';
    axios.defaults.headers.post['Access-Control-Allow-Headers'] = '*';
    //alert(global.userToken)
    const bodyParameters = {
    };
    axios
      .post(
        'https://appleskool.com/preview/appleskool_code/api/training/details', {
        "jsonrpc": "2.0",
        "params": {
          "id": Data,
        }
      }
      )
      .then(


        res => {
          setLoading(false);
          console.log(JSON.stringify(res.data.related_trainings));
          //global.Me = me.data.firstname + me.data.lastname;
          setTraining(res.data.training)
          setresults(res.data.training.user)
          setAge(res.data.training.age_grup)
          setLang(res.data.training.language)
          setRelated(res.data.related_trainings)
          setDescr(res.data.training.description.replace(regex, ''))

        },
        error => {
          setLoading(false);
         // alert("Something went wrong. Please try again")
        },
      );
  };
  return (
    <View style={styles.container}>
      <ScrollView>
        <Spinner
          //visibility of Overlay Loading Spinner
          visible={loading}
          color={"black"}
          overlayColor={'rgba(255,255,255, 1)'}
          //Text with the Spinner
          textContent={'Loading...'}
        //Text style of the Spinner Text
        //textStyle={styles.spinnerTextStyle}
        />
        <View style={{ height: 320, backgroundColor: '#f4efe9', position: 'relative', }}>
          <View style={{ height: 320, margin: 14, }}>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ flexDirection: 'column', marginLeft: 0, flexWrap: "wrap", width: "100%" }}>
                <Text style={{ color: '#41165e', fontFamily: 'roboto-bold', fontWeight: 'bold', fontSize: 19 }}>
                  {training.topic}
                </Text>
                <Text style={{ marginTop: 8, fontSize: 13, fontFamily: 'roboto-regular', height: 100, width: '95%',textAlign:'justify' }} >{descr}</Text>
                <Text style={{ color: '#41165e', marginTop: 10, fontSize: 14, fontFamily: 'roboto-regular', width: '90%', }} >{results.fname + " " + results.lname}</Text>
              </View>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text style={{ color: '#d73c5f', height: 35, justifyContent: 'center', textAlign: 'center', fontFamily: 'roboto-medium', marginTop: 15, marginLeft: 15 }}>Share this Training</Text>
              <Image source={require('../../icon/facebook-min.png')} size={17} style={styles.inputIconFace} />
              <Image source={require('../../icon/twitter-min.png')} size={17} style={styles.inputIconFace} />
              <Image source={require('../../icon/google-plus-min.png')} size={17} style={styles.inputIconFace} />
            </View>
            <View style={{ flexWrap: "wrap", flex: 1, flexDirection: 'row', marginTop: 5, marginLeft: 10, marginRight: 10 }} >
              <TouchableOpacity style={{
                height: 38, justifyContent: 'center', alignItems: 'center', width: '55%',
                padding: 5, marginRight: 5,
                backgroundColor: '#5a287d', borderRadius: 20
              }} onPress=
                {() => navigation.navigate('InstructorProfile', { Data: results.id })}>
                <Text style={{ fontSize: 13, color: 'white', fontWeight: 'bold' }}>Open Training Instructor</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{
                height: 38, justifyContent: 'center', alignItems: 'center', width: '35%',
                padding: 5, marginLeft: 5,
                backgroundColor: '#5a287d', borderRadius: 20
              }}>
                <Text style={{ fontSize: 13, color: 'white', fontWeight: 'bold' }}>Add To Favorite</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={{ marginTop: -20 }}>
          <View style={{
            borderWidth: 0,
            borderRedius: 0,
            borderColor: '#ddd',
            borderBottomWidth: 0,
            shadowColor: '#ff00b8',
            shadowOffset: { width: 10, height: 20 },
            shadowOpacity: 0.4,
            shadowRadius: 2,
            elevation: 7,
            marginLeft: 25,
            marginRight: 25,
            margintop: 20,
            marginBottom: 20,
            padding: 10,
            backgroundColor: "#faf9f9",
          }} >
            <View style={{ flex: 1, flexDirection: 'column', flexWrap: 'wrap', width: '100%' }}>
              <Image source={require('../../icon/banner1.png')} style={{ width: '100%', height: 130, marginTop: 5, }} />
              <View style={{ flexDirection: 'row', }}>
                <Text style={{ fontSize: 13, paddingLeft: 4, paddingRight: 2, marginTop: 8, marginLeft: 6, fontWeight: 'bold' }}>{'\u20B9'}{training.price}</Text>
                <Text style={{ color: '#988f8f', textDecorationLine: 'line-through', marginLeft: 4, marginTop: 8, textDecorationStyle: 'solid', fontSize: 13, }}>
                  {'\u20B9'}{training.fees}
                </Text>
                <Text style={{ fontSize: 13, paddingLeft: 4, paddingRight: 2, marginTop: 8, marginLeft: 6, fontWeight: 'bold' }}>{training.discount}% off</Text>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 15, marginBottom: 10 }}>
                <TouchableOpacity style={{ width: 60, height: 22, backgroundColor: '#c3dcb4', marginLeft: 8, alignItems: 'center', borderRadius: 5, borderColor: "#c3dcb4", borderWidth: 1, paddingLeft: 2 }}>
                  <Text style={{ color: 'black', textAlign: 'center', fontSize: 11, marginTop: 2 }}>{training.training_type == 2 ? "Recorded" : "Online"}  </Text>

                </TouchableOpacity>
                <TouchableOpacity style={{ width: 70, height: 22, backgroundColor: '#f1cd83', marginLeft: 8, alignItems: 'center', borderRadius: 5, borderColor: "#f1cd83", borderWidth: 1, paddingLeft: 2 }}>
                  <Text style={{ color: 'black', textAlign: 'center', fontSize: 11, marginTop: 2 }}>One to One</Text>
                </TouchableOpacity>
              </View>
              <View style={{ alignSelf: 'center' }}>
                <TouchableOpacity style={[styles.buttonContainer, styles.loginButton]} >
                  <Text style={styles.loginText}>Book This Training</Text>
                </TouchableOpacity>
                {/* <TouchableOpacity style={[styles.buttonContainer, styles.loginButton2]} >
                  <Text style={styles.loginText2}>Add To Cart</Text>
                </TouchableOpacity> */}
              </View>
              <Text style={{ width: '100%', alignItems: 'center', textAlign: 'center', fontSize: 11, paddingLeft: 4, paddingRight: 2, marginTop: 5, fontWeight: 'normal' }}>Money back gurantee on cancellation</Text>
              <Text style={{ width: '100%', alignItems: 'center', textAlign: 'left', fontSize: 16, paddingLeft: 4, paddingRight: 2, marginLeft: 10, marginTop: 25, fontWeight: 'bold', fontFamily: 'roboto-bold' }}>Training Description:</Text>
              <View style={{ flexDirection: 'column', marginLeft: 15, marginTop: 10 }}>
                <View style={{ flexDirection: 'row', marginTop: 5, marginBottom: 7 }}>
                  <Image source={require('../../icon/group-min.png')} style={{ width: 18, height: 18, marginRight: 25 }} />
                  <Text style={styles.loginText3}>Training In {lang.name}</Text>
                </View>
                <View style={{ flexDirection: 'row', marginTop: 5, marginBottom: 5 }}>
                  <Image source={require('../../icon/group-min.png')} style={{ width: 18, height: 18, marginRight: 25 }} />
                  <Text style={styles.loginText3}>Maximum Participants 20</Text>
                </View>
                <View style={{ flexDirection: 'row', marginTop: 5, marginBottom: 5 }}>
                  <Image source={require('../../icon/clock-min.png')} style={{ width: 18, height: 18, marginRight: 25 }} />
                  <Text style={styles.loginText3}>Total duration 10 days</Text>
                </View>
                <View style={{ flexDirection: 'row', marginTop: 5, marginBottom: 5 }}>
                  <Image source={require('../../icon/calendar-min.png')} style={{ width: 18, height: 18, marginRight: 25 }} />
                  <Text style={styles.loginText3}>10 Total Sessions</Text>
                </View>
                <View style={{ flexDirection: 'row', marginTop: 5, marginBottom: 5 }}>
                  <Image source={require('../../icon/calendar-min.png')} style={{ width: 18, height: 18, marginRight: 25 }} />
                  <Text style={styles.loginText3}>Wedn 11th sep to Thurs 26th Sep</Text>
                </View>
                <View style={{ flexDirection: 'row', marginTop: 5, marginBottom: 5 }}>
                  <Image source={require('../../icon/calendar-min.png')} style={{ width: 18, height: 18, marginRight: 25 }} />
                  <Text style={styles.loginText3}>Age group of: {ageGrp.age}</Text>
                </View>
                <View style={{ flexDirection: 'row', marginTop: 5, marginBottom: 5 }}>
                  <Image source={require('../../icon/check-min.png')} style={{ width: 18, height: 18, marginRight: 25 }} />
                  <Text style={styles.loginText3}>{training.training_level == 'B' ? "Beginer level training" : "Expert level training"}</Text>
                </View>
                <View style={{ flexDirection: 'row', flexWrap: 'wrap', marginTop: 5, marginBottom: 5 }}>
                  <Image source={require('../../icon/calendar-min.png')} style={{ width: 18, height: 18, marginRight: 25 }} />
                  <Text style={styles.loginText3}>Training using {training.training_type == 2 ? "Recorded" : "Online"}</Text>
                </View>
              </View>
            </View>
          </View>
        </View>
        <View style={{ marginTop: 10 }}>
          <View style={{
            borderWidth: 0,
            borderRedius: 0,
            borderColor: '#ddd',
            borderBottomWidth: 0,
            shadowColor: '#ff00b8',
            shadowOffset: { width: 10, height: 20 },
            shadowOpacity: 0.4,
            shadowRadius: 2,
            elevation: 7,
            marginLeft: 25,
            marginRight: 25,
            margintop: 20,
            marginBottom: 20,
            padding: 10,
            //paddingRight: 10,
            backgroundColor: "#faf9f9",
          }} >
            <View style={{ flexWrap: "wrap", flexDirection: 'column' }} numberOfLines={2}>
              {topsearch.map((item, key) => (
                <View key={key} style={{ flexWrap: "wrap", flexDirection: 'column' }}>
                  <TouchableOpacity style={styles.buttonContainerSub2}  >
                    <Text style={styles.loginText4}>{item.name}</Text>
                    <Image source={require('../../icon/right-arrow-angle.png')} style={{ paddingTop: 10, width: 15, height: 15, }} />

                  </TouchableOpacity>
                  <Text style={{ fontStyle: 'italic', fontWeight: 'bold', color: '#e9e9e9', width: '70%', marginLeft: 20, }}>________________________________________________</Text>
                </View>
              ))}
            </View>
          </View>
        </View>

        <View style={{ flexDirection: 'column', width: '85%', marginLeft: 20, marginRight: 15, marginTop: 40 }}>
          <Text style={{ fontWeight: 'bold', fontSize: 18, marginLeft: 5, fontFamily: 'roboto-bold' }}>Related Training</Text>
          <Image source={require('../../icon/color-border.png')} style={{ width: '25%', marginLeft: 2, height: 4, marginTop: 5, }} />
          <Text style={{ fontSize: 14, marginLeft: 5, marginTop: 10, fontFamily: 'roboto-light', marginBottom: 15 }}></Text>
          <View style={{ flexWrap: "wrap", flex: 1, flexDirection: 'column', marginRight: 5 }} numberOfLines={2}>
            {related.map((item, key) => (
              <View style={{ flexDirection: 'row', marginTop: 5, marginBottom: 5 }} key={key}>
                <Image source={{ uri: 'https://appleskool.com/preview/appleskool_code/storage/app/public/training/images/' + item.image }} style={{ width: 95, height: 95, borderColor: '#f4efe900', borderWidth: 1, borderRadius: 10 }} />
                <View style={{ marginLeft: 10, flex: 1 }}>
                  <Text style={{ fontSize: 13, fontWeight: 'bold', fontFamily: 'roboto-bold', flexWrap: 'wrap', flexDirection: 'row', }}  >{item.topic}</Text>
                  <Text style={{ fontSize: 13, fontWeight: 'bold', fontFamily: 'roboto-bold', }}>Prakash Sarkar</Text>
                  <View style={{ width: 100, borderRadius: 2, flexDirection: 'row', alignItems: 'center' }}>
                    <View style={{ backgroundColor: '#e6a000', alignItems: 'center', marginLeft: 2, marginTop: 5, width: 40, height: 17, borderRadius: 2, flexDirection: 'row', }}>
                      <Icon name='md-star' size={11} style={{ marginLeft: 4, color: 'white', marginRight: 5 }} />
                      <Text style={{ color: 'white', fontSize: 9 }}>{item.avg_rating}</Text>
                    </View>
                    <Text style={{ paddingLeft: 10, marginTop: 2, fontSize: 12 }}>Rating({item.total_rating})</Text>

                  </View>
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>

                    <Text style={{ fontSize: 13, paddingLeft: 4, paddingRight: 2, fontWeight: 'bold', fontFamily: 'roboto-bold', marginTop: 5, }}>{'\u20B9'} {item.price}</Text>
                    <Text style={{ color: '#988f8f', textDecorationLine: 'line-through', marginTop: 7, paddingLeft: 10, textDecorationStyle: 'solid', fontSize: 13, }}>
                      {'\u20B9'} {item.fees}
                    </Text>

                  </View>
                </View>
              </View>

            ))}
          </View>


        </View>
        {/* <View style={{ flexDirection: 'column', marginBottom: 20, width: '95%', marginLeft: 15, marginRight: 15, marginTop: 5 }}>
            <Text style={{ fontWeight: 'bold', fontSize: 15, marginLeft: 5, fontFamily: 'roboto-bold' }}>Write your question/answer</Text>
            <TextInput style={styles.inputs}
            />
            <TouchableOpacity style={{
              height: 38, justifyContent: 'center', alignItems: 'center', width: '35%',
              padding: 5, marginLeft: 5,
              backgroundColor: '#d73c5e', borderRadius: 5
            }}>
              <Text style={{ fontSize: 13, color: 'white', fontWeight: 'bold' }}>Post Comment</Text>
            </TouchableOpacity>
          </View> */}
      </ScrollView>
    </View >
  );
};
export default TrainingDetailsPostBookingPage;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    alignContent: 'center'
  },
  inputs: {
    height: 85,
    marginLeft: 5,
    borderColor: '#3c3b37',
    width: '93%',
    marginTop: 10,
    borderWidth: 1,
    borderRadius: 5,
    marginBottom: 15,
    fontFamily: 'roboto-regular',
  },
  buttonContainerSub: {
    height: 35,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 5,
    width: '31%',
    borderRadius: 30,
    padding: 5,
    margin: 3,
    borderColor: 'white',
    borderWidth: 1,
    backgroundColor: 'white'
  },
  buttonContainerSub2: {
    // height: 30,
    flexDirection: 'row',
    marginLeft: 20,
    marginTop: 15,
    //marginRight:20,
    //width: '100%',
    //borderRadius: 30,
    // padding: 5,
    //borderColor: 'white',
    //borderWidth: 1,
    //backgroundColor: '#8f00ff'
  },
  inputIcon: {
    width: 12,
    height: 12,
    marginLeft: 1,

    marginRight: 5,
    justifyContent: 'center'
  },
  loginText: {
    fontSize: 10
  },
  inputIconFace: {
    width: 25,
    height: 25,
    marginLeft: 10,

    justifyContent: 'center'
  },
  circle: {
    height: 100,
    width: 100,
    borderRadius: 50,
    position: 'absolute',
    top: 150,
    right: 10,
    elevation: 10,
    backgroundColor: 'yellow',
  },
  buttonContainer: {
    height: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 5,

    marginLeft: -9,
    width: 300,
    marginTop: 10,
    borderRadius: 3,
    textAlign: 'center',
  },
  loginButton: {
    backgroundColor: "#d73c5e",
    alignItems: 'center'
  },
  loginButton2: {
    backgroundColor: "#ffffff",
    alignItems: 'center',
    borderColor: '#a78bba', borderWidth: 1, borderRadius: 5,
  },
  loginText: {
    color: 'white',
    fontWeight: 'bold',
    fontFamily: 'roboto-bold'
  },
  loginText2: {
    color: '#630db4',
    fontWeight: 'bold',
    fontFamily: 'roboto-bold'
  },
  loginText3: {

    color: '#3b3c37',
    fontWeight: 'bold',
    fontFamily: 'roboto-bold'
  },
  loginText4: {
    alignItems: 'center',
    // marginTop: 12,
    height: 30,
    width: '88%',
    backgroundColor: "#faf9f9",
    fontWeight: 'bold',
    fontFamily: 'roboto-bold'
  },
});
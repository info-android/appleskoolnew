import React, { useState, useEffect } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    Text,
    Image,
    View,
    ScrollView,
    Dimensions, TextInput,
    TouchableOpacity, Picker, FlatList,AsyncStorage
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import IconIcon from 'react-native-vector-icons/FontAwesome';
//import Picker from '../Components/Picker'
import axios from 'axios';
import MultiSelect from 'react-native-multiple-select';
//import { Multiselect } from 'multiselect-react-dropdown';
//import MultiSelect from "react-multi-select-component";
import Spinner from 'react-native-loading-spinner-overlay';
//import MultiSelectInFlatList from '../MultiSelectInFlatList'

const ScreenCreatProfileSubjectSkill = ({ navigation }) => {
    const [token, setToken] = React.useState('');

    const [data, setData] = useState({
        skillName: '',
    });
    const [category, setCategory] = useState([]);
    const [Sub_category, setSub_category] = useState([]);
    const [master_skills, setMaster_skills] = useState([]);
    const [loading, setLoading] = useState(false);
    const [selectedItems, setSelectedItems] = useState([]);

    //const [resdata, setdata] = useState([]);
    const [data2, setSelectLabel] = useState({
        selectLabel: '',
    })
    const [data3, setSelectLabel3] = useState({
        selectLabel3: '',
    })
    const onSelectedItemsChange = (selectedItems) => {
        // Set Selected Items
        setSelectedItems(selectedItems);
    };
    //     const data = [
    //         {country: 'india' ,id:1},
    //         {country: 'uae' ,id:2},
    //         {country: 'kanada' ,id:3},
    //         {country: 'austr' ,id:4},
    //         {country: 'england' ,id:5},
    //     ]
    //    const [options] = useState(data);
    // const options = [
    //     { label: "Grapes 🍇", value: "grapes" },
    //     { label: "Mango 🥭", value: "mango" },
    //     { label: "Strawberry 🍓", value: "strawberry", disabled: true },
    //     { label: "Watermelon 🍉", value: "watermelon" },
    //     { label: "Pear 🍐", value: "pear" },
    //     { label: "Apple 🍎", value: "apple" },
    //     { label: "Tangerine 🍊", value: "tangerine" },
    //     { label: "Pineapple 🍍", value: "pineapple" },
    //     { label: "Peach 🍑", value: "peach" },
    // ];
    const items = [
        // name key is must. It is to show the text in front
        { id: 1, name: 'angellist' },
        { id: 2, name: 'codepen' },
        { id: 3, name: 'envelope' },
        { id: 4, name: 'etsy' },
        { id: 5, name: 'facebook' },
        { id: 6, name: 'foursquare' },
        { id: 7, name: 'github-alt' },
        { id: 8, name: 'github' },
        { id: 9, name: 'gitlab' },
        { id: 10, name: 'instagram' },
    ];
    const [selected, setSelected] = useState([]);

    const subjectSkillUserToken = () => {

        setLoading(true);
        //alert(token);

        // setLoding(false)
        // alert(data.email+data.password)
        axios.defaults.baseURL = 'https://appleskool.com/preview/appleskool_code/api/subject-skills';
        axios.defaults.headers.post['Content-Type'] =
            'application/json;charset=utf-8';
        axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';
        axios.defaults.headers.post['Access-Control-Allow-Headers'] = '*';
        //alert(global.userToken)
        const bodyParameters = {
        };
        axios
            .post('https://appleskool.com/preview/appleskool_code/api/subject-skills',
                bodyParameters,
                {
                    headers: {
                        Authorization: `Bearer ${token}`,
                        // Authorization: `Bearer  ${userToken}`,global.userToken
                    },
                },
            )
            .then(
                res => {
                    setLoading(false);

                    //alert(JSON.stringify(res.data.categories[0].name));
                    //global.Me = me.data.firstname + me.data.lastname;
                    setCategory(res.data.categories);
                    setMaster_skills(res.data.master_skills);

                    // setdata(res.data.teacher_skills.skills.skill_name);
                    //alert(JSON.stringify(res.data.master_skills[3].skill_name))
                   // alert(JSON.stringify(res.data.categories[1].name))
                },
                error => {
                    setLoading(false);
                 //   alert("Catagory api error")
                },
            );
    };
    let _catagory = category.map((myValue, myIndex) => {
       // console.log('myValue: ' + myValue.name)
        return (
            <Picker.Item label={myValue.name} value={myValue.id} key={myIndex} />
        )
    });
    let _subcatagory = Sub_category.map((myValue, myIndex) => {
       // console.log('myValue: ' + myValue.name)
        return (
            <Picker.Item label={myValue.name} value={myValue.id} key={myIndex} />
        )
    });
    var lang2 = [];
    let languag = master_skills.map((myValue, myIndex) => {
       // console.log('myValue: ' + myValue.skill_name)
        lang2.push({
            ["id"]: myValue.id,
            ["name"]: myValue.skill_name
        });
    });
    // var lang = [];
    // let language2 = resdata5.map((myValue, myIndex) => {
    //     console.log('myValue: ' + myValue.name)
    //     lang.push({
    //         ["id"]: myValue.id,
    //         ["name"]: myValue.name
    //     });
    // });
    const findSubCatagory = () => {
        // setLoding(false)
        // alert(data.email+data.password)
        axios.defaults.baseURL = 'https://appleskool.com/preview/appleskool_code/api/get-subcategory';
        axios.defaults.headers.post['Content-Type'] =
            'application/json;charset=utf-8';
        axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';
        axios.defaults.headers.post['Access-Control-Allow-Headers'] = '*';
        //alert(global.userToken)
        // const headers = {
        //     Authorization: `Bearer ${token}`,
        // };
        axios
            .post('https://appleskool.com/preview/appleskool_code/api/get-subcategory',
                 // headers,
                {
                    "jsonrpc": "2.0",
                    "params": {
                        "category_id": data2.selectLabel,
                    }
                }
            )
            .then(
                res => {
                    //alert(JSON.stringify(res.data.result.sub_category[0].name));
                    //global.Me = me.data.firstname + me.data.lastname;
                    setSub_category(res.data.result.sub_category);

                    // setdata(res.data.teacher_skills.skills.skill_name);
                    //alert(JSON.stringify(Sub_category))
                },
                error => {
                   // alert("Sub-catagory api error")
                },
            );
        // alert("function call")

    };
    // var bodyFormData = new FormData();

    // bodyFormData.append('fname','sir')
    // bodyFormData.append('lname','sirsirsxsxsx')
    // bodyFormData.append('phone_no','1231561231')
    // bodyFormData.append('phone_country_code','+91')
    // bodyFormData.append('street','assasas')
    // bodyFormData.append('state','1')
    // bodyFormData.append('city','1')
    // bodyFormData.append('pin','700056')
    // bodyFormData.append('country_id','4')
    // bodyFormData.append('gender','male')
    // bodyFormData.append('about_me_shrt','sassas')
    // bodyFormData.append('experience','5')
  
    // bodyFormData.append('language[]','1')
    // bodyFormData.append('about_me','sasasas')
    // bodyFormData.append('dob','01-05-1997')
    // bodyFormData.append('degree[]','asas')
    // bodyFormData.append('institute_name[]','sasassa')
    // bodyFormData.append('year[]','2013')

    // const creatprofilebasicinfoupdate = async() => {
    //     try{
    //        alert(token)
           
    //        //alert(Authorization)
    //      await axios({
    //            method: 'post',
    //            url: 'https://appleskool.com/preview/appleskool_code/api/update-profile',
    //            data: bodyFormData,
    //            headers: {"Content-Type": "application/x-www-form-urlencoded",
    //            'Authorization': 'Bearer'+ token
    //         }
    //            }).then(function (response) {
    //             //handle success
    //             console.log(response);
    //            alert(JSON.stringify(response.data.success.message))
    //         })
    //         .catch(function (response) {
    //             //handle error
    //            // alert(JSON.stringify(response.data.error))
    //             console.log(response);
    //         });
              
    //     }catch(err){
    //         console.log(err)
    //     }   
         
               
    //    }
    const submitSubjectSkillPage = () => {
        // setLoding(false)
        // alert(data.email+data.password)
        axios.defaults.baseURL = 'https://appleskool.com/preview/appleskool_code/api/subject-skill-save';
        axios.defaults.headers.post['Content-Type'] =
            'application/json;charset=utf-8';
        axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';
        axios.defaults.headers.post['Access-Control-Allow-Headers'] = '*';
        //alert(global.userToken)
        // const headers = {
        // };

        const bodyParameters = {
                "jsonrpc": "2.0",
                "params": {
                        "category_id": "3",
                        "sub_category_id": "6",
                        "skill_list": {
                            "1":"sdsd"
                        },
                        "search_skill":
                        {
                            "1":""
                            },
                        "skill_name": {
                            "1":data.skillName
                        }
                }            
        };
        axios
            .post('https://appleskool.com/preview/appleskool_code/api/subject-skill-save',
            bodyParameters,
                {
                    headers: {
                        Authorization: `Bearer ${token}`,
                        // Authorization: `Bearer  ${userToken}`,global.userToken
                    },
                },
            )
            .then(
                res => {
                    //alert(JSON.stringify(res.data.result.sub_category[0].name));
                    //global.Me = me.data.firstname + me.data.lastname;
                    setSub_category(res.data.result.sub_category);
                    
                    // setdata(res.data.teacher_skills.skills.skill_name);
                  //  alert("SUCCESS")
                    navigation.navigate('ScreenCreatProfileAccountingInformation')
                },
                error => {
                 //   alert("submit Subject Skill Page api error")
                },
            );
        // alert("function call")

    };
    const skillText = (val) => {
        if (val.length !== 0) {
            setData({
                ...data,
                skillName: val.split(" "),
                isValiedphonenumber: true,
                // alert(val)
            });
        } else {
            setData({
                ...data,
                isValiedphonenumber: null,
            });
        }
    }

    let listOfSubjectSkill = selectedItems.map((myValue, myIndex) => {
        //  console.log('myValue: ' + myValue.city_name)
        return (
            <View style={{ marginTop: 15, flexDirection: 'row' }}>
                    <TouchableOpacity style={{
                        height: 35, justifyContent: 'center', alignItems: 'center', width: '15%',
                        padding: 1,
                        backgroundColor: '#fffff', borderRadius: 5, marginLeft: 30, borderRadius: 5, borderWidth: 1, borderColor: '#ff52d1'
                    }}>
                        <Text style={{ fontSize: 13, color: '#130002', fontWeight: 'bold' }}>{myValue.skill_name}</Text>
                    </TouchableOpacity>
            
            </View>
        )
    });

    useEffect(() => {
      if(token){
        subjectSkillUserToken()
        findSubCatagory()
      }else{
        AsyncStorage.getItem('UID123', (err, result) => {
            setToken(result);
            // setTimeout(()=>{
            //    
            // }, 3000);
        })
      }
       // submitSubjectSkillPage()
    }, [token])
    // let _catagory = catagory.map((myValue, myIndex) => {
    //     console.log('myValue: ' + myValue.city_name)
    //     return (
    //         <Picker.Item label={myValue.city_name} value={myValue.id} key={myIndex} />
    //     )
    // });
    return (
        <View style={styles.container}>
            <Spinner
                //visibility of Overlay Loading Spinner
                visible={loading}
                color={"black"}
                overlayColor={'rgba(255,255,255, 1)'}
                //Text with the Spinner
                textContent={'Loading...'}
            //Text style of the Spinner Text
            //textStyle={styles.spinnerTextStyle}
            />
            <ScrollView>
                <View style={{ padding: 15, }}>
                    <Text style={{ fontSize: 22, fontWeight: '700' }}>Creat Instructor's Profile</Text>
                </View>
                {/* <View>
                    <FlatList
                        data={category}
                        renderItem={({ item }) => (
                            <>
                                <TouchableOpacity>
                                    <Text style={{ fontSize: 17 }}>{item.id}.{item.name}</Text>
                                </TouchableOpacity>
                            </>
                        )}
                    />
                </View>
                <View>
                    <FlatList
                        data={Sub_category}
                        renderItem={({ item }) => (
                            <>
                                <TouchableOpacity>
                                    <Text style={{ fontSize: 17 }}>{item.id}.{item.name}</Text>
                                </TouchableOpacity>
                            </>
                        )}
                    />
                </View> */}
                {/* <View>
                    <Multiselect options={options} displayValue="country"/>
                </View> */}
                {/* <View>
                    <MultiSelect
                        options={options}
                        value={selected}
                        onChange={setSelected}
                        labelledBy={"Select"}
                    />
                </View> */}
                <View style={styles.container}>
                    <View style={{ alignItems: 'center' }}><Text style={{ color: "#82b46d", }}>________________</Text><Text style={{ color: "#000000", paddingTop: 20, fontSize: 9 }}>Basic Information</Text><View style={{ position: 'absolute' }}><View style={{ height: 33, width: 33, backgroundColor: "#82b46d", borderRadius: 50, paddingLeft: 3.7, paddingTop: .6 }}><Icon name="lock-closed-outline" size={25} color="#ffffff" /></View></View></View>

                    <View style={{ alignItems: 'center' }}><Text style={{ color: "#5a00b5", }}>________________</Text><Text style={{ color: "#000000", paddingTop: 20, fontSize: 9 }}>Subject/Skills</Text><View style={{ backgroundColor: "749182", paddingBottom: 20, position: 'absolute' }}><View style={{ height: 33, width: 33, backgroundColor: "#5a00b5", borderRadius: 50, paddingLeft: 4, paddingTop: 3 }}><Icon name="cog" size={25} color="#ffffff" backgroundColor="#ffffff" /></View></View></View>

                    <View style={{ alignItems: 'center' }}><Text style={{ color: "#749182", }}>________________</Text><Text style={{ color: "#000000", paddingTop: 20, fontSize: 9 }}>Accounting Information</Text><View style={{ backgroundColor: "749182", paddingBottom: 20, position: 'absolute' }}><View style={{ height: 33, width: 33, backgroundColor: "#b5b5b5", borderRadius: 50, paddingLeft: 4.5, paddingTop: 3.8 }}><Icon name="calendar" size={22} color="#ffffff" /></View></View></View>

                    <View style={{ alignItems: 'center' }}><Text style={{ color: "#749182", }}>________________</Text><Text style={{ color: "#000000", paddingTop: 20, fontSize: 9 }}>Get Certified</Text><View style={{
                        backgroundColor: "2cd383", paddingBottom: 20,
                        position: 'absolute'
                    }}><View style={{ height: 33, width: 33, backgroundColor: "#b5b5b5", borderRadius: 50, paddingLeft: 6.5, paddingTop: 6.5 }}><IconIcon name="check" size={20} color="#ffffff" /></View></View></View>
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Category</Text>
                </View>
                <View style={{ borderRadius: 5, borderColor: "#131313", borderWidth: .5, color: "#131313", marginVertical: 10, marginHorizontal: 10 }}>
                    <Picker
                        selectedValue={data2.selectLabel}
                        mode="dropdown"
                        onValueChange={(value) => setSelectLabel({ selectLabel: value })}
                        onPress={findSubCatagory()}
                    >
                        {_catagory}
                    </Picker>
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Sub Category</Text>
                </View>
                <View style={{ borderRadius: 5, borderColor: "#131313", borderWidth: .5, color: "#131313", marginVertical: 10, marginHorizontal: 10 }}>
                    <Picker
                        selectedValue={data3.selectLabel3}
                        mode="dropdown"
                        onValueChange={(value) => setSelectLabel3({ selectLabel: value })}
                    // onPress={findSubCatagory()}
                    >
                        {_subcatagory}
                    </Picker>
                </View>
                <View style={{
                    borderWidth: .2,
                    borderColor: 'black',
                    margin: 15,
                    marginTop: 10
                }} />
                <View style={{ alignSelf: 'center', marginTop: 0 }}>
                    <Text>Or</Text>
                </View>
                <View style={{
                    borderWidth: .2,
                    borderColor: 'black',
                    margin: 15,
                    marginTop: 10
                }} />
                {/* <View style={{ margin: 10 }}>
                    <Text>Search Hear Your Skills</Text>
                </View> */}
                {/* <View style={{ borderRadius: 5, borderColor: "#131313", borderWidth: .5, color: "#131313", marginVertical: 10, marginHorizontal: 10 }}>
                    <Picker />
                </View> */}
                <View style={{ margin: 10 }}>
                    <MultiSelect
                        hideTags
                        items={lang2}
                        uniqueKey="id"
                        onSelectedItemsChange={onSelectedItemsChange}
                        selectedItems={selectedItems}
                        selectText="Search Hear Your Skills"
                        searchInputPlaceholderText="Search Items..."
                        onChangeInput={(text) => console.log(text)}
                        tagRemoveIconColor="#CCC"
                        tagBorderColor="#CCC"
                        tagTextColor="#CCC"
                        selectedItemTextColor="#dc2359"
                        selectedItemIconColor="#CCC"
                        itemTextColor="#900"
                        displayKey="name"
                        searchInputStyle={{ color: '#CCC' }}
                        submitButtonColor="#dc2359"
                        submitButtonText="Submit"
                    />
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Do You Want To Add New Skill</Text>
                </View>
                <View style={styles.action}>
                    <TextInput
                        placeholder="Your Skill Name"
                        style={styles.textInput}
                        autoCapitalize="none"
                        //value={resdata.email}
                        onChangeText={(val) => skillText(val)}
                    />
                </View>
                <View style={{ margin: 10 }}>
                    <Text>{data.skillName}</Text>
                </View>
                {/* <View style={{ marginTop: 20, marginBottom: 25 }}>
                    <TouchableOpacity style={{
                        height: 35, justifyContent: 'center', alignItems: 'center', width: '20%',
                        padding: 1,
                        backgroundColor: '#fffff', borderRadius: 5, marginLeft: 10, borderRadius: 5, borderWidth: 1, borderColor: '#5e10b2'
                    }}>
                        <Text style={{ fontSize: 13, color: '#5e10b2', fontWeight: 'bold', padding: 0 }}>Save</Text>
                    </TouchableOpacity>
                </View> */}
                <View style={styles.lineStyle} />
                <View style={{ marginLeft: 15 }}>
                    <Text>List of Subject/Skill</Text>
                </View>
                <View style={{ marginTop: 15, flexDirection: 'row' }}>
                    <TouchableOpacity style={{
                        height: 35, justifyContent: 'center', alignItems: 'center', width: '15%',
                        padding: 1,
                        backgroundColor: '#fffff', borderRadius: 5, marginLeft: 30, borderRadius: 5, borderWidth: 1, borderColor: '#ff52d1'
                    }}>
                        <Text style={{ fontSize: 13, color: '#130002', fontWeight: 'bold' }}>.Net</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{
                        height: 35, justifyContent: 'center', alignItems: 'center', width: '15%',
                        padding: 1,
                        backgroundColor: '#fffff', borderRadius: 5, marginLeft: 30, borderRadius: 5, borderWidth: 1, borderColor: '#ff52d1'
                    }}>
                        <Text style={{ fontSize: 13, color: '#130002', fontWeight: 'bold' }}>Java</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{
                        height: 35, justifyContent: 'center', alignItems: 'center', width: '15%',
                        padding: 1,
                        backgroundColor: '#fffff', borderRadius: 5, marginLeft: 30, borderRadius: 5, borderWidth: 1, borderColor: '#ff52d1'
                    }}>
                        <Text style={{ fontSize: 13, color: '#130002', fontWeight: 'bold' }}>Php</Text>
                    </TouchableOpacity>
                   
                </View>
                {listOfSubjectSkill}
                <View style={styles.lineStyle} />
                <View style={{ flexDirection: 'row', paddingTop: 10, alignItems: 'flex-start', justifyContent: 'space-around', marginRight: 5, marginBottom: 10 }}>
                    <TouchableOpacity style={{
                        height: 35, justifyContent: 'center', alignItems: 'center', width: '20%',
                        padding: 1,
                        backgroundColor: '#5e10b2', borderRadius: 5,
                    }} onPress={() => navigation.navigate('ScreenCreateprofilebasicinformation')}>
                        <Text style={{ fontSize: 13, color: 'white', fontWeight: 'bold' }}>Back</Text>
                    </TouchableOpacity>
                    {/* <TouchableOpacity style={{
                        height: 35, justifyContent: 'center', alignItems: 'center', width: '15%',
                        padding: 1,
                        backgroundColor: '#fffff', borderRadius: 5, borderRadius: 5, borderWidth: 1, borderColor: '#5a287d'
                    }}>
                        <Text style={{ fontSize: 13, color: '#130002', fontWeight: 'bold' }}>Discurt</Text>
                    </TouchableOpacity> */}
                    <TouchableOpacity style={{
                        height: 35, justifyContent: 'center', alignItems: 'center', width: '20%',
                        padding: 1,
                        backgroundColor: '#fffff', borderRadius: 5, marginLeft: 30, borderRadius: 5, borderWidth: 1, borderColor: '#5e10b2'
                    }} onPress={() => navigation.navigate('HomeScreen')}>
                        <Text style={{ fontSize: 13, color: '#5e10b2', fontWeight: 'bold' }}>Discurd</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{
                        height: 35, justifyContent: 'center', alignItems: 'center', width: '33%',
                        padding: 1,
                        backgroundColor: '#d51c6d', borderRadius: 5
                    }} 
                    onPress={() => navigation.navigate('ScreenCreatProfileAccountingInformation')}
                   // onPress={() => submitSubjectSkillPage()}
                  // onPress={() => creatprofilebasicinfoupdate()}
                    >
                        <Text style={{ fontSize: 13, color: 'white', fontWeight: 'bold' }}>Save & Continue</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        </View>
        // {this.props.part2 ? <View><Text style={{ color:"#5a00b5" }}>_________________</Text><Text style={{ color:"#5a00b5" }}>abhijit</Text></View> : <View><Text style={{ color:"#2cd383" }}>______________</Text></View>}
        // {this.props.part3 ? <View><Text style={{ color:"#fffeff" }}>______________</Text></View> : <View><Text style={{ color:"#749182" }}>________________</Text><Text style={{ color:"#749182" }}>abhijit</Text></View>}
    );
};
export default ScreenCreatProfileSubjectSkill;

const styles = StyleSheet.create({
    outerView: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        alignContent: 'center',
        //     flexDirection: 'row',
        //     alignItems: 'center',
        //     justifyContent: 'center',
        paddingTop: 5,
        // paddingBottom: 20,
    },
    container: {
        flex: 1,
        backgroundColor: 'white',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 5,
        //paddingBottom: 10,
        // },
    },
    lineStyle: {
        borderWidth: .2,
        borderColor: 'black',
        margin: 15,
        marginTop: 25
    },
    action: {
        //flexDirection: 'row',
        marginRight: 10,
        marginLeft: 10,
        borderWidth: 0.6,
        borderRadius: 5,
        borderColor: '#505050',
        //padding: 5
    },
    textInput: {
        //flex: 1,
        //marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        color: '#05375a',
        height: 40
    },
});
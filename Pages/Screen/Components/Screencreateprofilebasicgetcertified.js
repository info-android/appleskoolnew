import React, { useState, useEffect } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    Text,
    Image,
    View,
    ScrollView,
    Dimensions,
    TouchableOpacity,
    TextInput, AsyncStorage
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import IconIcon from 'react-native-vector-icons/FontAwesome';
import Spinner from 'react-native-loading-spinner-overlay';
import axios from 'axios';
import ImagePicker from 'react-native-image-crop-picker';

const Screencreateprofilebasicgetcertified = ({ navigation }) => {
    const [token, setToken] = React.useState('');
    const [data, setData] = React.useState('');

    const [loading, setLoading] = useState(false);
    const [imagepath, setImagepath] = useState('');
    const [videopath, setVideopath] = useState('');
    const [Oficiallink, setOficiallink] = useState('');

    const takeVideoFromGallery = () => {
       // alert('take from gallery')
        ImagePicker.openPicker({
            mediaType: "video",
        }).then((video) => {
             console.log(video.path);
             setVideopath(video.path)
        });
    }

    const takePhotoFromGallery = () => {
        // alert('take from gallery')
        ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true,
        }).then(image => {
            setImagepath(image.path)
            console.log(imagepath);
        });
    }
    const TokenGetAndCertificat = () => {
        setLoading(true);
        // setLoding(false)
        // alert(data.email+data.password)
        axios.defaults.baseURL = 'https://appleskool.com/preview/appleskool_code/api/get-certificate';
        axios.defaults.headers.post['Content-Type'] =
            'application/json;charset=utf-8';
        axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';
        axios.defaults.headers.post['Access-Control-Allow-Headers'] = '*';
        //alert(global.userToken)
        const bodyParameters = {
        };
        axios
            .post('https://appleskool.com/preview/appleskool_code/api/get-certificate',
                bodyParameters,
                {
                    headers: {
                        Authorization: `Bearer ${token}`,
                        // Authorization: `Bearer  ${userToken}`,global.userToken
                    },
                },
            )
            .then(
                res => {
                    setLoading(false);

                    //alert(JSON.stringify(res.data.categories[0].name));
                    //global.Me = me.data.firstname + me.data.lastname;
                    setData(res.data.user)
                    //setImagepath(res.data.degree_certificate)
                    setVideopath(res.data.user.video);
                    setOficiallink(res.data.user.official_link)
                    //setMaster_skills(res.data.master_skills);

                    // setdata(res.data.teacher_skills.skills.skill_name);
                    // alert(JSON.stringify(res.data.user.degree_certificate))
                    //  alert(JSON.stringify(res.data.user.official_link))
                },
                error => {
                    setLoading(false);
                    //   alert("Certifecat get api error")
                },
            );
    };

    var bodyFormData = new FormData();

    // bodyFormData.append('degree_certificate','siraaaaaaaaa')
    // bodyFormData.append('video','sirsirsxsxsx')
    bodyFormData.append('official_link', Oficiallink)
    bodyFormData.append('degree_certificate', {
        uri: imagepath,
        name: 'selfie.jpg',
        type: 'image/jpg'
    })
    bodyFormData.append('video', {
        uri: videopath,
        name: 'selfie.mp4',
        type: 'video/mp4'
    })

    const CertificatAndVideoSubmit = async () => {
        try {
            // alert(token)

            //alert(Authorization)
            await axios({
                method: 'post',
                url: 'https://appleskool.com/preview/appleskool_code/api/certificate-save',
                data: bodyFormData,
                headers: {
                    'Content-Type': 'multipart/form-data',
                    'Authorization': 'Bearer' + token
                }
            }).then(function (response) {
                //handle success
                console.log(response);
                // navigation.navigate("HomeScreen")
              //  alert(JSON.stringify(response.data))
            })
                .catch(function (response) {
                    //handle error
                    //navigation.navigate("HomeScreen")
                    //  alert(JSON.stringify(response.data.error))
                    console.log(response);
                });

        } catch (err) {
            console.log(err)

        }


    };

    useEffect(() => {
        if (token) {
            TokenGetAndCertificat()
            // CertificatAndVideoSubmit()
        } else {
            AsyncStorage.getItem('UID123', (err, result) => {
                setToken(result);
                // setTimeout(()=>{
                //    
                // }, 3000);
            })
        }


    }, [token])
    return (
        <View style={styles.outerview}>
            <Spinner
                //visibility of Overlay Loading Spinner
                visible={loading}
                color={"black"}
                overlayColor={'rgba(255,255,255, 1)'}
                //Text with the Spinner
                textContent={'Loading...'}
            //Text style of the Spinner Text
            //textStyle={styles.spinnerTextStyle}
            />
            <ScrollView>
                <View style={{ padding: 15, }}>
                    <Text style={{ fontSize: 25, fontWeight: 'bold' }}>Creat Instructor's Profile</Text>
                </View>
                <View style={styles.container}>
                    <View style={{ alignItems: 'center' }}><Text style={{ color: "#82b46d", }}>________________</Text><Text style={{ color: "#0e0200", paddingTop: 20, fontSize: 9 }}>Basic Information</Text><View style={{ position: 'absolute' }}><View style={{ height: 33, width: 33, backgroundColor: "#82b46d", borderRadius: 50, paddingLeft: 6.2, paddingTop: 4 }}><Icon name="lock-closed" size={21} color="#ffffff" /></View></View></View>
                    <View style={{ alignItems: 'center' }}><Text style={{ color: "#82b46d", }}>________________</Text><Text style={{ color: "#0e0200", paddingTop: 20, fontSize: 9 }}>Subject/Skills</Text><View style={{ backgroundColor: "2cd383", paddingBottom: 20, position: 'absolute' }}><View style={{ height: 33, width: 33, backgroundColor: "#82b46d", borderRadius: 50, paddingLeft: 4.4, paddingTop: 3.6 }}><Icon name="cog" size={25} color="#ffffff" backgroundColor="#2cd383" /></View></View></View>
                    <View style={{ alignItems: 'center' }}><Text style={{ color: "#82b46d", }}>________________</Text><Text style={{ color: "#0e0200", paddingTop: 20, fontSize: 9 }}>Accounting Information</Text><View style={{ backgroundColor: "2cd383", paddingBottom: 20, position: 'absolute' }}><View style={{ height: 33, width: 33, backgroundColor: "#82b46d", borderRadius: 50, paddingLeft: 5.5, paddingTop: 4 }}><Icon name="calendar" size={21} color="#ffffff" /></View></View></View>
                    <View style={{ alignItems: 'center' }}><Text style={{ color: "#5a00b5", }}>________________</Text><Text style={{ color: "#0e0200", paddingTop: 20, fontSize: 9 }}>Get Certified</Text><View style={{
                        backgroundColor: "2cd383", paddingBottom: 20,
                        position: 'absolute'
                    }}><View style={{ height: 33, width: 33, backgroundColor: "#5a00b5", borderRadius: 50, paddingLeft: 5, paddingTop: 6.5 }}><IconIcon name="check" size={21} color="#ffffff" /></View></View></View>
                </View>
                <View style={{ padding: 15, }}>
                    <Text style={{ fontSize: 22, fontWeight: '400' }}>Get Certified by AppleSkool</Text>
                    <View style={{ marginTop: 10, }}>
                        <Text style={{ fontSize: 10 }}>Your ratings, reviews, credentials, introductory video and how you perform in the class will be analysed by the AppleSkool experts to certify you to train on AppleSkool. The certification process is a rigorous process and would take 45 days to be completed. You may be contacted during the process.</Text>
                    </View>
                </View>
                <View style={{ padding: 15, }}>
                    <Text style={{ fontSize: 19, fontWeight: '300' }}>Being certified will enable you to</Text>
                    <View style={{ marginTop: 10, }}>
                        <Text style={{ fontSize: 10 }}>1. Enable listings with "certified" highlight.</Text>
                        <Text style={{ fontSize: 10 }}>2.Reach out to more students.</Text>
                        <Text style={{ fontSize: 10 }}>3.Have more credibility.</Text>
                        <Text style={{ fontSize: 10 }}>4.AppleSkool will reach out to you with Students requirements</Text>
                    </View>
                    <View style={styles.lineStyle} />
                </View>
                <View style={{ paddingHorizontal: 10, }}>
                    <Text style={{ fontSize: 15, textAlign: 'justify' }}> Upload your credentials of the highest degree/ certificates in your skill areas</Text>
                </View>
                <View style={{ margin: 10 }} >
                    <TouchableOpacity style={{
                        height: 45, width: '100%',
                        padding: 5,
                        backgroundColor: '#e6a000', borderRadius: 5
                    }} onPress={takePhotoFromGallery}>
                        <View style={{ flexDirection: 'row', alignItems: 'stretch', justifyContent: 'space-between' }}>
                            <Text style={{ fontSize: 13, color: 'white', alignItems: 'flex-start', justifyContent: 'flex-start', paddingTop: 8 }}>Click here to Upload your file</Text>
                            <View style={{ paddingTop: 6, paddingRight: 10 }}>
                                <IconIcon name="upload" size={23} color="#ffffff" />
                            </View>
                        </View>
                    </TouchableOpacity>
                    <Text style={{ paddingTop: 10, color: "#8300c2" }}>View Certificate</Text>
                </View>
                <View style={{ padding: 0, }}>
                    <Text style={{ fontSize: 15, marginLeft: 10 }}>Upload a 2 minute introductory video (if not uploaded already)</Text>
                </View>
                <View style={{ margin: 10 }}


                >
                    <TouchableOpacity style={{
                        height: 45, width: '100%',
                        padding: 5,
                        backgroundColor: '#e6a000', borderRadius: 5
                    }}
                        onPress={takeVideoFromGallery}
                    >
                        <View style={{ flexDirection: 'row', alignItems: 'stretch', justifyContent: 'space-between' }}>
                            <Text style={{ fontSize: 13, color: 'white', alignItems: 'flex-start', justifyContent: 'flex-start', paddingTop: 8 }}>{videopath ? videopath :"Click to Upload your file"}</Text>
                            <View style={{ paddingTop: 6, paddingRight: 10 }}>
                                <IconIcon name="upload" size={23} color="#ffffff" />
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={styles.lineStyle} />
                {/* <View style={{ padding: 0, }}>
                    <Text style={{ fontSize: 15, fontWeight: 'bold' }}>Creat Instructor's Profile Creat Instructor's Profile</Text>
                </View> */}
                <View style={{ margin: 0 }}>
                    <Text style={{ marginLeft: 10 }}>Send link to a class that you are taking for our experts to observe your training</Text>
                    <TextInput
                        value={Oficiallink}

                        style={{ height: 40, borderColor: 'gray', borderWidth: 1, borderRadius: 5, fontWeight: 'bold', margin: 10 }}
                        onChangeText={(text) => setOficiallink(text)}
                    //value={value}
                    />
                </View>
                <View style={styles.lineStyle} />
                <View style={{ flexDirection: 'row', paddingTop: 10, alignItems: 'flex-start', justifyContent: 'space-around', marginHorizontal: 10, marginBottom: 10 }}>
                    <TouchableOpacity style={{
                        height: 35, justifyContent: 'center', alignItems: 'center', width: '25%',
                        backgroundColor: '#5a287d', borderRadius: 5, marginLeft: 10
                    }} onPress={() => navigation.navigate("ScreenCreatProfileAccountingInformation")}>
                        <Text style={{ fontSize: 13, color: 'white', fontWeight: 'bold' }}>Back</Text>
                    </TouchableOpacity>
                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
                        <TouchableOpacity style={{
                            height: 35, justifyContent: 'center', alignItems: 'center', width: '20%',
                            padding: 1,
                            backgroundColor: '#fffff', borderRadius: 5, borderRadius: 5, borderWidth: 1, borderColor: '#6620b9', marginRight: 5
                        }}
                            // onPress={() => navigation.navigate("HomeScreen")}
                            onPress={() => CertificatAndVideoSubmit()}
                        >
                            <Text style={{ fontSize: 13, color: '#6620b9', fontWeight: 'bold' }} >Skip</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{
                            height: 35, justifyContent: 'center', alignItems: 'center', width: '22%',
                            padding: 1,
                            backgroundColor: '#fffff', borderRadius: 5, borderRadius: 5, borderWidth: 1, borderColor: '#6620b9', marginRight: 5
                        }} onPress={() => navigation.navigate("HomeScreen")}>
                            <Text style={{ fontSize: 13, color: '#6620b9', fontWeight: 'bold' }}>Discard</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{
                            height: 35, justifyContent: 'center', alignItems: 'center', width: '39%',
                            padding: 1,
                            backgroundColor: '#d51c6d', borderRadius: 5, marginRight: 10
                        }} onPress={() => CertificatAndVideoSubmit()}>
                            <Text style={{ fontSize: 13, color: 'white', fontWeight: 'bold' }}>Save and Continue</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        </View>
    );
};
export default Screencreateprofilebasicgetcertified;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 10,
        paddingBottom: 20,
    },
    lineStyle: {
        borderWidth: .4,
        borderColor: 'black',
        margin: 15,
        marginTop: 25
    },
    outerview: {
        backgroundColor: 'white',
        paddingTop: 3,
        // marginBottom:9
    }
});


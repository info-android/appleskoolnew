import React, { useState, useEffect } from 'react';
import {
    SafeAreaView, StyleSheet, Text, Image, View, ScrollView, Dimensions, TouchableOpacity, TextInput, StatusBar, Button, ActivityIndicator, FlatList, AsyncStorage,
    Picker, Animated, Platform
} from 'react-native';
import axios from 'axios';
import RadioButtonRN from 'radio-buttons-react-native';

import MultiSelect from 'react-native-multiple-select';
import PhoneInput from "react-native-phone-number-input";
import DateTimePickerModal from "react-native-datepicker";
import ImagePicker from 'react-native-image-crop-picker';
//import PhoneInput from 'react-native-phone-input'
//import 'react-phone-number-input/style.css'
//import PhoneInput from 'react-phone-number-input'
import { RadioButton } from 'react-native-paper';
import IconIcon from 'react-native-vector-icons/FontAwesome';
import IconIconIcon from 'react-native-vector-icons/FontAwesome5';
import Icon from 'react-native-vector-icons/Ionicons';
import { Item } from 'native-base';
//import MultiSelect from 'react-native-multiple-select';
import Spinner from 'react-native-loading-spinner-overlay';
//import Picker from '../Components/Picker'
import { Dropdown } from 'react-native-material-dropdown';
const icon1 = '<IconIcon name="check" size={20} color="#ffffff" />'

const ScreenCreateprofilebasicinformation = ({ navigation }) => {
    const [imagepath, setImagepath] = useState('')
    const [videopath, setVideopath] = useState('');
    const [Date, setDate] = useState('');
    const [loading, setLoading] = useState(false);
    const [uniset, setuniset] = useState(false);
    const [unisetlink, setunisetlink] = useState(false);

    const [checked, setChecked] = React.useState('first');
    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
    const data9 = [
        {
            label: 'Male'
        },
        {
            label: 'Fimale'
        }
    ];

    const [data, setData] = useState({
        loginToken: '',
        basicIinfo: '',
        firstname: '',
        lastname: '',
        nickname: '',
        phone: '',
        email: '',
        dob: '',
        gender: '',
        collagename: '',
        degree: '',
        year: '',
        onelineaboutme: '',
        aboutme: '',
        photos: '',
        videos: '',
        street: '',
        pin: '',
        state: '',
        city: '',
        country: '',
        youtube: '',
        twitter: '',
        facebook: '',
        blog: '',
        website: '',
        otherlink: '',
        link: '',
        State: [],
        city: [],
        //isLoading: true,
        selectedItems: [],
    });
    //          FOR UNIVERCITY

    const [inputs, setInputs] = useState([{ key: '', value: '' }]);
    const [inputsy, setInputsy] = useState([{ key: '', value: '' }]);
    const [inputsz, setInputsz] = useState([{ key: '', value: '' }]);

    const addHandler = () => {

        const _inputs = [...inputs];
        const _inputsY = [...inputsy];
        const _inputsZ = [...inputsz];
        _inputs.push({ key: '', value: '' });
        _inputsY.push({ key: '', value: '' });
        _inputsZ.push({ key: '', value: '' });
        setInputs(_inputs);
        setInputsy(_inputsY);
        setInputsz(_inputsZ);
        setuniset(true)
    }

    const deleteHandler = (key) => {
        const _inputs = inputs.filter((input, index) => index != key);
        setInputs(_inputs);
    }

    const inputHandler1 = (text, key) => {
        const _inputs = [...inputs];
        _inputs[key].value = text;
        _inputs[key].key = key;
        setInputs(_inputs);
        //alert(_inputs[key])
        console.log(JSON.stringify(_inputs))
    }
    const inputHandler2 = (text, key) => {
        const _inputs = [...inputsy];
        _inputs[key].value = text;
        _inputs[key].key = key;
        setInputsy(_inputs);
        console.log(JSON.stringify(_inputs))


    }
    const inputHandler3 = (text, key) => {
        const _inputs = [...inputsz];
        _inputs[key].value = text;
        _inputs[key].key = key;
        setInputsz(_inputs);
        console.log(JSON.stringify(_inputs))
    }

    const inputHandler4 = (text, key) => {
        const _inputs = [...inputs];
        _inputs[key].value = text;
        _inputs[key].key = key;
        setInputs(_inputs);
        //alert(_inputs[key])
        console.log(JSON.stringify(_inputs))
    }
    const inputHandler5 = (text, key) => {
        const _inputs = [...inputsy];
        _inputs[key].value = text;
        _inputs[key].key = key;
        setInputsy(_inputs);
        console.log(JSON.stringify(_inputs))


    }
    const inputHandler6 = (text, key) => {
        const _inputs = [...inputsz];
        _inputs[key].value = text;
        _inputs[key].key = key;
        setInputsz(_inputs);
        console.log(JSON.stringify(_inputs))
    }

    //          FOR LINK
    const [inputs2, setInputs2] = useState([{ key: '', value: '' }]);
    const [inputsa, setInputsa] = useState([{ key: '', value: '' }]);

    const _addHandler2 = () => {
        //alert("link function")
        const _inputs2 = [...inputs2];
        const _inputsA = [...inputsa];
        _inputs2.push({ key: '', value: '' });
        _inputsA.push({ key: '', value: '' });
        setInputs2(_inputs2);
        setInputsa(_inputsA);
        setunisetlink(true)
    }

    const _deleteHandler2 = (key) => {
        const _inputs2 = inputs2.filter((input2, index2) => index2 != key);
        setInputs2(_inputs2);
    }

    const inputHandler11 = (text, key) => {
        const _inputs2 = [...inputs2];
        _inputs2[key].value = text;
        _inputs2[key].key = key;
        setInputs2(_inputs2);
        console.log(JSON.stringify(_inputs2))
    }
    const inputHandler22 = (text, key) => {
        const _inputs2 = [...inputsa];
        _inputs2[key].value = text;
        _inputs2[key].key = key;
        setInputsa(_inputs2);
        console.log(JSON.stringify(_inputs2))
    }

    const items = [
        // name key is must. It is to show the text in front
        { id: 1, name: 'angellist' },
        { id: 2, name: 'codepen' },
        { id: 3, name: 'envelope' },
        { id: 4, name: 'etsy' },
        { id: 5, name: 'facebook' },
        { id: 6, name: 'foursquare' },
        { id: 7, name: 'github-alt' },
        { id: 8, name: 'github' },
        { id: 9, name: 'gitlab' },
        { id: 10, name: 'instagram' },
    ];
    //const [loding , setLoding] = useState(true);
    const [token, setToken] = React.useState('');


    const [resdata, setData2] = useState('');
    const [resdata3, setData3] = useState([]);
    const [resdata4, setData4] = useState([]);
    const [resdata5, setData5] = useState([]);
    const [resdata6, setData6] = useState([]);
    const [resdata7, setData7] = useState([]);
    const [resdata8, setData8] = useState([]);
    const [resdata9, setData9] = useState([]);
    const [resdata10, setData10] = useState([]);
    const [resdata11, setData11] = useState([]);

    const [selectedItems, setSelectedItems] = useState([]);

    const [data2, setSelectLabel] = useState({
        selectLabel: '',
    })
    const [data3, setSelectLabel3] = useState({
        selectLabel3: '',
    })
    const [data4, setSelectLabel4] = useState({
        selectLabel4: '',
    })
    const [key, setKey] = useState('male'); // const [gender, setGender] = useState(‘male’);
    const [option1, setOption1] = useState(true) // const [maleCheck, setMaleCheck) = useState(true);
    const [femaleCheck, setFemaleCheck] = useState(false);
    // onSelectedItemsChange = selectedItems => {
    //     setData.selectedItems({ selectedItems });
    // };

    // const [loding,setLoding] = useState('')
    //const [value, setValue] = React.useState()
    // constructor(props) {
    //     super(props);
    //     //global.Currentstate = navigation.state.routeName;
    //     // this.state = { setData: '' };
    //     this.state = {
    //         // token: '',
    //         productArray: [],
    //         isLoading: true
    //       }
    // }

    const onSelectedItemsChange = (selectedItems) => {
        // Set Selected Items
        setSelectedItems(selectedItems);
        setLanguageSpoken2(selectedItems)
        // alert(JSON.stringify(LanguageSpoken2))
        // console.log(LanguageSpoken)
        //  console.log(selectedItems)
    };



    const showDatePicker = () => {
        setDatePickerVisibility(true);
    };

    const hideDatePicker = () => {
        setDatePickerVisibility(false);
    };

    const handleConfirm = (date) => {
       // console.warn("A date has been picked: ", date);
        setDate(date)
        hideDatePicker();

    };

    const takePhotoFromGallery = () => {
        // alert('take from gallery')
        ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true,


        }).then(image => {

            setImagepath(image.path)
            console.log(imagepath);
        });
    }

    const takeVideoFromGallery = () => {
        // alert('take from gallery')
        ImagePicker.openPicker({
            mediaType: "video",
        }).then((video) => {
            setVideopath(video.path)
            console.log(video.path);
        });
    }

    const creatprofilebasicinfo = () => {
        //alert(token);
        setLoading(true);
        // setLoding(false)
        // alert(data.email+data.password)
        axios.defaults.baseURL = 'https://appleskool.com/preview/appleskool_code/api';
        axios.defaults.headers.post['Content-Type'] =
            'application/json;charset=utf-8';
        axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';
        axios.defaults.headers.post['Access-Control-Allow-Headers'] = '*';
        //alert(global.userToken)
        const bodyParameters = {
        };
        axios
            .post('https://appleskool.com/preview/appleskool_code/api/edit-profile',
                bodyParameters,
                {
                    headers: {
                        Authorization: `Bearer ${token}`,
                        // Authorization: `Bearer  ${userToken}`,global.userToken
                    },
                },
            )
            .then(


                res => {
                    setLoading(false);
                    //alert(JSON.stringify(res.data.result.user.fname));
                    //global.Me = me.data.firstname + me.data.lastname;
                    setData({
                        ...data,
                        basicIinfo: res.data.result.user.fname
                    });

                    setData2(res.data.result.user);
                    setFname(res.data.result.user.fname)
                    setLname(res.data.result.user.lname)
                    setNickname(res.data.result.user.nick_name)
                    setDate(res.data.result.user.dob)
                    setPhoneNo(res.data.result.user.phone_no)
                    setPhoneCountryCode(res.data.result.user.phone_country_code)

                    setStreet(res.data.result.user.street)
                    setPin(res.data.result.user.pin)
                    setAboutmeshort(res.data.result.user.about_me_shrt)
                    setExperience(res.data.result.user.experience)

                    setUnivercity(res.data.result.user.qualifications)
                    // setDegree(res.data.result.user.qualifications)
                    // setDegreeYear(res.data.result.user.qualifications)

                    setLink(res.data.result.user.social_links)
                    setGender(res.data.result.user.gender)


                    setAboutme(res.data.result.user.about_me)

                    setYoutube(res.data.result.user.youtube_link)
                    setTwiter(res.data.result.user.twitter_link)
                    setFacebook(res.data.result.user.facebook_link)
                    setBlog(res.data.result.user.blogs_link)
                    setWebsite(res.data.result.user.website_link)
                    setData3(res.data.result.state);
                    setLanguageSpoken(res.data.result.languages)
                    setShowLanguageSpoken(res.data.result.user.teacher_languages)
                    setIMage(res.data.result.user.profile_picture)
                    setVIdeo(res.data.result.user.video)
                    setData5(res.data.result.languages);
                    setData8(res.data.result.user.teacher_languages);
                    setData6(res.data.result.user.social_links);
                    setData7(res.data.result.state);
                    setData9(res.data.result.user.user_city);
                    setData10(res.data.result.user.qualifications);
                    setSelectLabel({ selectLabel: res.data.result.user.state })
                    setSelectLabel3({ selectLabel3: res.data.result.user.city })
                    if (res.data.result.user.gender == "male") {

                        radioHandler()

                    } else {

                        radioHandler2()

                    }

                    selectState(res.data.result.user.state)
                    //alert(res.data.result.user.user_state)
                    // alert(res.data.result.user.teacher_languages[2].name)
                    //alert(res.data.result.user.social_links[0].name)
                    //alert(res.data.result.user.gender)

                    // setData({
                    //     ...data,
                    //     lastname:response.data.result.token
                    // });
                    // setData({
                    //     ...data,
                    //     email:response.data.result.token
                    // });

                },
                error => {
                    setLoading(false);
                    // alert("Create profile info api error")
                },
            );
    };
    // if (data.isLoading) {
    //     return (
    //         <View style={{ alignItems: 'center', justifyContent: 'center' }}>
    //             <ActivityIndicator size="large" color="#900" />
    //         </View>
    //     )
    // }
    const selectState = (id) => {
        //alert(id);
        setSelectLabel({ selectLabel: id })
        axios.defaults.baseURL = 'https://appleskool.com/preview/appleskool_code/api';
        axios.defaults.headers.post['Content-Type'] =
            'application/json;charset=utf-8';
        axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';
        axios.defaults.headers.post['Access-Control-Allow-Headers'] = '*';
        axios
            .post(
                'https://appleskool.com/preview/appleskool_code/api/get-city', {
                "jsonrpc": "2.0",
                "params": {
                    "state_id": id,
                }
            }
            )
            .then((response) => {
                setData({
                    ...data,
                    isLoading: false,
                });
                //  alert(JSON.stringify(response.data.error));
                if (response.data.error) {
                    // alert(JSON.stringify(response.data))
                }
                else if (response.data.result) {
                    setData4(response.data.result.city_list);
                    //alert(JSON.stringify(resdata4))
                    //navigation.navigate('ScreenCreatProfileSubjectSkill')
                }
            }, (error) => {
                // alert('state ERROR', [
                //     { text: 'Okay' }
                // ]);
                // console.log(error);
            });
        //alert("State ERROR")
    }
    const selectState2 = (id) => {
        //alert(id);
        setSelectLabel({ selectLabel: id })
        axios.defaults.baseURL = 'https://appleskool.com/preview/appleskool_code/api';
        axios.defaults.headers.post['Content-Type'] =
            'application/json;charset=utf-8';
        axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';
        axios.defaults.headers.post['Access-Control-Allow-Headers'] = '*';
        axios
            .post(
                'https://appleskool.com/preview/appleskool_code/api/get-city', {
                "jsonrpc": "2.0",
                "params": {
                    "state_id": id,
                }
            }
            )
            .then((response) => {
                setData({
                    ...data,
                    isLoading: false,
                });
                //  alert(JSON.stringify(response.data.error));
                if (response.data.error) {
                    // alert(JSON.stringify(response.data))
                }
                else if (response.data.result) {
                    setData4(response.data.result.city_list);
                    setSelectLabel3({ selectLabel3: response.data.result.city_list[0].id })
                    //alert(JSON.stringify(resdata4))
                    //navigation.navigate('ScreenCreatProfileSubjectSkill')
                }
            }, (error) => {
                // alert('state ERROR', [
                //     { text: 'Okay' }
                // ]);
                // console.log(error);
            });
        //alert("State ERROR")
    }
    // EDIT FIELDS
    const [Fname, setFname] = useState('');
    const [Lname, setLname] = useState('');
    const [Nickname, setNickname] = useState('');
    const [PhoneNo, setPhoneNo] = useState('');
    const [Street, setStreet] = useState('');
    const [Pin, setPin] = useState('');
    const [Aboutmeshort, setAboutmeshort] = useState('');
    const [Experience, setExperience] = useState('');
    const [phoneCountryCode, setPhoneCountryCode] = useState('');
    const [Univercity, setUnivercity] = useState([]);
    const [COMMINGDATE, setDegree] = useState([]);
    const [DegreeYear, setDegreeYear] = useState([]);
    const [IMage, setIMage] = useState([]);
    const [VIdeo, setVIdeo] = useState([]);


    const [Link, setLink] = useState([]);
    //const [EditLink, setEditLink] = useState('');
    const [Gender, setGender] = useState([]);
    const [LanguageSpoken, setLanguageSpoken] = useState('');
    const [ShowLanguageSpoken, setShowLanguageSpoken] = useState([]);
    const [LanguageSpoken2, setLanguageSpoken2] = useState('');



    const [Aboutme, setAboutme] = useState('');
    const [Youtube, setYoutube] = useState('');
    const [Twiter, setTwiter] = useState('');
    const [Facebook, setFacebook] = useState('');
    const [Blog, setBlog] = useState('');
    const [Website, setWebsite] = useState('');

    var bodyFormData = new FormData();

    //bodyFormData.append('fname', 'sorry')
    bodyFormData.append('fname', Fname)

    //bodyFormData.append('lname', 'sorry')
    bodyFormData.append('lname', Lname)

    //bodyFormData.append('nick_name', 'Nickname')
    bodyFormData.append('nick_name', Nickname)

    //bodyFormData.append('phone_no', '3216549871')
    bodyFormData.append('phone_no', PhoneNo)

    //bodyFormData.append('phone_country_code', '+91')
    bodyFormData.append('phone_country_code', phoneCountryCode)

    //bodyFormData.append('street', 'sasasass')
    bodyFormData.append('street', Street)

    //bodyFormData.append('state', '1')
    bodyFormData.append('state', data2.selectLabel)

    //bodyFormData.append('city', '1394')
    bodyFormData.append('city', data3.selectLabel3)

    //bodyFormData.append('pin', '123456')
    bodyFormData.append('pin', Pin)

    //bodyFormData.append('country_id', '4')
    bodyFormData.append('country_id', '4')

    //bodyFormData.append('gender', 'male')
    bodyFormData.append('gender', resdata11)

    //bodyFormData.append('about_me_shrt', 'sasasa')
    bodyFormData.append('about_me_shrt', Aboutmeshort)

    //bodyFormData.append('experience', '2')
    bodyFormData.append('experience', Experience)

    //bodyFormData.append('language', '[2, 3, 4, 5, 6]')
    bodyFormData.append('language', JSON.stringify(LanguageSpoken2))

    //bodyFormData.append('about_me', 'sasasasa')
    bodyFormData.append('about_me', Aboutme)

    //bodyFormData.append('dob','')
    bodyFormData.append('dob', Date)

    //bodyFormData.append('youtube_link', 'Youtube')
    bodyFormData.append('youtube_link', Youtube)

    //bodyFormData.append('twitter_link', 'Twiter')
    bodyFormData.append('twitter_link', Twiter)

    //bodyFormData.append('facebook_link', 'Facebook')
    bodyFormData.append('facebook_link', Facebook)

    //bodyFormData.append('blogs_link', 'Blog')
    bodyFormData.append('blogs_link', Blog)

    //bodyFormData.append('website_link', 'Website')
    bodyFormData.append('website_link', Website)

    //bodyFormData.append('degree', '[{"key":0,"value":"MCA"},{"key":1,"value":"BCA"}]')
    bodyFormData.append('degree', JSON.stringify(inputs))

    //bodyFormData.append('institute_name', '[{"key":0,"value":"techno"},{"key":1,"value":"CU"}]')
    bodyFormData.append('institute_name', JSON.stringify(inputsy))

    //bodyFormData.append('year', '[{"key":0,"value":"2015"},{"key":1,"value":"2019"}]')
    bodyFormData.append('year', JSON.stringify(inputsz))

    // bodyFormData.append('profile_picture', '')
    bodyFormData.append('profile_picture', {
        uri: imagepath,
        name: 'selfie.jpg',
        type: 'image/jpg'
    })
    //bodyFormData.append('Content-Type', 'image/jpeg')

    bodyFormData.append('video', {
        uri: videopath,
        name: 'selfie.mp4',
        type: 'video/mp4'
    })
    // bodyFormData.append('video', '')

    //bodyFormData.append('other_link_name', '[{"key":0,"value":"LINK"},{"key":1,"value":"LINK2"}]')
    bodyFormData.append('other_link_name', JSON.stringify(inputs2))

    //bodyFormData.append('other_link', '[{"key":0,"value":"its link"},{"key":1,"value":"its link"}]')
    bodyFormData.append('other_link', JSON.stringify(inputsa))

    const sendGetRequest = async () => {
        try {
            const resp = await axios.get('https://jsonplaceholder.typicode.com/posts');
            console.log(resp.data);
        } catch (err) {
            // Handle Error Here
            console.error(err);
        }
    };


    const creatprofilebasicinfoupdate = async () => {
        if (Lname == '' || Nickname == '' || PhoneNo == '' || Gender == '' || LanguageSpoken2 == '' || Street == '' || Pin == '' || data2.selectLabel == '' || data3.selectLabel3 == '' || resdata11 == '' || Aboutmeshort == '' || Aboutme == '' || Experience == '' || Youtube == '' || Twiter == '' || Facebook == '' || Blog == '' || Website == '' || imagepath == '' || videopath == '') {
            alert("Enter Valid Details");
        } else {
            try {
                // alert(token)
                // alert(Fname + "--" + Lname + "--" + Nickname + "--" + PhoneNo + "--" + "--" + Gender + "--" + JSON.stringify(LanguageSpoken2) + "--" + Street + "-" + Pin + "" + data2.selectLabel + "" + data3.selectLabel3 + "" + resdata11 + "" + Aboutmeshort + "" + Aboutmeshort + "" + Experience + "" + Youtube + "" + Twiter + "" + Facebook + "" + Blog + "" + Website + JSON.stringify(imagepath) + "" + JSON.stringify(videopath))
                console.log(JSON.stringify(imagepath))
                //alert(Authorization)
                await axios({
                    method: 'post',
                    url: 'https://appleskool.com/preview/appleskool_code/api/update-profile',
                    data: bodyFormData,
                    headers: {
                        'Content-Type': 'multipart/form-data',
                        'Authorization': 'Bearer' + token
                    }
                }).then(function (response) {
                    //handle success
                    // console.log(response);

                    //  alert(JSON.stringify(response.data))
                    navigation.navigate("ScreenCreatProfileSubjectSkill")
                })
                    .catch(function (response) {
                        //handle error
                        // navigation.navigate("ScreenCreatProfileSubjectSkill")
                        //alert(JSON.stringify(response.data.error))
                        console.log(response);
                    });

            } catch (err) {
                console.log(err)

            }
        }
    }

    // Shows = (value) => {
    //     alert(value);
    //     // setSelectLabel({
    //     //     ...selectLabel,
    //     //     selectLabel: label
    //     // });
    //  setSelectLabel({selectLabel:value});
    // }
    const Show = (value) => {
        //alert(value);
        setSelectLabel({ selectLabel: value });
        //alert(data2.selectLabel);
    };

    const Show2 = (value2) => {
        //alert(value);
        setData3({ resdata3: value2 });
        //alert(data2.selectLabel);
    };

    useEffect(() => {
        if (token) {
            //alert("Useeffect If Condition")
            creatprofilebasicinfo()
            //selectState()
        } else {
            // alert("Please Login Again")
            AsyncStorage.getItem('UID123', (err, result) => {
                setToken(result);
                // setTimeout(()=>{
                //    
                // }, 3000);
            })
        }
    }, [token])

    // let language = resdata5.map((myValue, myIndex) => {
    //     // console.log('myValue: ' + myValue.name)
    //     return (
    //         <Picker.Item label={myValue.name} value={myValue.id} key={myIndex} />
    //     )
    // });
    // var lang = [];
    // let langr = resdata5.map((myValue, myIndex) => {
    //     console.log('myValue: ' + myValue.name)
    //     lang.push({
    //         ["id"]: myValue.id,
    //         ["name"]: myValue.name
    //     });
    // });
    var langSpacific = [];
    let lanasass = resdata8.map((myValue, myIndex) => {
        // console.log('myValue: ' + myValue.name)
        langSpacific.push({
            ["id"]: myValue.id,
            ["name"]: myValue.name
        });
    });
    var langSpacific2 = [];
    let langua = resdata5.map((myValue, myIndex) => {
        // console.log('myValue: ' + myValue.name)
        langSpacific2.push({
            ["id"]: myValue.id,
            ["name"]: myValue.name
        });
        //  return (
        //      <Picker.Item label={myValue.name} value={myValue.id} key={myIndex} />
        //  )
    });
    if (ShowLanguageSpoken !== null) {
        var langnew = [];

        let langN = ShowLanguageSpoken.map((myValue, myIndex) => {
            langnew.push(myValue.id);
        });
    }


    var pickerItem = [];

    let pickers = resdata3.map((myValue, myIndex) => {
        //  console.log('myValue: ' + myValue.state)
        pickerItem.push({ label: myValue.state, value: myValue.id })

    });

    //alert(JSON.stringify(langnew))
    let myUsers = resdata3.map((myValue, myIndex) => {
        //  console.log('myValue: ' + myValue.state)
        return (
            <Picker.Item label={myValue.state} value={myValue.id} key={myIndex} />
        )
    });


    var pickerItem2 = [];

    let pickerss = resdata4.map((myValue, myIndex) => {
        //  console.log('myValue: ' + myValue.state)
        pickerItem2.push({ label: myValue.city_name, value: myValue.id })

    });
    let myUsers2 = resdata4.map((myValue, myIndex) => {
        //  console.log('myValue: ' + myValue.city_name)
        return (
            <Picker.Item label={myValue.city_name} value={myValue.id} key={myIndex} />
        )
    });
    // let ShowTeacherLanguage = setShowLanguageSpoken.map((myValue, myIndex) => {
    //     //  console.log('myValue: ' + myValue.city_name)
    //     return (
    //         <Picker.Item label={myValue.city_name} value={myValue.id} key={myIndex} />
    //     )
    // });
    let ShowTeacherLanguage = ShowLanguageSpoken.map((myValue, myIndex) => {
        //  console.log('myValue: ' + myValue.city_name)
        return (
            <>
                <View style={{ flexDirection: 'row', marginLeft: 10, flexWrap: "wrap", }}>
                    <Text>{myValue.name}</Text>
                </View>
            </>
        )
    });
    // const deleteHandler3 = (key3) => {
    //     const _inputs3 = inputs3.filter((input3, index3) => index3 != key3);
    //     setInputs3(_inputs3);
    // }

    let Qual = Univercity.map((myValue, myIndex) => {
        //  console.log('myValue: ' + myValue.city_name)
        return (
            <>
                <View style={{ margin: 10 }}>
                    <Text>Univercity/Collage/Institution</Text>
                </View >
                <View style={styles.action}>
                    <TextInput
                        value={myValue.institute_name}
                        placeholder="Collage Name"
                        style={styles.textInput}
                        autoCapitalize="none"
                        onChangeText={(text) => inputHandler4(text, myIndex)}
                    />
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Degree/Certificate</Text>
                </View>
                <View style={styles.action}>
                    <TextInput
                        value={myValue.name}
                        placeholder="Degree Name"
                        style={styles.textInput}
                        autoCapitalize="none"
                        onChangeText={(text) => inputHandler5(text, myIndex)}
                    />
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Year</Text>
                </View>
                <View style={styles.action}>
                    <TextInput
                        value={myValue.year}
                        placeholder="Year"
                        style={styles.textInput}
                        autoCapitalize="none"
                        keyboardType='numeric'
                        onChangeText={(text) => inputHandler6(text, myIndex)}
                    />
                </View>
                <View style={{ marginLeft: 10, marginTop: 5 }}>
                    <TouchableOpacity
                        // onPress={() => deleteHandler2(key)}
                        style={{
                            height: 35, justifyContent: 'center', alignItems: 'center', width: '20%', padding: 1, backgroundColor: '#f10e0e', borderRadius: 5, paddingLeft: 10
                        }}
                    >
                        <Text style={{ fontSize: 13, color: 'white', fontWeight: 'bold' }}>Delete</Text>
                    </TouchableOpacity>
                </View>
            </>
        )
    });
    let BlankUni = <View></View>;
    if (uniset) {
        BlankUni = inputs.map((input, key) => (
            <View
                style={{ marginTop: 10 }}
            >
                <View style={{ margin: 10 }}>
                    <Text>Univercity/Collage/Institution</Text>
                </View >
                <View style={styles.action2}>
                    <TextInput placeholder={"Collage Name"} value={input.value} onChangeText={(text) => inputHandler1(text, key)}
                        style={styles.textInput}
                    />
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Degree/Certificate</Text>
                </View>
                <View style={styles.action2}>
                    <TextInput placeholder={"Degree"} value={inputsy[key].value} onChangeText={(text) => inputHandler2(text, key)}
                        style={styles.textInput}
                    />
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Year</Text>
                </View>
                <View style={styles.action2}>
                    <TextInput placeholder={"Year"} value={inputsz[key].value} onChangeText={(text) => inputHandler3(text, key)}
                        style={styles.textInput}
                    />
                </View>
                <View style={{ marginLeft: 10 }}>
                    <TouchableOpacity onPress={() => deleteHandler(key)}
                        style={{
                            height: 35, justifyContent: 'center', alignItems: 'center', width: '20%', padding: 1, backgroundColor: '#f10e0e', borderRadius: 5, paddingLeft: 10
                        }}
                    >
                        <Text style={{ fontSize: 13, color: 'white', fontWeight: 'bold' }}>Delete</Text>
                    </TouchableOpacity>
                </View>
            </View>
        ))
    }

    let otherLinks = Link.map((myValue, myIndex) => {
        //  console.log('myValue: ' + myValue.city_name)
        return (
            <>
                <View style={{ margin: 10 }}>
                    <Text>Others Link</Text>
                </View>
                <View style={styles.action}>
                    <TextInput
                        // placeholder="Your First name"
                        value={myValue.name}
                        style={styles.textInput}
                        autoCapitalize="none"
                        onChangeText={(text) => setLink(text)}
                    />
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Link</Text>
                </View>
                <View style={styles.action}>
                    <TextInput
                        value={myValue.link}
                        //  placeholder="Your First name"
                        style={styles.textInput}
                        autoCapitalize="none"
                        onChangeText={(text) => setLink(text)}
                    />
                </View>
                <View style={{ marginLeft: 10, marginTop: 5 }}>
                    <TouchableOpacity
                        // onPress={() => deleteHandler2(key)}
                        style={{
                            height: 35, justifyContent: 'center', alignItems: 'center', width: '20%', padding: 1, backgroundColor: '#f10e0e', borderRadius: 5, paddingLeft: 10
                        }}
                    >
                        <Text style={{ fontSize: 13, color: 'white', fontWeight: 'bold' }}>Delete</Text>
                    </TouchableOpacity>
                </View>
            </>
        )
    });

    let BlankUniLink = <View></View>;
    if (unisetlink) {
        BlankUniLink = inputs2.map((input, key) => (
            <View
                style={{ marginTop: 10 }}
            >
                <View style={{ margin: 10 }}>
                    <Text>Others Link Titel</Text>
                </View >
                <View style={styles.action2}>
                    <TextInput placeholder={"Others Link Titel"} value={input.value} onChangeText={(text) => inputHandler11(text, key)}
                        style={styles.textInput}
                    />
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Link</Text>
                </View>
                <View style={styles.action2}>
                    <TextInput placeholder={"Link"} value={inputsa[key].value} onChangeText={(text) => inputHandler22(text, key)}
                        style={styles.textInput}
                    />
                </View>
                <View style={{ marginLeft: 10 }}>
                    <TouchableOpacity onPress={() => _deleteHandler2(key)}
                        style={{
                            height: 35, justifyContent: 'center', alignItems: 'center', width: '20%', padding: 1, backgroundColor: '#f10e0e', borderRadius: 5, paddingLeft: 10
                        }}
                    >
                        <Text style={{ fontSize: 13, color: 'white', fontWeight: 'bold' }}>Delete</Text>
                    </TouchableOpacity>
                </View>
            </View>
        ))
    }

    let LinksCreatBlankField = () => {
        //  alert("LinksCreatBlankField")
        //  console.log('myValue: ' + myValue.city_name)
        return (
            <>
                <View style={{ margin: 10 }}>
                    <Text>Others Link</Text>
                </View>
                <View style={styles.action}>
                    <TextInput
                        // placeholder="Your First name"
                        // value={myValue.name}
                        style={styles.textInput}
                        autoCapitalize="none"
                    // onChangeText={(text) => setLink(text)}
                    />
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Link</Text>
                </View>
                <View style={styles.action}>
                    <TextInput
                        // value={myValue.link}
                        //  placeholder="Your First name"
                        style={styles.textInput}
                        autoCapitalize="none"
                    //onChangeText={(text) => setLink(text)}
                    />
                </View>
            </>
        )
    };
    const RadioButton = props => {
        return (
            <TouchableOpacity style={styles.circle} onPress={props.onPress}>
                {props.checked ? (<View style={styles.checkedCircle} />) : (<View />)}
            </TouchableOpacity>
        )
    };
    const radioHandler = () => {
        //logic goes here
        if (femaleCheck) {
            setFemaleCheck(false); //for avoiding multiple checks at a time;
            setKey('male');
            setOption1(true);
            setData11('male')
            // value=
        } else {
            setOption1(false);
            setFemaleCheck(true)
            setKey('female');
            setData11('female')

        }
    }
    const radioHandler2 = () => {
        if (option1) {
            setOption1(false);
            setKey('female');
            setFemaleCheck(true);
            setData11('female')
            // value=
        } else {
            setKey('male');
            setFemaleCheck(false);
            setOption1(true)
            setData11('male')

        }
    }

    // var PROFILEPICK = {resdata.profile_picture}
    return (

        <View style={styles.container}>
            <Spinner
                //visibility of Overlay Loading Spinner
                visible={loading}
                color={"black"}
                overlayColor={'rgba(255,255,255, 1)'}
                //Text with the Spinner
                textContent={'Loading...'}
            //Text style of the Spinner Text
            //textStyle={styles.spinnerTextStyle}
            />
            <StatusBar backgroundColor='#961b37' barStyle="light-content" />
            <ScrollView>
                {/* <Text>icon1</Text> */}
                <View style={{ padding: 15, }}>
                    <Text style={{ fontSize: 22, fontWeight: '800' }}>Edit Instructor's Profile</Text>
                </View>
                {/* <View>
                <MultiSelect
          hideTags
          items={items}
          uniqueKey="id"
          ref={(component) => { multiSelect = component }}
          onSelectedItemsChange={onSelectedItemsChange}
          selectedItems={selectedItems}
          selectText="Pick Items"
          searchInputPlaceholderText="Search Items..."
          onChangeInput={ (text)=> console.log(text)}
          altFontFamily="ProximaNova-Light"
          tagRemoveIconColor="#CCC"
          tagBorderColor="#CCC"
          tagTextColor="#CCC"
          selectedItemTextColor="#CCC"
          selectedItemIconColor="#CCC"
          itemTextColor="#000"
          displayKey="name"
          searchInputStyle={{ color: '#CCC' }}
          submitButtonColor="#CCC"
          submitButtonText="Submit"
        />
                </View>
                <View>
          {multiSelect.getSelectedItemsExt(selectedItems)}
        </View> */}
                {/* <View>
                    <FlatList
                        data={resdata3}
                        renderItem={({ item }) => (
                            <>
                                <Text>{item.id}</Text>
                                <TouchableOpacity onPress={() => selectState(item.id)}>
                                    <Text style={{ fontSize: 17 }}>{item.id}.{item.state}</Text>
                                </TouchableOpacity>
                            </>
                        )}
                    

                    />
                </View> */}

                {/* <View style={{ borderRadius: 5, borderColor: "#131313", borderWidth: .5, color: "#131313", marginVertical: 10, marginHorizontal: 10 }}>
                    <Picker
                        selectedValue={data2.selectLabel}
                        mode="dropdown"
                        onValueChange={(value) => setSelectLabel({ selectLabel: value })}
                        onPress={selectState()}
                    >
                        {myUsers}
                    </Picker>
                </View> */}

                {/* <View style={{ borderRadius: 5, borderColor: "#131313", borderWidth: .5, color: "#131313", marginVertical: 10, marginHorizontal: 10 }}>
                    <Picker
                        selectedValue={resdata3}
                        onValueChange={Show2.bind()}
                    >
                        <Picker.Item label={item.state} value="0" color=""></Picker.Item>
                        <Picker.Item label="java" value="2000"></Picker.Item>
                    </Picker>
                </View> */}
                <View style={styles.container}>
                    <View style={{ alignItems: 'center' }}><Text style={{ color: "#5a00b5", }}>________________</Text><Text style={{ color: "#000000", paddingTop: 20, fontSize: 9 }}>Basic Information</Text><View style={{ position: 'absolute' }}><View style={{ height: 33, width: 33, backgroundColor: "#5a00b5", borderRadius: 50, paddingLeft: 3.7, paddingTop: .6 }}><Icon name="lock-closed-outline" size={25} color="#ffffff" /></View></View></View>

                    <View style={{ alignItems: 'center' }}><Text style={{ color: "#749182", }}>________________</Text><Text style={{ color: "#000000", paddingTop: 20, fontSize: 9 }}>Subject/Skills</Text><View style={{ backgroundColor: "749182", paddingBottom: 20, position: 'absolute' }}><View style={{ height: 33, width: 33, backgroundColor: "#b5b5b5", borderRadius: 50, paddingLeft: 4, paddingTop: 3.7 }}><Icon name="cog" size={25} color="#ffffff" backgroundColor="#ffffff" /></View></View></View>

                    <View style={{ alignItems: 'center' }}><Text style={{ color: "#749182", }}>________________</Text><Text style={{ color: "#000000", paddingTop: 20, fontSize: 9 }}>Accounting Information</Text><View style={{ backgroundColor: "749182", paddingBottom: 20, position: 'absolute' }}><View style={{ height: 33, width: 33, backgroundColor: "#b5b5b5", borderRadius: 50, paddingLeft: 4.9, paddingTop: 3.9 }}><Icon name="calendar" size={22} color="#ffffff" /></View></View></View>

                    <View style={{ alignItems: 'center' }}><Text style={{ color: "#749182", }}>________________</Text><Text style={{ color: "#000000", paddingTop: 20, fontSize: 9 }}>Get Certified</Text><View style={{
                        backgroundColor: "2cd383", paddingBottom: 20,
                        position: 'absolute'
                    }}><View style={{ height: 33, width: 33, backgroundColor: "#b5b5b5", borderRadius: 50, paddingLeft: 6.5, paddingTop: 6.5 }}><IconIcon name="check" size={20} color="#ffffff" /></View></View></View>
                </View>
                <View style={{ margin: 10 }}>
                    <Text>First Name</Text>
                </View>
                <View style={styles.action}>
                    <TextInput
                        placeholder="First Name"
                        style={styles.textInput}
                        value={Fname}
                        autoCapitalize="none"
                        onChangeText={(text) => setFname(text)}
                    />
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Last Name</Text>
                </View>
                <View style={styles.action}>
                    <TextInput
                        placeholder="Last Name"
                        style={styles.textInput}
                        value={Lname}
                        autoCapitalize="none"
                        onChangeText={(text) => setLname(text)}
                    />
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Nick Name(Max 15 Latter)</Text>
                </View>
                <View style={styles.action}>
                    <TextInput
                        placeholder="Nick Name"
                        style={styles.textInput}
                        value={Nickname}
                        autoCapitalize="none"
                        onChangeText={(text) => setNickname(text)}
                    />
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Phone No</Text>
                </View>
                <View style={{ borderWidth: 0.6, borderRadius: 1.5, borderColor: '#505050', marginHorizontal: 10, height: 70 }}>
                    <PhoneInput style={styles.phonestylee}
                        // ref={phoneInput}
                        // defaultValue={value}
                        // defaultCode="DM"
                        value={PhoneNo}
                        onChangeText={(text) => {
                            setPhoneNo(text);
                        }}
                        // onChangeFormattedText={(number) => {
                        //     { resdata.phone_no } (number)
                        // }}
                        placeholder={resdata.phone_no}
                    // value={resdata.phone_no}
                    //textContainerStyle="height=10"
                    />
                    {/* <PhoneInput /> */}

                    {/* <PhoneInput
                        placeholder="Enter phone number"
                        value={value}
                        onChange={setValue} /> */}
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Email</Text>
                </View>
                <View style={styles.action}>
                    <TextInput
                        placeholder="abcd@gmail.com"
                        style={styles.textInput}
                        autoCapitalize="none"
                        value={resdata.email}
                        onChangeText={(val) => emailName(val)}
                    />
                </View>
                <View style={{ margin: 10 }}>
                    <Text>D.O.B</Text>
                </View>
                <View style={{ margin: 10 }}>
                    <TouchableOpacity style={{
                        // height: 45, width: '100%',
                        // padding: 5,
                        // // marginRight: 10,
                        // // marginLeft: 10,
                        // borderWidth: 0.6,
                        // borderRadius: 5,
                        // borderColor: '#505050',
                    }}>
                        <View style={{ flexDirection: 'row', alignItems: 'stretch', justifyContent: 'space-between' }}>
                            <Text style={{ fontSize: 13, color: '#898b8a', alignItems: 'flex-start', justifyContent: 'flex-start', paddingTop: 8, paddingLeft: 5 }}>

                                {/* {resdata.dob} */}

                            </Text>
                            <View styles={{ paddingTop: 10, }}
                                onPress={showDatePicker}
                            >
                                <DateTimePickerModal
                                    isVisible={isDatePickerVisible}
                                    style={{ width: 375 }}
                                    date={Date} // Initial date from state
                                    mode="date" // The enum of date, datetime and time
                                    // placeholder="select date"
                                    format="DD-MM-YYYY"
                                    confirmBtnText="Confirm"
                                    cancelBtnText="Cancel"
                                    onDateChange={(date) => {
                                        handleConfirm(date);
                                    }}

                                />
                                {/* <Icon name="calendar" size={23} color="#898b8a"
                                    onPress={showDatePicker}
                                /> */}
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Gender</Text>
                </View>
                {/* <View style={{ flexDirection: 'column' }}>
                        <RadioButton.Group style={{ flexDirection: 'column' }}>
                            <View style={{ flexDirection: 'row', paddingLeft: 50 }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <RadioButton value="Male" />
                                    <Text style={{ paddingTop: 8 }}>First</Text>
                                </View>
                                <View style={{ flexDirection: 'row' }}>
                                    <RadioButton value="Female" />
                                    <Text style={{ paddingTop: 8 }}>Second</Text>
                                </View>
                            </View>
                        </RadioButton.Group>
                    </View> */}
                <View style={{ flexDirection: 'row', paddingLeft: 50 }}>
                    {/* <View style={{ flexDirection: 'row' }}>
                        <RadioButton
                            value="Male"
                            status={checked === 'Male' ? 'checked' : 'unchecked'}
                            // onPress={(val) => alert(setChecked('Male',val))}
                            onPress={() => setChecked('Male')}
                        // style={{ colour:'#900' }}
                        />
                        <Text style={{ paddingTop: 8 }}>Male</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <RadioButton
                            value="Female"
                            status={checked === 'Female' ? 'checked' : 'unchecked'}
                            // onPress={(val) => alert(setChecked('Female',val))}
                            onPress={() => setChecked('Female')}
                        />
                        <Text style={{ paddingTop: 8 }}>Female</Text>
                    </View>
                    <RadioButtonRN
                        data9={data9.label}
                        selectedBtn={(data9) => console.log(data9)}
                    /> */}
                    <Text>Male:</Text>
                    <RadioButton checked={option1} onPress={radioHandler} />
                    <Text>Female:</Text>
                    <RadioButton checked={femaleCheck} onPress={radioHandler2} />
                </View>
                {/* <View style={{ margin: 10 }}>
                    <Text>Language Spek (Max 3)</Text>
                </View> */}
                {/* <View style={{ borderRadius: 5, borderColor: "#131313", borderWidth: .5, color: "#131313", marginVertical: 10, marginHorizontal: 10 }}>
                    <Picker
                        selectedValue={data4.selectLabel4}
                        mode="dropdown"
                        onValueChange={(value) => setSelectLabel4({ selectLabel4: value })}
                    >
                        {language}
                    </Picker>
                </View> */}
                <View style={{ margin: 10 }}>
                    <MultiSelect
                        hideTags
                        items={langSpacific2}
                        uniqueKey="id"
                        onSelectedItemsChange={onSelectedItemsChange}
                        selectedItems={langnew}
                        selectText="Language Spek (Max 3)"
                        searchInputPlaceholderText="Search Language..."
                        onChangeInput={(text) => (text)}
                        // languageSpokenName
                        tagRemoveIconColor="#CCC"
                        tagBorderColor="#CCC"

                        tagTextColor="#CCC"
                        selectedItemTextColor="#dc2359"
                        selectedItemIconColor="#CCC"
                        itemTextColor="#900"
                        displayKey="name"
                        searchInputStyle={{ color: '#CCC' }}
                        submitButtonColor="#dc2359"
                        submitButtonText="Submit"
                        hideSubmitButton='true'
                    />

                </View>
                {/* <View>
                {MultiSelect.getSelectedItemsExt(selectedItems)}
                </View> */}
                {ShowTeacherLanguage}
                <View style={{ margin: 10 }}>
                    <Text>Experience (in years)</Text>
                </View>
                <View style={styles.action}>
                    <TextInput
                        placeholder="Experience"
                        style={styles.textInput}
                        autoCapitalize="none"
                        value={Experience}
                        onChangeText={(text) => setExperience(text)}
                    />
                </View>
                {/* <Picker /> */}
                {/* <View style={{ borderRadius: 5, borderColor: "#131313", borderWidth: .5, color: "#131313", marginVertical: 10, marginHorizontal: 10 }}>
                    <Picker
                        selectedValue={data2.selectLabel}
                        onValueChange={Show.bind()}
                    >
                        <Picker.Item label="select one" value="0" color=""></Picker.Item>
                        <Picker.Item label="java" value="2000"></Picker.Item>
                        <Picker.Item label="node" value="5000"></Picker.Item>
                        <Picker.Item label="mongo db" value="6000"></Picker.Item>
                    </Picker>
                </View> */}
                <View style={styles.lineStyle} />
                <View style={{ margin: 10 }}>
                    <Text style={{ fontSize: 20, fontWeight: 'bold' }}>Qualification</Text>
                </View>
                {Qual}
                {/* <Picker /> */}
                {/* <View style={{ borderRadius: 5, borderColor: "#131313", borderWidth: .5, color: "#131313", marginVertical: 10, marginHorizontal: 10 }}>
                    <Picker
                        selectedValue={data2.selectLabel}
                        onValueChange={Show.bind()}
                    >
                        <Picker.Item label="select one" value="0" color=""></Picker.Item>
                        <Picker.Item label="1997" value="2000"></Picker.Item>
                        <Picker.Item label="1998" value="5000"></Picker.Item>
                        <Picker.Item label="1999" value="6000"></Picker.Item>
                    </Picker>
                </View> */}
                {BlankUni}

                <TouchableOpacity style={{
                    height: 35, justifyContent: 'center', alignItems: 'center', width: '15%',
                    padding: 1,
                    backgroundColor: '#fffff', borderRadius: 5, marginLeft: 10, marginTop: 10, marginBottom: 5, borderRadius: 5, borderWidth: 1, borderColor: '#5a287d'
                }}
                    onPress={addHandler}
                >
                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}
                        onPress={addHandler}
                    >
                        <Icon name="add" size={20} color="#9700e3" />
                        <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Add</Text>
                    </View>
                </TouchableOpacity>
                <View style={{ margin: 10, }}>
                    <Text>One Line About Me</Text>
                </View>
                <View style={styles.action}>
                    <TextInput
                        style={{ height: 60 }}
                        value={Aboutmeshort}
                        placeholder="One Line About Me"
                        style={styles.textInput}
                        autoCapitalize="none"
                        onChangeText={(text) => setAboutmeshort(text)}
                    />
                </View>
                <View style={{ margin: 10 }}>
                    <Text>About Me</Text>
                </View>
                <View style={styles.action}>
                    <TextInput
                        multiline
                        numberOfLines={4}
                        value={Aboutme}
                        placeholder="About Me"
                        style={{ height: 70 }}
                        autoCapitalize="none"
                        onChangeText={(text) => setAboutme(text)}
                    />
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Profile Photo</Text>
                </View>
                <View >
                    {/* {resdata.profile_picture} */}
                    <Image style={{ marginLeft: 10, height: 80, width: 80 }} source={{ uri: 'https://appleskool.com/preview/appleskool_code/storage/app/public/profile_picture/' + IMage }} />
                </View>
                <View style={{ margin: 10 }}>
                    <TouchableOpacity style={{
                        height: 45, width: '100%',
                        padding: 5,
                        backgroundColor: '#e6a000', borderRadius: 5
                    }}
                        onPress={takePhotoFromGallery}
                        value={IMage}
                    //  value={imagepath}
                    >
                        <View style={{ flexDirection: 'row', alignItems: 'stretch', justifyContent: 'space-between' }}>
                            <Text style={{ fontSize: 13, color: 'white', alignItems: 'flex-start', justifyContent: 'flex-start', paddingTop: 8 }}>
                                {/* {imagepath} */}
                                {IMage}
                            </Text>
                            <View style={{ paddingTop: 6, paddingRight: 10 }}>
                                <IconIcon name="upload" size={23} color="#ffffff" />
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Uplode Introductory Video</Text>
                </View>
                <View style={{ margin: 10 }}>
                    <TouchableOpacity style={{
                        height: 45, width: '100%',
                        padding: 5,
                        backgroundColor: '#e6a000', borderRadius: 5
                    }}
                        onPress={takeVideoFromGallery}
                        value={VIdeo}
                    //value={videopath}
                    >
                        <View style={{ flexDirection: 'row', alignItems: 'stretch', justifyContent: 'space-between' }}>
                            <Text style={{ fontSize: 13, color: 'white', alignItems: 'flex-start', justifyContent: 'flex-start', paddingTop: 8 }}>
                                {/* {videopath} */}
                                {VIdeo}
                            </Text>
                            <View style={{ paddingTop: 6, paddingRight: 10 }}>
                                <IconIcon name="upload" size={23} color="#ffffff" />
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={styles.lineStyle} />
                <View style={{ margin: 10 }}>
                    <Text style={{ fontSize: 17, fontWeight: 'bold' }}>Add Address</Text>
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Street</Text>
                </View>
                <View style={styles.action}>
                    <TextInput
                        placeholder="Street"
                        value={Street}
                        style={styles.textInput}
                        autoCapitalize="none"
                        onChangeText={(text) => setStreet(text)}
                    />
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Pin</Text>
                </View>
                <View style={styles.action}>
                    <TextInput
                        placeholder="Pin"
                        value={Pin}
                        style={styles.textInput}
                        autoCapitalize="none"
                        onChangeText={(text) => setPin(text)}
                    />
                </View>
                <View style={{ margin: 10 }}>
                    <Text>State</Text>
                </View>
                {/* <Picker /> */}
                <View style={{ height: 45, borderRadius: 5, borderColor: "#131313", borderWidth: .5, color: "#131313", marginVertical: 10, marginHorizontal: 10 }}>
                    <Dropdown
                        style={{ paddingStart: 10, height: 25, marginTop: -20 }}
                        data={pickerItem}
                        value={parseInt(data2.selectLabel)}
                        baseColor={'#ffffff'}
                        labelFontSize={0}
                        onChangeText={
                            (dropdownitemvalue) => {
                                selectState2(dropdownitemvalue)
                            }}

                    />

                </View>
                <View style={{ margin: 10 }}>
                    <Text>City</Text>
                </View>
                {/* <Picker /> */}
                <View style={{ height: 45, borderRadius: 5, borderColor: "#131313", borderWidth: .5, color: "#131313", marginVertical: 10, marginHorizontal: 10 }}>
                    <Dropdown
                        style={{ paddingStart: 10, height: 25, marginTop: -20 }}
                        data={pickerItem2} baseColor={'#ffffff'}
                        labelFontSize={0}
                        onChangeText={
                            (dropdownitemvalue) => {
                                setSelectLabel3({ selectLabel3: dropdownitemvalue })
                            }}
                        value={parseInt(data3.selectLabel3)}
                    />

                </View>
                <View style={{ margin: 10 }}>
                    <Text>Country</Text>
                </View>
                <View style={styles.action}>
                    <TextInput
                        value={resdata.country_id}
                        placeholder="Country"
                        style={styles.textInput}
                        autoCapitalize="none"
                        onChangeText={(val) => countryName(val)}
                    />
                </View>
                <View style={styles.lineStyle} />
                <View style={{ margin: 10 }}>
                    <Text style={{ fontSize: 17, fontWeight: 'bold' }}>My Link</Text>
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Youtube</Text>
                </View>
                <View style={styles.action}>
                    <TextInput
                        value={Youtube}
                        placeholder="Youtube"
                        style={{ height: 40 }}
                        autoCapitalize="none"
                        onChangeText={(text) => setYoutube(text)}
                    />
                    <View style={{ position: 'absolute', paddingLeft: 340, paddingTop: 7 }}>
                        <IconIconIcon name="youtube" size={21} color="#fc3536" />
                    </View>
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Twiter</Text>
                </View>
                <View style={styles.action}>
                    <TextInput
                        value={Twiter}
                        placeholder="Twiter"
                        style={styles.textInput}
                        autoCapitalize="none"
                        onChangeText={(text) => setTwiter(text)}
                    />
                    <View style={{ position: 'absolute', paddingLeft: 340, paddingTop: 7 }}>
                        <IconIcon name="twitter" size={21} color="#23b3f8" />
                    </View>
                    {/*                     
                    <View style={{ alignItems:'flex-end',justifyContent:'flex-end'}}>
                        <IconIcon name="twiter" size={23} color="#900" />
                    </View> */}
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Facebook</Text>
                </View>
                <View style={styles.action}>
                    <TextInput
                        value={Facebook}
                        placeholder="Facebook"
                        style={styles.textInput}
                        autoCapitalize="none"
                        onChangeText={(text) => setFacebook(text)}
                    />
                    <View style={{ position: 'absolute', paddingLeft: 340, paddingTop: 7 }}>
                        <IconIcon name="facebook-square" size={21} color="#46639c" />
                    </View>
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Blog</Text>
                </View>
                <View style={styles.action}>
                    <TextInput
                        value={Blog}
                        placeholder="Blog"
                        style={styles.textInput}
                        autoCapitalize="none"
                        onChangeText={(text) => setBlog(text)}
                    />
                    <View style={{ position: 'absolute', paddingLeft: 340, paddingTop: 7 }}>
                        <IconIconIcon name="blogger-b" size={21} color="#fdc107" />
                    </View>
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Website</Text>
                </View>
                <View style={styles.action}>
                    <TextInput
                        value={Website}
                        placeholder="Website"
                        style={styles.textInput}
                        autoCapitalize="none"
                        onChangeText={(text) => setWebsite(text)}
                    />
                    <View style={{ position: 'absolute', paddingLeft: 340, paddingTop: 7 }}>
                        <IconIcon name="globe" size={21} color="#90bb7c" />
                    </View>
                </View>

                {otherLinks}
                {/* {LinksCreatBlankField} */}
                {BlankUniLink}
                <TouchableOpacity style={{
                    height: 35, justifyContent: 'center', alignItems: 'center', width: '15%',
                    padding: 1,
                    backgroundColor: '#fffff', borderRadius: 5, marginLeft: 10, marginTop: 10, marginBottom: 5, borderRadius: 5, borderWidth: 1, borderColor: '#5a287d'
                }}
                    onPress={_addHandler2}
                >
                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}
                        onPress={_addHandler2}
                    >
                        <Icon name="add" size={20} color="#9700e3" />
                        <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Add</Text>
                    </View>
                </TouchableOpacity>
                {/* <View>
                    <View style={{ paddingTop: 30, marginRight: 5 }}>
                        <TouchableOpacity style={{
                            height: 35, justifyContent: 'center', alignItems: 'center', width: '20%',
                            padding: 1,
                            backgroundColor: '#fffff', borderRadius: 5, marginLeft: 10, borderRadius: 5, borderWidth: 1, borderColor: '#8c56c7'
                        }}
                        onPress={addHandler}
                        >
                            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                                <Icon name="add" size={20} color="#8c56c7" />
                                <Text style={{ fontSize: 13, color: '#8c56c7', fontWeight: 'bold' }}>Add</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View> */}
                <View>
                    <View style={{ flexDirection: 'row', paddingTop: 30, alignItems: 'flex-end', justifyContent: 'flex-end', marginRight: 5, marginBottom: 10 }}>
                        <TouchableOpacity style={{
                            height: 35, justifyContent: 'center', alignItems: 'center', width: '20%',
                            padding: 1,
                            backgroundColor: '#fffff', borderRadius: 5, marginLeft: 30, borderRadius: 5, borderWidth: 1, borderColor: '#8c56c7', marginRight: 15, marginLeft: 30
                        }} onPress={() => navigation.navigate('HomeScreen')}>
                            <Text style={{ fontSize: 13, color: '#8c56c7', fontWeight: 'bold' }}>Discard</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{
                            height: 35, justifyContent: 'center', alignItems: 'center', width: '33%', padding: 1, backgroundColor: '#d51c6d', borderRadius: 5
                        }}
                            //onPress={() => navigation.navigate("ScreenCreatProfileSubjectSkill")}
                            onPress={() => creatprofilebasicinfoupdate()}
                        >
                            <Text style={{ fontSize: 13, color: 'white', fontWeight: 'bold' }}>Save & Continue</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        </View>
        // {part2 ? <View><Text style={{ color:"#5a00b5" }}>_________________</Text><Text style={{ color:"#5a00b5" }}>abhijit</Text></View> : <View><Text style={{ color:"#2cd383" }}>______________</Text></View>}
        // {part3 ? <View><Text style={{ color:"#fffeff" }}>______________</Text></View> : <View><Text style={{ color:"#749182" }}>________________</Text><Text style={{ color:"#749182" }}>abhijit</Text></View>}
    );
};
export default ScreenCreateprofilebasicinformation;

const styles = StyleSheet.create({
    outerView: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        alignContent: 'center',
        //     flexDirection: 'row',
        //     alignItems: 'center',
        //     justifyContent: 'center',
        paddingTop: 10,
        paddingBottom: 20,
    },
    container: {
        flex: 1,
        backgroundColor: 'white',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 5,
        //paddingBottom: 10,
        // },
    },
    lineStyle: {
        borderWidth: .4,
        borderColor: 'black',
        margin: 15,
        marginTop: 25
    },
    action: {
        //flexDirection: 'row',
        marginRight: 10,
        marginLeft: 10,
        borderWidth: 0.6,
        borderRadius: 5,
        borderColor: '#505050',
        //padding: 5
    },
    textInput: {
        //flex: 1,
        //marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        color: '#05375a',
        height: 40
    },
    phonestyle: {
        height: 20,
        width: 100
    },
    // textContainerStyle{
    //     height:20
    // }
    circle: {
        height: 20,
        width: 20,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#ACACAC',
        alignItems: 'center', // To center the checked circle…
        justifyContent: 'center',
        marginHorizontal: 10
    },
    checkedCircle: {
        width: 14,
        height: 14,
        borderRadius: 7,
        backgroundColor: '#fa0562' // You can set it default or with yours one…
    },
    action2: {
        //flexDirection: 'row',
        marginRight: 10,
        marginLeft: 10,
        borderWidth: 0.6,
        borderRadius: 5,
        borderColor: '#505050',
        marginBottom: 5
        //padding: 5
    },
});
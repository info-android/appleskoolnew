import React, { Component, useState, useEffect } from 'react';
import {
    StyleSheet,
    Text,
    View, Dimensions,
    TextInput,
    TouchableOpacity,
    TouchableHighlight, AsyncStorage,
    Image, ScrollView, SafeAreaView
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
//import Card from "./Components/Card";
import CustomCarasole from './Components/CustomCarasole'
import ProgressBarSteps from './Components/ProgressBarSteps'
import CreatInstructorProfileAccount from '../Screen/CreatInstructorProfileAccount'
import Screencreateprofilebasicgetcertified from '../Screen/Components/Screencreateprofilebasicgetcertified'
import ScreenCreateprofilebasicinformation from '../Screen/Components/ScreenCreateprofilebasicinformation'
import ScreenCreatProfileSubjectSkill from '../Screen/Components/ScreenCreatProfileSubjectSkill'
//import AcordiamItems from './Components/AcordiamItems'
import Spinner from 'react-native-loading-spinner-overlay';
import axios from 'axios';
const InstructorProfilePage = ({ navigation, route }) => {
    const [loading, setLoading] = React.useState(false);
    const [token, setToken] = React.useState('');
    const [user, setUser] = useState('');
    const [results, setresults] = useState('');
    const [review, setreview] = useState([]);
    const [subcategory, setSubcategory] = React.useState([
        { id: 0, name: 'Total Student', data: '1,456', img: require('../../icon/group-min.png') },
        { id: 1, name: 'Experience', data: '10 Years', img: require('../../icon/reading-book-min.png') },
        { id: 2, name: 'Taught', data: '40 Hours', img: require('../../icon/clock-min.png') },
        { id: 3, name: 'Past Traning', data: '18', img: require('../../icon/calendar-min.png') },
        { id: 4, name: 'Active Traning', data: '10', img: require('../../icon/first-aid-kit-min.png') },
    ])

    const { Data } = route.params
    //alert("data>>>>>>>>" + JSON.stringify(Data))
    useEffect(() => {
        if (token) {
            HomeApi()
        } else {
            AsyncStorage.getItem('UID123', (err, result) => {
                setToken(result);
                // setTimeout(()=>{
                //    
                // }, 3000);
            })
        }
    }, [token])
    const HomeApi = () => {

        setLoading(true);
        // setLoding(false)
        // alert(data.email+data.password)
        axios.defaults.baseURL = 'https://appleskool.com/preview/appleskool_code/api';
        axios.defaults.headers.post['Content-Type'] =
            'application/json;charset=utf-8';
        axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';
        axios.defaults.headers.post['Access-Control-Allow-Headers'] = '*';
        //alert(global.userToken)
        const bodyParameters = {
        };
        axios
            .post(
                'https://appleskool.com/preview/appleskool_code/api/instructor/public-profile', {
                "jsonrpc": "2.0",
                "params": {
                    "id": Data,
                }
            }
            )
            .then(


                res => {
                    setLoading(false);
                    console.log(JSON.stringify(res.data.user));
                    //global.Me = me.data.firstname + me.data.lastname;
                    setUser(res.data.user)
                    setresults(res.data)
                    setreview(res.data.reviews)

                },
                error => {
                    setLoading(false);
                   // alert("Something went wrong. Please try again")
                },
            );
    };
    return (
        <View style={styles.container}>
            <ScrollView>
                {/* <AcordiamItems/> */}
                {/* <ProgressBarSteps part2></ProgressBarSteps> */}
                {/* <CreatInstructorProfileAccount/> */}
                {/* <Screencreateprofilebasicgetcertified part3></Screencreateprofilebasicgetcertified> */}
                {/* <ScreenCreateprofilebasicinformation part1></ScreenCreateprofilebasicinformation> */}
                {/* <ScreenCreatProfileSubjectSkill part2></ScreenCreatProfileSubjectSkill> */}
                <View style={{ height: 370, backgroundColor: '#f4efe9', position: 'relative', }}>
                    <Spinner
                        //visibility of Overlay Loading Spinner
                        visible={loading}
                        color={"black"}
                        overlayColor={'rgba(255,255,255, 1)'}
                        //Text with the Spinner
                        textContent={'Loading...'}
                    //Text style of the Spinner Text
                    //textStyle={styles.spinnerTextStyle}
                    />
                    <View style={{ height: 250, margin: 14, }}>
                        <View style={{ flexDirection: 'row' }}>
                            <Image source={{ uri: 'https://appleskool.com/preview/appleskool_code/storage/app/public/profile_picture/' + user.profile_picture }} style={{ width: 115, height: 115, borderColor: '#f4efe900', borderWidth: 1, borderRadius: 10 }} />
                            <View style={{ flexDirection: 'column', marginLeft: 15, }}>
                                <Text style={{ color: '#3c3b37', fontWeight: 'bold', fontSize: 17 }}>
                                    {user.fname + " " + user.lname}
                                </Text>
                                <View style={{ marginTop: 10, flexDirection: 'row', alignItems: 'center' }} >
                                    <View style={{ backgroundColor: '#e6a000', paddingRight: 5, paddingTop: 3, paddingBottom: 3, borderRadius: 2, flexDirection: 'row', alignItems: 'center' }} >
                                        <Icon name='md-star' size={9} style={{ marginLeft: 4, color: 'white', marginRight: 5 }} />
                                        <Text style={{ color: 'white', fontSize: 9 }}>{user.avg_rating}</Text>
                                    </View>
                                    <Text style={{ fontFamily: 'roboto-regular', fontSize: 11, paddingLeft: 10, width: 140 }}>Rating({user.total_rating})</Text>
                                </View>
                                <Text style={{ marginTop: 8, fontSize: 13, fontFamily: 'roboto-regular', height: 40, width: '95%', }} >{user.about_me_shrt}</Text>
                                <Text style={{ marginTop: -5, fontSize: 13, fontFamily: 'roboto-regular', width: '90%', }} >Language Known:English,Hindi</Text>
                            </View>
                        </View>
                        <View style={{ flexWrap: "wrap", flex: 1, flexDirection: 'row', marginTop: 10, }} numberOfLines={2}>

                            <TouchableOpacity style={styles.buttonContainerSub}  >
                                <Image source={require('../../icon/group-min.png')} size={5} style={styles.inputIcon} />
                                <Text style={styles.loginText}>Total Student:{results.total_student}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.buttonContainerSub}  >
                                <Image source={require('../../icon/group-min.png')} size={5} style={styles.inputIcon} />
                                <Text style={styles.loginText}>Total Hours:{results.total_hours}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.buttonContainerSub}  >
                                <Image source={require('../../icon/group-min.png')} size={5} style={styles.inputIcon} />
                                <Text style={styles.loginText}>Active Training:{results.active_trainings}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.buttonContainerSub}  >
                                <Image source={require('../../icon/group-min.png')} size={5} style={styles.inputIcon} />
                                <Text style={styles.loginText}>Past Training:{results.past_trainings}</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ flexDirection: 'row', paddingTop: 108, }}>
                            <TouchableOpacity style={{
                                height: 35, justifyContent: 'center', alignItems: 'center', width: '33%',
                                padding: 5,
                                backgroundColor: '#5a287d', borderRadius: 5
                            }}>
                                <Text style={{ fontSize: 13, color: 'white', fontWeight: 'bold' }}>Add To Favorite</Text>
                            </TouchableOpacity>
                            <Text style={{ color: '#d73c5f', height: 35, justifyContent: 'center', textAlign: 'center', fontFamily: 'roboto-medium', marginTop: 7, marginLeft: 20 }}>Share this Training</Text>
                            <Image source={require('../../icon/facebook-min.png')} size={20} style={styles.inputIconFace} />
                            <Image source={require('../../icon/twitter-min.png')} size={20} style={styles.inputIconFace} />
                            <Image source={require('../../icon/google-plus-min.png')} size={20} style={styles.inputIconFace} />
                        </View>
                        <View>
                            {/* <View style={styles.circle} /> */}
                        </View>
                    </View>
                </View >
                {/* <View style={{ marginTop: -70 }}> */}
                {/* style={{ height: '95%', width: '95%', zIndex: 1000, flex: 1, flexWrap: 'wrap', elevation: 10, marginTop: -70, marginLeft: 10, marginRight: 10,}} */}
                <View style={{
                    //     borderWidth: 0,
                    //     borderRedius: 0,
                    //     borderColor: '#ddd',
                    //     borderBottomWidth: 0,
                    //     shadowColor: '#ff00b8',
                    //     shadowOffset: { width: 10, height: 20 },
                    //     shadowOpacity: 0.4,
                    //     shadowRadius: 2,
                    //     elevation: 7,
                    //     marginHorizontal: 20,
                    //     //marginRight: 25,
                    //    // margintop: 25,
                    //     marginBottom: 20,
                    //     backgroundColor:"#faf9f9",
                    borderWidth: 0,
                    borderRedius: 0,
                    borderColor: '#ddd',
                    borderBottomWidth: 0,
                    shadowColor: '#ff00b8',
                    shadowOffset: { width: 10, height: 20 },
                    shadowOpacity: 0.4,
                    shadowRadius: 2,
                    elevation: 7,
                    marginHorizontal: '5%',
                    //marginLeft: 25,
                    //marginRight: 25,
                    // marginHorizontal:5,
                    marginTop: -70,
                    marginBottom: 20,
                    padding: 10,
                    //paddingRight: 10,
                    backgroundColor: "#faf9f9",
                }} >
                    <View style={{ flexDirection: 'column' }}>
                        <Text style={{ fontFamily: 'roboto-bold', color: '#3b3a36', fontSize: 17, paddingLeft: 20, paddingTop: 30, width: 140 }}>About Me</Text>
                        <Text style={{ fontFamily: 'roboto-regular', lineHeight: 20, color: '#40413c', fontSize: 13, paddingHorizontal: 20, paddingVertical: 20, textAlign: 'justify' }}>{user.about_me}</Text>
                        <Text style={{ fontFamily: 'roboto-bold', color: '#661eb5', fontSize: 14, paddingLeft: 20, marginBottom: 35, }}>read more+</Text>
                    </View>
                </View>
                {/* sample */}
                {/* sample */}
                {/* </View> */}

                <View style={{ flexDirection: 'column', width: '95%', marginLeft: 16, marginRight: 10, marginTop: 20 }}>
                    <Text style={{ fontWeight: 'bold', fontSize: 22, marginLeft: 8, fontFamily: 'roboto-bold' }}>Upcoming Training Offered By {user.fname + " " + user.lname}</Text>
                    <Image source={require('../../icon/color-border.png')} style={{ width: '45%', height: 4, marginTop: 5, }} />
                    <Text style={{ fontSize: 11, marginTop: 20, marginHorizontal: 18, fontFamily: 'roboto-regular' }}>Lorem ipsum dolor sit amet consectetur adip text scing elit sed do eiusmod tempor labore</Text>
                </View>
                <View style={{ marginTop: 20 }}>
                    <CustomCarasole />
                    {/* <Text style={{ fontWeight: 'bold', fontSize: 18, marginLeft: 8, fontFamily: 'roboto-bold' }}>Upcoming Training Offered By Rabin Chatterjee Training Offered By Rabin Chatterjee</Text>
                       <View style={{ flexDirection: 'row', marginTop: 5, marginBottom: 5 }}>
                    <Image source={require('../../icon/calendar-min.png')} style={{ width: 18, height: 18, marginRight: 25 }} />
                    <Text style={styles.loginText3}>10 Total Sessions</Text>
                  </View> */}
                </View>
                <View style={{ flexDirection: 'column', marginBottom: 20, width: '95%', marginLeft: 10, marginRight: 10, marginTop: 20 }}>
                    <Text style={{ fontWeight: 'bold', fontSize: 18, marginLeft: 8, fontFamily: 'roboto-bold' }}>Reviews</Text>
                    <Image source={require('../../icon/color-border.png')} style={{ width: '20%', height: 8, marginTop: 5, marginLeft: 5 }} />
                    <View style={{ flexWrap: "wrap", flex: 1, flexDirection: 'column', marginRight: 5 }} numberOfLines={2}>
                        {review.map((item, key) => (
                            <View style={{ flexDirection: 'column' }}>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <Image source={{ uri: 'https://appleskool.com/preview/appleskool_code/storage/app/public/profile_picture/' + item.user.profile_picture }} style={{ width: 60, height: 60, borderColor: '#d73b60', borderWidth: 2, borderRadius: 50 }} />
                                    <View style={{ flexDirection: 'column', marginLeft: 15, }}>
                                        <Text style={{ color: '#3c3b37', fontWeight: 'bold', fontSize: 17 }}>
                                            {item.user.fname + " " + item.user.lname}
                                        </Text>
                                        <View style={{ flexDirection: 'row', marginTop: 5, marginBottom: 5 }}>
                                            <View style={{ alignItems: 'center', marginLeft: 2, marginRight: 5, borderRadius: 2, flexDirection: 'row', }}>

                                                <Icon name='md-star' size={13} style={{ marginLeft: 1, color: '#e6a000', marginRight: 1 }} />
                                                <Icon name='md-star' size={13} style={{ marginLeft: 1, color: '#e6a000', marginRight: 1 }} />
                                                <Icon name='md-star' size={13} style={{ marginLeft: 1, color: '#e6a000', marginRight: 1 }} />
                                                <Icon name='md-star' size={13} style={{ marginLeft: 1, color: '#e6a000', marginRight: 1 }} />
                                                <Icon name='md-star' size={13} style={{ marginLeft: 1, color: '#e6a000', marginRight: 1 }} />
                                            </View>
                                            <Image source={require('../../icon/calendar-min.png')} style={{ width: 13, height: 13, marginRight: 10 }} />
                                            <Text style={{ fontSize: 12 }}>{item.created_at}</Text>
                                        </View>
                                    </View>
                                </View>
                                <Text style={{ marginTop: 8, fontSize: 13, flexWrap: 'wrap', fontFamily: 'roboto-regular', }} >{item.comment}</Text>
                                <Text style={{ fontFamily: 'roboto-bold', color: '#661eb5', fontSize: 14, marginBottom: 10, marginTop: 3 }}>read more+</Text>
                                <Text style={{ fontStyle: 'italic', marginTop: 5, marginBottom: 10, fontWeight: 'bold', color: '#e9e9e9', width: '100%' }}>_______________________________________________________</Text>
                            </View>
                        ))}
                    </View>
                </View>
            </ScrollView>
        </View>
    );
};
export default InstructorProfilePage;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        // alignItems: 'center',
        // justifyContent: 'center',
        // alignContent: 'center'
    },
    buttonContainerSub: {

        height: 35,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 5,
        width: '31%',
        borderRadius: 30,
        padding: 5,
        margin: 3,
        borderColor: 'white',
        borderWidth: 1,
        backgroundColor: 'white'
    },
    inputIcon: {
        width: 12,
        height: 12,
        marginLeft: 1,

        marginRight: 5,
        justifyContent: 'center'
    },
    loginText: {
        fontSize: 10
    },
    inputIconFace: {
        width: 25,
        height: 25,
        marginLeft: 10,
        marginTop: 5,
        justifyContent: 'center'
    },
    circle: {
        height: 100,
        width: 100,
        borderRadius: 50,
        position: 'absolute',
        top: 150,
        right: 10,
        elevation: 10,
        backgroundColor: 'yellow',
    },
});
import React, { Component, useState, useEffect } from 'react';
import {
  StyleSheet,
  Text,
  View, Dimensions,
  TextInput,
  TouchableOpacity,
  TouchableHighlight,
  ImageBackground,
  Image, ScrollView, StatusBar, AsyncStorage
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { Card } from "react-native-elements";
import Spinner from 'react-native-loading-spinner-overlay';
import axios from 'axios';
const screenWidth = Math.round(Dimensions.get('window').width);

const HomePage = ({ navigation }) => {
  const [token, setToken] = React.useState('');

  const [loading, setLoading] = useState(false);
  const [images, setImages] = React.useState([
    require('../../icon/banner1.png'),
    require('../../icon/banner2.png'),
    require('../../icon/banner3.png'),
  ])
  const [traing, setTraing] = React.useState([
    require('../../icon/featured-training-img.png'),
    require('../../icon/featured-training-img2.png'),
    require('../../icon/featured-training-img3.png'),
  ])
  const [instructorImage, setInstructorImage] = React.useState([
    require('../../icon/instructor1.png'),
    require('../../icon/instructor2.png'),
    require('../../icon/instructor3.png'),
  ])
  const [catdata, setDataCat] = useState([]);
  const [teacherdata, setDataTeacher] = useState([]);
  const [trainingdata, setDataTraining] = useState([]);
  useEffect(() => {
    if (token) {
      HomeApi()
    } else {
      AsyncStorage.getItem('UID123', (err, result) => {
        setToken(result);
        // setTimeout(()=>{
        //    
        // }, 3000);
      })
    }
  }, [token])
  const HomeApi = () => {

    setLoading(true);
    // setLoding(false)
    // alert(data.email+data.password)
    axios.defaults.baseURL = 'https://appleskool.com/preview/appleskool_code/api';
    axios.defaults.headers.post['Content-Type'] =
      'application/json;charset=utf-8';
    axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';
    axios.defaults.headers.post['Access-Control-Allow-Headers'] = '*';
    //alert(global.userToken)
    const bodyParameters = {
    };
    axios
      .post('https://appleskool.com/preview/appleskool_code/api/home',
        bodyParameters,
        {
          headers: {
            Authorization: `Bearer ${token}`,
            // Authorization: `Bearer  ${userToken}`,global.userToken
          },
        },
      )
      .then(


        res => {
          setLoading(false);
          console.log(JSON.stringify(res.data));
          //global.Me = me.data.firstname + me.data.lastname;
          setDataTraining(
            res.data.trainings
          );
          setDataTeacher(
            res.data.featured_teachers
          );
          setDataCat(
            res.data.popular_categories
          );


        },
        error => {
          setLoading(false);
        //  alert("Something went wrong. Please try again")
        },
      );
  };



  let categories = catdata.map((myValue, myIndex) => {
    //alert('myValue: ' + myValue.name)
    return (
      <>
        <TouchableOpacity style={styles.buttonContainer} onPress={() => navigation.navigate('Search')}>
          <Image source={require('../../icon/it_software.png')} size={10} style={styles.inputIcon} />
          <Text style={styles.loginText}>{myValue.name}</Text>
        </TouchableOpacity>
      </>
    )
  });

  return (
    <View style={styles.container}>
      <Spinner
        //visibility of Overlay Loading Spinner
        visible={loading}
        color={"black"}
        overlayColor={'rgba(255,255,255, 1)'}
        //Text with the Spinner
        textContent={'Loading...'}
      //Text style of the Spinner Text
      //textStyle={styles.spinnerTextStyle}
      />
      <StatusBar backgroundColor='#961b37' barStyle="light-content" />

      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={{ width: '100%', marginTop: 5, }}>
          <ScrollView style={{ margin: 2, }} showsHorizontalScrollIndicator={false} horizontal>
            {images.map((item, key) => {
              return (
                <View key={key} >
                  <TouchableOpacity style={{ width: '95%', height: 175, marginTop: 2, marginRight: 10 }} >
                    <View style={{ justifyContent: 'center' }}>
                      <View style={{ height: 175, width: 315, justifyContent: 'center' }}>
                        <Image source={item} style={styles.cardImage} />
                        <View style={styles.overlay} >
                          <Text style={styles.banner1stText}>Learn on your schedule</Text>
                          <Text style={styles.banner2stText}>Study any topic,anytime. Explore thousands of Trainings starting at INR 455 each.</Text>
                          <Text style={styles.banner3stText}>Get Started Now</Text>
                        </View>
                      </View>
                    </View>
                  </TouchableOpacity>
                </View>
              )
            })}
          </ScrollView>
        </View>
        <View style={styles.category}>
          <Text style={{ fontWeight: 'bold', fontSize: 18, marginLeft: 10 }}>Popular Categories</Text>
          <Text style={{ fontWeight: '100', marginTop: 5, marginLeft: 20 }}>See all</Text>
        </View>
        <ScrollView style={{ margin: 2, }} showsHorizontalScrollIndicator={false} horizontal>
          <View style={{ flexDirection: 'row' }}>
            {categories}
          </View>
        </ScrollView>
        <View style={styles.category}>
          <View style={{ flexDirection: 'column' }}>
            <Text style={{ fontWeight: 'bold', fontSize: 18, marginLeft: 8 }}>Featured Training</Text>
            <Image source={require('../../icon/color-border.png')} style={{ width: '45%', height: 4, marginTop: 5, marginLeft: 5 }} />
          </View>
          <Text style={{ fontWeight: '100', marginTop: 5, }}>See all</Text>
        </View>
        <ScrollView style={{ margin: 2, }} showsHorizontalScrollIndicator={false} horizontal>
          {
            trainingdata.map((item, key) => (
              <View key={key} >
                <TouchableOpacity style={{ width: 230, height: 350, marginTop: 2, marginLeft: 10, marginRight: 17, }} onPress={() => navigation.navigate('TrainingDetailsPostBooking', { Data: item.id })}>
                  <View >
                    <View style={{ height: 200, }}>
                      <ImageBackground
                        style={{
                          width: '100%',
                          height: '100%',

                          borderRadius: 10,
                        }}
                        source={
                          require('../../icon/featured-training-img3.png') //Indicator
                        }>
                        <Image source={{ uri: 'https://appleskool.com/preview/appleskool_code/storage/app/public/training/images/' + item.training_image }} style={styles.cardImage2} />
                      </ImageBackground>
                    </View>
                    <Text style={{ margin: 2, fontSize: 15, fontWeight: 'bold' }}>{item.topic}</Text>
                    <Text style={{ marginLeft: 2 }}>{item.category.name}</Text>
                    <View style={{ width: 100, borderRadius: 2, flexDirection: 'row', alignItems: 'center' }}>
                      <View style={{ backgroundColor: '#e6a000', alignItems: 'center', marginLeft: 2, marginTop: 5, width: 50, height: 20, borderRadius: 2, flexDirection: 'row', }}>
                        <Icon name='md-star' size={13} style={{ marginLeft: 4, color: 'white', marginRight: 5 }} />
                        <Text style={{ color: 'white', fontSize: 11 }}>{item.avg_rating}</Text>

                      </View>
                      <Text style={{ color: 'white', fontSize: 11 }}>Rating({item.total_rating})</Text>

                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                      <Text style={{ fontSize: 15, paddingLeft: 4, paddingRight: 2, marginTop: 3, fontWeight: 'bold' }}>{'\u20B9'}{item.price}</Text>
                      <Text style={{ color: '#988f8f', textDecorationLine: 'line-through', marginTop: 3, paddingLeft: 20, textDecorationStyle: 'solid', fontSize: 15, }}>
                        {'\u20B9'} {item.fees}
                      </Text>
                    </View>
                  </View>
                </TouchableOpacity>
              </View>
            ))
          }
        </ScrollView>
        <View style={styles.category}>
          <View style={{ flexDirection: 'column' }}>
            <Text style={{ fontWeight: 'bold', fontSize: 18, marginLeft: 10 }}>Featured Instructor Profile</Text>
            <Image source={require('../../icon/color-border.png')} style={{ width: '45%', height: 3, marginTop: 5, marginLeft: 5 }} />
          </View>
          <Text style={{ fontWeight: '100', marginTop: 5, marginLeft: 10 }}>See all</Text>
        </View>
        <ScrollView style={{ margin: 2, }} showsHorizontalScrollIndicator={false} horizontal>
          {
            teacherdata.map((item, key) => (
              <View key={key} >
                <Card style={{ width: 200, height: 330, marginTop: 2, marginRight: 2, marginBottom: 2, }} >
                  <View >
                    <View style={{ height: 150, }}>
                      {/* <ImageBackground
                        style={{
                          width: '100%',
                          height: '100%',

                          borderRadius: 10,
                        }}
                        source={
                          require('../../icon/instructor1.png') //Indicator
                        }> */}
                        <Image style={{
                          width: '100%',
                          height: '120%',

                          borderRadius: 10,
                        }} source={{ uri: 'https://appleskool.com/preview/appleskool_code/storage/app/public/profile_picture/' + item.profile_picture }} style={styles.instructorcardImage} />
                      {/* </ImageBackground> */}
                    </View>
                    <View style={{}}>
                      <Text style={{ margin: 4, fontSize: 20, fontWeight: 'bold', marginTop: 6 }}>{item.fname + " " + item.lname}</Text>
                      <View style={{ flexDirection: 'row', width: 200, flexWrap: "wrap", }}>
                        {item.teacher_skill.map((item, key) =>

                        (
                          <View key={key}>
                            <Text style={{ marginLeft: 6 }}>{item.skills.skill_name}</Text>
                          </View>)
                        )
                        }
                      </View>


                      <View style={{ flexDirection: 'row', marginLeft: 5, marginTop: 5, width: 170, height: 20, borderRadius: 2, }}>
                        <Icon name='md-star' size={14} style={{ marginTop: 2, color: '#5e10b1', marginRight: 5 }} />
                        <Text style={{ color: '#5e10b1', marginRight: 5 }}>{item.avg_rating}</Text>
                        <Text style={{}}>Instructor Rating</Text>
                      </View>
                      <Text style={{ marginLeft: 6 }}>123,457 Students</Text>
                      <Text style={{ marginLeft: 6 }}>{item.trainings_count} Training</Text>
                      <View style={{ borderBottomColor: '#e9e9e9', borderBottomWidth: 1, marginTop: 10, width: '100%' }} />
                    </View>
                    <View style={{ flexDirection: 'row', }}>
                      <Text style={{ fontSize: 15, paddingLeft: 4, paddingRight: 2, marginTop: 8, marginLeft: 6, fontWeight: 'bold' }}>{'\u20B9'}{item.member_ship_amount}</Text>
                      <Text style={{ color: '#988f8f', textDecorationLine: 'line-through', marginLeft: 4, marginTop: 8, textDecorationStyle: 'solid', fontSize: 15, }}>
                        {'\u20B9'}{item.member_ship_amount}
                      </Text>
                      <TouchableOpacity

                        onPress={
                          () => navigation.navigate('InstructorProfile', { Data: item.id })

                        } style={{ width: 80, height: 23, backgroundColor: '#d73c5e', marginTop: 8, marginLeft: 8, alignContent: 'center', borderRadius: 5, borderColor: "#d73c5e", borderWidth: 1, paddingLeft: 2 }}

                      >
                        <Text style={{ color: 'white', textAlign: 'center', fontSize: 11, marginTop: 3 }}>Read More</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </Card>
              </View>
            ))
          }
        </ScrollView>


        <View style={{ alignItems: 'center', textAlign: 'center', marginTop: 15 }}>
          <Image source={require('../../icon/post-banner.png')} style={{ width: '100%', height: 140, alignContent: 'center', alignItems: 'center' }} />
          <Text style={{ position: 'absolute', margin: 10, fontSize: 17, fontWeight: 'bold', marginTop: 25, marginRight: 20, color: 'white', }}>Get personal learning recommendations </Text>
          <Text style={{
            position: 'absolute', margin: 15, fontSize: 12, marginTop: 50,
            textAlign: 'center', color: 'white',
          }}>Answer a few questions for your top picks</Text>
          <Text style={{
            position: 'absolute', margin: 10, fontSize: 13, marginTop: 78, borderRadius: 3,
            textAlign: 'center', justifyContent: 'center', color: 'white', backgroundColor: '#82b46e', padding: 7
          }}>Get Started Now</Text>
        </View>
        <View style={{ alignItems: 'center', flexWrap: 'wrap', width: '93%', marginLeft: 10, marginTop: 20, marginBottom: 20, marginRight: 30 }} >

          <View style={{ flexDirection: 'row', width: '100%', borderBottomColor: 'gray', alignItems: 'center', }} >


            <View style={{ marginLeft: 10, flex: 1, marginTop: -30 }}>
              <Text style={{ fontSize: 17, fontWeight: '800', fontFamily: 'roboto-bold', flexWrap: 'wrap', flexDirection: 'row', }}  >Become An Instructor</Text>

              <Text style={{ fontFamily: 'roboto-regular', fontSize: 14, flexWrap: "wrap", width: '100%' }}>master Photoshop CC 2020 without any previous knowledge</Text>
              <Text style={{
                position: 'absolute', fontSize: 15, marginTop: 60, borderRadius: 3,
                textAlign: 'center', justifyContent: 'center', color: 'white', backgroundColor: '#d73c5e', padding: 7
              }}>Get Started Now</Text>

            </View>
            <View style={{ flexDirection: 'column' }}>
              <Image source={require('../../icon/post-banner.png')} style={styles.cardImageALLCourses} />

            </View>
          </View>

        </View>
        {/* <View style={styles.category}>
          <View style={{ flexDirection: 'column' }}>
            <Text style={{ fontWeight: 'bold', fontSize: 18, marginLeft: 8 }}>Students are viewing</Text>
            <Image source={require('../../icon/color-border.png')} style={{ width: '45%', height: 4, marginTop: 5, marginLeft: 5 }} />
          </View>
          <Text style={{ fontWeight: '100', marginTop: 5, }}>See all</Text>
        </View>
        <ScrollView style={{ margin: 2, }} showsHorizontalScrollIndicator={false} horizontal>
          {
            trainingdata.map((item, key) => (
              <View key={key} >
                <TouchableOpacity style={{ width: 230, height: 350, marginTop: 2, marginLeft: 10, marginRight: 17, }} onPress={() => navigation.navigate('TrainingDetailsPostBooking')}>
                  <View >
                    <View style={{ height: 200, }}>
                      <ImageBackground
                        style={{
                          width: '100%',
                          height: '100%',

                          borderRadius: 10,
                        }}
                        source={
                          require('../../icon/featured-training-img3.png') //Indicator
                        }>
                        <Image source={{ uri: 'https://appleskool.com/preview/appleskool_code/storage/app/public/category/' + item.training_image }} style={styles.cardImage2} />
                      </ImageBackground>
                    </View>
                    <Text style={{ margin: 2, fontSize: 15, fontWeight: 'bold' }}>{item.topic}</Text>
                    <Text style={{ marginLeft: 2 }}>{item.category.name}</Text>
                    <View style={{ width: 100, borderRadius: 2, flexDirection: 'row', alignItems: 'center' }}>
                      <View style={{ backgroundColor: '#e6a000', alignItems: 'center', marginLeft: 2, marginTop: 5, width: 50, height: 20, borderRadius: 2, flexDirection: 'row', }}>
                        <Icon name='md-star' size={13} style={{ marginLeft: 4, color: 'white', marginRight: 5 }} />
                        <Text style={{ color: 'white', fontSize: 11 }}>{item.avg_rating}</Text>

                      </View>
                      <Text style={{ color: 'white', fontSize: 11 }}>Rating({item.total_rating})</Text>

                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                      <Text style={{ fontSize: 15, paddingLeft: 4, paddingRight: 2, marginTop: 3, fontWeight: 'bold' }}>{'\u20B9'}{item.price}</Text>
                      <Text style={{ color: '#988f8f', textDecorationLine: 'line-through', marginTop: 3, paddingLeft: 20, textDecorationStyle: 'solid', fontSize: 15, }}>
                        {'\u20B9'} {item.fees}
                      </Text>
                    </View>
                  </View>
                </TouchableOpacity>
              </View>
            ))
          }
        </ScrollView> */}
        <View style={styles.category2}>
          <View style={{ flexDirection: 'column' }}>
            <Text style={{ fontWeight: '700', fontSize: 18, marginLeft: 8 }}>Testimonials</Text>
            <Image source={require('../../icon/color-border.png')} style={{ width: '45%', height: 4, marginTop: 5, marginLeft: 5 }} />
          </View>

        </View>
        <ScrollView style={{}} showsHorizontalScrollIndicator={false} horizontal>
          {
            instructorImage.map((item, key) => (
              <View key={key} style={{ alignItems: 'center', flexWrap: 'wrap', width: 300, marginRight: 50, marginBottom: 20 }} >
                <Card style={{ width: 300, marginTop: 2, marginRight: 12, marginBottom: 12 }} >
                  <View style={{ flexDirection: 'row', width: 300, borderBottomColor: 'gray' }} >
                    <View style={{ flexDirection: 'column' }}>
                      <Image source={item} style={styles.cardImageALLCourses} />

                    </View>

                    <View style={{ marginLeft: 10, flex: 1 }}>
                      <Text style={{ margin: 2, fontSize: 15, fontWeight: 'bold', fontFamily: 'roboto-bold', flexWrap: 'wrap', flexDirection: 'row', }}  >Rabin Manna</Text>
                      <View style={{ width: 100, borderRadius: 2, flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{ alignItems: 'center', marginLeft: 2, marginTop: 5, borderRadius: 2, flexDirection: 'row', }}>

                          <Icon name='md-star' size={13} style={{ marginLeft: 1, color: '#e6a000', marginRight: 1 }} />
                          <Icon name='md-star' size={13} style={{ marginLeft: 1, color: '#e6a000', marginRight: 1 }} />
                          <Icon name='md-star' size={13} style={{ marginLeft: 1, color: '#e6a000', marginRight: 1 }} />
                          <Icon name='md-star' size={13} style={{ marginLeft: 1, color: '#e6a000', marginRight: 1 }} />
                          <Icon name='md-star' size={13} style={{ marginLeft: 1, color: '#e6a000', marginRight: 1 }} />
                        </View>


                      </View>
                      <Text style={{ fontFamily: 'roboto-regular', fontSize: 12, flexWrap: "wrap", width: '100%' }}>master Photoshop CC 2020 without any previous knowledge</Text>
                      <View style={styles.orLine} />




                      <View style={{ flexDirection: 'column', alignItems: 'center', flexWrap: "wrap", }}>

                        <Text style={{ fontSize: 11, paddingLeft: 3, paddingRight: 2, fontWeight: 'bold', fontFamily: 'roboto-bold', marginTop: 2, }}>Posted on :  21,sep 2020</Text>
                        <Text style={{ fontSize: 11, paddingLeft: 3, paddingRight: 2, fontWeight: 'bold', fontFamily: 'roboto-bold', marginTop: 2, }}>Kolkata, India</Text>
                      </View>
                    </View>

                  </View>
                </Card>
              </View>
            ))
          }
        </ScrollView>
      </ScrollView>
      {/* {
          (this.state.isVisible === true) ? Splash_Screen : null
        } */}
    </View >
  );
};
export default HomePage;

const styles = StyleSheet.create({
  orLine: {
    borderBottomColor: '#e9e9e9',
    borderBottomWidth: 1,
    marginTop: 8,
    width: '80%'
  },
  container: {
    flex: 1,
    backgroundColor: "#fff"
    //justifyContent: 'center',
    //alignItems: 'center',
    // width:Math.round(Dimensions.get('window').width),
  },
  cardImage: {
    height: '100%',
    width: '100%',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#d73c5e00',
    marginRight: 15,
  },
  cardImage2: {
    height: '100%',
    width: '99%',
    borderRadius: 15,
    borderWidth: 1,
    borderColor: '#d73c5e00',
    marginRight: 15,
  },
  cardImageALLCourses: {
    height: 115,
    width: 115,
    borderColor: 'transparent',
    borderWidth: 1,
    borderRadius: 10
  },
  banner1stText: {
    position: 'absolute',
    margin: 7,
    fontSize: 16,
    fontWeight: 'bold',
    marginTop: 10,
    textAlign: 'center',
    justifyContent: 'center',
    fontFamily: 'roboto-regular',
    marginLeft: 12
  },
  banner2stText: {
    position: 'absolute',
    margin: 7,
    fontSize: 11,
    marginTop: 36,
    marginRight: 10,
    color: '#6c6763',
    fontFamily: 'roboto-light',
    marginLeft: 12
  },
  banner3stText: {
    // position: 'absolute', 
    // marginTop:80,
    fontSize: 13,
    textAlign: 'center',
    justifyContent: 'center',
    color: 'white',
    backgroundColor: '#d73c5e',
    marginTop: 80,
    width: '50%',
    height: 28,
    marginLeft: 12,
    paddingTop: 5,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#d73c5e',
    fontFamily: 'roboto-bold',
  },
  instructorcardImage: {
    height: 150,
    width: '100%',
    borderRadius: 4,
    marginTop: 5,
    marginBottom: 15
  },
  overlay: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'rgba(255,255,255,.8)',
    height: '70%',
    width: '80%',
    alignContent: 'center',
    alignItems: 'baseline',
    borderBottomEndRadius: 5,
    borderTopEndRadius: 5,
    borderWidth: 1,
    borderColor: '#fff',
    marginTop: 40,
    marginBottom: 10
  },
  category: {
    height: 45,
    width: '95%',
    flexDirection: 'row',
    alignItems: "center",
    justifyContent: 'space-between',
    //  backgroundColor:'red',
    marginTop: 10,
    marginLeft: 5
  },
  category2: {
    height: 45,
    width: '95%',
    flexDirection: 'row',
    alignItems: "center",
    justifyContent: 'space-between',
    //  backgroundColor:'red',
    marginLeft: 5
  },
  buttonContainer: {
    height: 35,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    paddingLeft: 10,
    paddingRight: 10,
    borderRadius: 30,
    marginTop: 10,
    marginLeft: 15,
    borderColor: '#d73c5e',
    borderWidth: 1
  },
  inputIcon: {
    width: 25,
    height: 25,
    marginLeft: 1,
    marginTop: 4,
    marginRight: 2,
    justifyContent: 'center'
  },
  cardshadow: {
    marginRight: 12,
    // borderTopWidth: 0,
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 2,
    //  borderWidth: 1,
    borderRadius: 3,
    marginLeft: 10
  },
  SplashScreen_RootView:
  {
    justifyContent: 'center',
    flex: 1,
    backgroundColor: '#db4f6e',
    position: 'absolute',
    width: '100%',
    height: '100%',
  },
  SplashScreen_ChildView:
  {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
});
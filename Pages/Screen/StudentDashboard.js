import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View, Dimensions,
    TextInput,
    TouchableOpacity,
    TouchableHighlight,
    Image, ScrollView, StatusBar
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { Card } from "react-native-elements";
const screenWidth = Math.round(Dimensions.get('window').width);

const StudentDashboard = ({ navigation }) => {
    const [images, setImages] = React.useState([
        require('../../icon/banner1.png'),
        require('../../icon/banner2.png'),
        require('../../icon/banner3.png'),
    ])
    const [traing, setTraing] = React.useState([
        require('../../icon/featured-training-img.png'),
        require('../../icon/featured-training-img2.png'),
        require('../../icon/featured-training-img3.png'),
    ])
    const [instructorImage, setInstructorImage] = React.useState([
        require('../../icon/instructor1.png'),
        require('../../icon/instructor2.png'),
        require('../../icon/instructor3.png'),
    ])

    // static navigationOptions = ({ navigation, navigationOptions }) => {
    //   return {
    //     title: (<View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: '100%' }}>
    //       <Image style={{ height: 70, width: 100 }} resizeMode="contain" source={require('../../icon/logo.png')} /></View>),
    //     headerMode: 'none',
    //     headerTitleStyle: {
    //       color: '#5e5f61',
    //       fontSize: 16,
    //     },
    //     headerStyle: {
    //       // backgroundColor: '#141f47',//'#04adb3',
    //       height: 50,
    //     },
    //     headerTitleAlign: 'center',

    //   }
    // }
    // constructor(props) {
    //   super(props);
    //   // global.Currentstate = this.props.navigation.state.routeName;
    //   this.state = {
    //     isVisible: true,
    //     email: '',
    //     password: '',
    //     images: [
    //       require('../../icon/banner1.png'),
    //       require('../../icon/banner2.png'),
    //       require('../../icon/banner3.png'),
    //     ],
    //     traing: [
    //       require('../../icon/featured-training-img.png'),
    //       require('../../icon/featured-training-img2.png'),
    //       require('../../icon/featured-training-img3.png'),
    //     ],
    //     instructorImage: [
    //       require('../../icon/instructor1.png'),
    //       require('../../icon/instructor2.png'),
    //       require('../../icon/instructor3.png'),
    //     ],
    //   }
    // }
    // Hide_Splash_Screen = () => {
    //   this.setState({
    //     isVisible: false
    //   });
    // }
    // componentDidMount() {
    //   var that = this;
    //   setTimeout(function () {
    //     that.Hide_Splash_Screen();
    //   }, 3000);
    // }

    // let Splash_Screen = (
    //   <View style={styles.SplashScreen_RootView}>
    //     <Image source={require('../../icon/splash-background.png')}
    //       style={{ width: '100%', height: '100%', resizeMode: 'contain' }} />
    //     <View style={styles.SplashScreen_ChildView}>

    //       <Image source={require('../../icon/logo-white.png')}
    //         style={{ width: '50%', height: '90%', resizeMode: 'contain' }} />
    //       <Image source={require('../../icon/loader.png')}
    //         style={{ width: '15%', height: '10%', resizeMode: 'contain' }} />
    //     </View>
    //   </View>)
    return (
        <View style={styles.drawerContent}>
            <ScrollView>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#cecece' }}>
                    <View>
                        <Text style={{ fontSize: 26, margin: 10, }}>Dashboard</Text>
                    </View>


                </View>
                <View style={{ margin: 10 }}>
                    <Text style={{ fontSize: 19, fontWeight: 'bold' }}>Welcome Abhijit</Text>
                    <View>
                        <Text style={{ marginTop: 10, fontSize: 17, fontWeight: 'bold' }}>Basic Information</Text>
                        <View style={{ flexDirection: 'column', flex: 0.3 }}>
                            <View style={{ margin: 10 }}>
                                <Text style={{ fontSize: 16 }}>Full Name</Text>
                                <Text>Abhijit Saha</Text>
                            </View>
                            <View style={{ margin: 10 }}>
                                <Text style={{ fontSize: 16 }}>Nick Name</Text>
                                <Text>Abhijit Saha</Text>
                            </View>
                            <View style={{ margin: 10 }}>
                                <Text >Email</Text>
                                <Text>rahit@gmail.com</Text>
                            </View>
                        </View>
                    </View>
                    <View >
                        <Text style={{ fontSize: 16, fontWeight: 'bold' }}>Address Information</Text>
                        <View style={{ margin: 10 }}>
                            <Text>Full Address</Text>
                            <Text>Abhijit Saha</Text>
                        </View>
                        <View style={{ flexDirection: 'column' }}>
                            <View style={{ margin: 10 }}>
                                <Text>State</Text>
                                <Text>West Bengal</Text>
                            </View>
                            <View style={{ margin: 10 }}>
                                <Text>City</Text>
                                <Text>Kolkata</Text>
                            </View>
                            <View style={{ margin: 10 }}>
                                <Text>Pincode</Text>
                                <Text>700064</Text>
                            </View>
                            <View style={{ margin: 10 }}>
                                <Text>Country</Text>
                                <Text>India</Text>
                            </View>
                        </View>
                    </View><Text style={{ fontSize: 16, fontWeight: 'bold' }}>Profile Picture</Text>
                    <View style={{ flexDirection: 'column' }}>
                        <Image source={require('../../icon/instructor1.png')} style={styles.cardImageALLCourses} />

                    </View>
                </View>
            </ScrollView>
        </View>
    );
};
export default StudentDashboard;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff"
        //justifyContent: 'center',
        //alignItems: 'center',
        // width:Math.round(Dimensions.get('window').width),
    },
    cardImage: {
        height: '100%',
        width: '100%',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#d73c5e00',
        marginRight: 15,
    },
    cardImageALLCourses: {
        height: 115,
        width: 115,
        borderColor: 'transparent',
        borderWidth: 1,
        borderRadius: 10
    },
    cardImage2: {
        height: '100%',
        width: '99%',
        borderRadius: 15,
        borderWidth: 1,
        borderColor: '#d73c5e00',
        marginRight: 15,
    },
    banner1stText: {
        position: 'absolute',
        margin: 7,
        fontSize: 16,
        fontWeight: 'bold',
        marginTop: 10,
        textAlign: 'center',
        justifyContent: 'center',
        fontFamily: 'roboto-regular',
        marginLeft: 12
    },
    banner2stText: {
        position: 'absolute',
        margin: 7,
        fontSize: 11,
        marginTop: 36,
        marginRight: 10,
        color: '#6c6763',
        fontFamily: 'roboto-light',
        marginLeft: 12
    },
    banner3stText: {
        // position: 'absolute', 
        // marginTop:80,
        fontSize: 13,
        textAlign: 'center',
        justifyContent: 'center',
        color: 'white',
        backgroundColor: '#d73c5e',
        marginTop: 80,
        width: '50%',
        height: 28,
        marginLeft: 12,
        paddingTop: 5,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#d73c5e',
        fontFamily: 'roboto-bold',
    },
    instructorcardImage: {
        height: 150,
        width: '100%',
        borderRadius: 4,
        marginTop: 5,
        marginBottom: 15
    },
    overlay: {
        ...StyleSheet.absoluteFillObject,
        backgroundColor: 'rgba(255,255,255,.8)',
        height: '70%',
        width: '80%',
        alignContent: 'center',
        alignItems: 'baseline',
        borderBottomEndRadius: 5,
        borderTopEndRadius: 5,
        borderWidth: 1,
        borderColor: '#fff',
        marginTop: 40,
        marginBottom: 10
    },
    category: {
        height: 45,
        width: '95%',
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'space-between',
        //  backgroundColor:'red',
        marginTop: 15,
        marginLeft: 5
    },
    category2: {
        height: 45,
        width: '95%',
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'space-between',
        //  backgroundColor:'red',
        marginLeft: 5
    },
    buttonContainer: {
        height: 35,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20,
        paddingLeft: 10,
        paddingRight: 10,
        borderRadius: 30,
        marginTop: 10,
        marginLeft: 15,
        borderColor: '#d73c5e',
        borderWidth: 1
    },
    inputIcon: {
        width: 25,
        height: 25,
        marginLeft: 1,
        marginTop: 4,
        marginRight: 2,
        justifyContent: 'center'
    },
    cardshadow: {
        marginRight: 12,
        // borderTopWidth: 0,
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 2,
        //  borderWidth: 1,
        borderRadius: 3,
        marginLeft: 10
    },
    SplashScreen_RootView:
    {
        justifyContent: 'center',
        flex: 1,
        backgroundColor: '#db4f6e',
        position: 'absolute',
        width: '100%',
        height: '100%',
    },
    SplashScreen_ChildView:
    {
        ...StyleSheet.absoluteFillObject,
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
});
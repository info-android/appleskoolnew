import React, { Component } from 'react';
import { View, Text, Button, StyleSheet, TextInput, TouchableOpacity, ScrollView, StatusBar, AsyncStorage, FlatList } from 'react-native';
import Picker from './Components/Picker'
import { RadioButton } from 'react-native-paper';
import Icon from 'react-native-vector-icons/Ionicons';
import Moment from 'moment';
import IconIcon from 'react-native-vector-icons/FontAwesome';
import IconIconIcon from 'react-native-vector-icons/FontAwesome5';

const StudentBooking = ({ navigation }) => {
    const [checked, setChecked] = React.useState('first');
    const [MyTraining, setMyTraining] =React.useState([
        {
            "id":125,
            "topic":"243",
            "fees":"545.00",
            "date":"NA",
            "training_type":"1",
            "no_of_sessions":0,
            "status":"InComplete"
         },
         {
            "id":124,
            "topic":"522021",
            "fees":"233.00",
            "date":"Not Applicable",
            "training_type":"2",
            "no_of_sessions":"No Applicable",
            "status":"InComplete"
         },
         {
            "id":122,
            "topic":"832",
            "fees":"565.00",
            "date":"NA",
            "training_type":"1",
            "no_of_sessions":0,
            "status":"InComplete"
         },
         {
            "id": 124,
            "topic": "522021",
            "fees": "233.00",
            "date": "Not Applicable",
            "training_type": "2",
            "no_of_sessions": "No Applicable",
            "status": "InComplete"
        },
        {
            "id": 122,
            "topic": "832",
            "fees": "565.00",
            "date": "NA",
            "training_type": "1",
            "no_of_sessions": 0,
            "status": "InComplete"
        },
        {
            "id": 121,
            "topic": "time805asas",
            "fees": "434.00",
            "date": "NA",
            "training_type": "1",
            "no_of_sessions": 0,
            "status": "InComplete"
        },
        {
            "id": 120,
            "topic": "time805",
            "fees": "433.00",
            "date": "NA",
            "training_type": "1",
            "no_of_sessions": 0,
            "status": "InComplete"
        },
        {
            "id": 119,
            "topic": "time805",
            "fees": "433.00",
            "date": "NA",
            "training_type": "1",
            "no_of_sessions": 0,
            "status": "InComplete"
        },
        {
            "id": 118,
            "topic": "time805",
            "fees": "433.00",
            "date": "NA",
            "training_type": "1",
            "no_of_sessions": 0,
            "status": "InComplete"
        },
        {
            "id": 117,
            "topic": "time721",
            "fees": "433.00",
            "date": "NA",
            "training_type": "1",
            "no_of_sessions": 0,
            "status": "InComplete"
        },
        {
            "id": 116,
            "topic": "time721",
            "fees": "433.00",
            "date": "NA",
            "training_type": "1",
            "no_of_sessions": 0,
            "status": "InComplete"
        },
        {
            "id": 115,
            "topic": "we5",
            "fees": "345.00",
            "date": "NA",
            "training_type": "1",
            "no_of_sessions": 0,
            "status": "InComplete"
        },
        {
            "id": 114,
            "topic": "we5",
            "fees": "345.00",
            "date": "NA",
            "training_type": "1",
            "no_of_sessions": 0,
            "status": "InComplete"
        },
        {
            "id": 113,
            "topic": "we5",
            "fees": "345.00",
            "date": "NA",
            "training_type": "1",
            "no_of_sessions": 0,
            "status": "InComplete"
        },
        {
            "id": 112,
            "topic": "we5",
            "fees": "345.00",
            "date": "NA",
            "training_type": "1",
            "no_of_sessions": 0,
            "status": "InComplete"
        },
        {
            "id": 111,
            "topic": "we5",
            "fees": "345.00",
            "date": "NA",
            "training_type": "1",
            "no_of_sessions": 0,
            "status": "InComplete"
        },
        {
            "id": 110,
            "topic": "we5",
            "fees": "345.00",
            "date": "NA",
            "training_type": "1",
            "no_of_sessions": 0,
            "status": "InComplete"
        },
        {
            "id": 109,
            "topic": "we5",
            "fees": "345.00",
            "date": "NA",
            "training_type": "1",
            "no_of_sessions": 0,
            "status": "InComplete"
        },
        {
            "id": 108,
            "topic": "we5",
            "fees": "345.00",
            "date": "NA",
            "training_type": "1",
            "no_of_sessions": 0,
            "status": "InComplete"
        },
    ])
    Moment.locale('en');

    return (
        <View style={styles.container}>
            <StatusBar backgroundColor='#961b37' barStyle="light-content" />

            <ScrollView>
                <View style={{ padding: 15, }}>
                    <Text style={{ fontSize: 22, fontWeight: 'bold' }}>My Booking</Text>
                </View>
                <View>
                    <View style={{ margin: 10 }}>
                        <Text>Start Date</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={styles.action}>
                            <TextInput
                                placeholder="From"
                                style={styles.textInput}
                                autoCapitalize="none"
                            // onChangeText={(val) => textFirstName(val)}
                            />
                        </View>
                        <View style={styles.action}>
                            <TextInput
                                placeholder="To"
                                style={styles.textInput}
                                autoCapitalize="none"
                            // onChangeText={(val) => textFirstName(val)}
                            />
                        </View>
                    </View>
                    <View style={{ margin: 10 }}>
                        <Text>Status</Text>
                    </View>
                    <Picker />
                    {/* <View style={{ flexDirection: 'column' }}>
                        <RadioButton.Group style={{ flexDirection: 'column' }}>
                            <View style={{ flexDirection: 'row', paddingLeft: 50 }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <RadioButton value="Male" />
                                    <Text style={{ paddingTop: 8 }}>Past Training</Text>
                                </View>
                                <View style={{ flexDirection: 'row' }}>
                                    <RadioButton value="Female" />
                                    <Text style={{ paddingTop: 8 }}>Upcoming Training</Text>
                                </View>
                            </View>
                        </RadioButton.Group>
                    </View> */}
                    <View style={{ flexDirection: 'row', paddingLeft: 50 }}>
                        <View style={{ flexDirection: 'row' }}>
                            <RadioButton
                                value="first"
                                status={checked === 'first' ? 'checked' : 'unchecked'}
                                onPress={() => setChecked('first')}
                            />
                            <Text style={{ paddingTop: 8 }}>Past Training</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <RadioButton
                                value="second"
                                status={checked === 'second' ? 'checked' : 'unchecked'}
                                onPress={() => setChecked('second')}
                            />
                            <Text style={{ paddingTop: 8 }}>Upcoming Training</Text>
                        </View>
                    </View>
                    <TouchableOpacity style={{
                        height: 35, justifyContent: 'center', alignItems: 'center', width: '25%',
                        padding: 1,
                        backgroundColor: '#fffff', borderRadius: 5, marginLeft: 10, marginTop: 10, marginBottom: 5, borderRadius: 5, backgroundColor: '#d73c5e'
                    }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={{ fontSize: 13, color: '#ffffff', fontWeight: 'bold' }}>Search</Text>
                        </View>
                    </TouchableOpacity>
                </View>

                {/* <View style={{ flexDirection: 'row' }}>
                    <View style={{ flexDirection: 'row', backgroundColor: '#ffffff', height: 150, width: 250 }}>
                        <View >
                            <Icon name="contact" size={20} color="#9700e3" />
                        </View>
                        <View>
                            <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Training ID:</Text>
                            <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Name:</Text>
                            <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Email:</Text>
                            <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Phone No:</Text>
                            <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Gender:</Text>
                            <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Age:</Text>
                            <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Status:</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', backgroundColor: '#ffffff', height: 150, width: 250 }}>
                        <View >
                            <Icon name="contact" size={20} color="#9700e3" />
                        </View>
                        <View>
                            <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Training ID:</Text>
                            <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Name:</Text>
                            <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Email:</Text>
                            <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Phone No:</Text>
                            <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Gender:</Text>
                            <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Age:</Text>
                            <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Status:</Text>
                        </View>
                    </View>
                </View> */}
                 <View style={{
                    // justifyContent: 'space-around',
                    flex: 1,
                    margin: '1%'
                }}>
                    <FlatList
                        data={MyTraining}
                        renderItem={({ item }) => (
                            <View style={{
                                //backgroundColor: '#ffb4f3',
                                height: 180, width: 180,
                                //justifyContent: 'space-around',
                                flex: .5,
                                margin: 4,
                                backgroundColor: '#ffffff', borderWidth: 1,
                                borderRedius: 2,
                                borderColor: '#ddd',
                                borderBottomWidth: 0,
                                shadowColor: '#ff00b8',
                                shadowOffset: { width: 10, height: 20 },
                                shadowOpacity: 0.4,
                                shadowRadius: 2,
                                elevation: 10,
                            }}>
                                <View style={{ marginTop: '5%' }}>
                                    <Text style={{ fontSize: 13, color: '#000000', fontWeight: 'bold', textAlign: 'center' }}>Id:{item.id}</Text>
                                    <Text style={{ fontSize: 13, color: '#000000', fontWeight: 'bold', textAlign: 'center' }}>Topic: {item.topic}</Text>
                                    <Text style={{ fontSize: 13, color: '#000000', fontWeight: 'bold', textAlign: 'center' }}>Fees: {item.fees}</Text>
                                    <Text style={{ fontSize: 13, color: '#000000', fontWeight: 'bold', textAlign: 'center' }}>Status: {item.status}</Text>
                                    <Text style={{ fontSize: 13, color: '#000000', fontWeight: 'bold', textAlign: 'center' }}>Topic: {item.topic}</Text>
                                    <Text style={{ fontSize: 13, color: '#000000', fontWeight: 'bold', textAlign: 'center' }}>Date: {Moment(item.date).format('DD-MM-YY')}</Text>
                                    <Text style={{ fontSize: 13, color: '#000000', fontWeight: 'bold', textAlign: 'center' }}>No Of Sessions: {item.no_of_sessions}</Text>
                                </View>
                                <View style={{ flexDirection: 'row', marginTop: '6%', justifyContent: 'space-evenly' }}>
                                    <View style={{ marginRight: 7 }}>
                                        <TouchableOpacity>
                                            <IconIcon name="eye" size={20} color="#5d5f5e" />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ marginRight: 7 }}>
                                        <TouchableOpacity>
                                            <IconIcon name="copy" size={20} color="#5d5f5e" />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ marginRight: 7 }}>
                                        <TouchableOpacity>
                                            <IconIcon name="pencil" size={20} color="#5d5f5e" />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ marginRight: 7 }}>
                                        <TouchableOpacity>
                                            <IconIconIcon name="clock" size={20} color="#5d5f5e" />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ marginRight: 7 }}>
                                        <TouchableOpacity>
                                            <IconIcon name="file" size={20} color="#5d5f5e" />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ marginRight: 7 }}>
                                        <TouchableOpacity>
                                            <IconIconIcon name="file-alt" size={20} color="#5d5f5e" />
                                        </TouchableOpacity>
                                    </View>
                                    <View >
                                        <TouchableOpacity>
                                            <IconIcon name="check-circle" size={20} color="#5d5f5e" />
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        )}
                        numColumns={2}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </View>
            </ScrollView>
        </View >
    );
};

export default StudentBooking;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        //     flexDirection: 'row',
        //     alignItems: 'center',
        //     justifyContent: 'center',
        paddingTop: 5,
        paddingBottom: 15,
    },
    action: {
        flexDirection: 'row',
        marginRight: 10,
        marginLeft: 10,
        borderWidth: 0.6,
        borderRadius: 5,
        borderColor: '#505050',
        //padding: 5
        width: 170
    },
    textInput: {
        //flex: 1,
        //marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        color: '#05375a',
        height: 40
    },
});

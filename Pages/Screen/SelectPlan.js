import React, { useState, useEffect } from 'react'
import {
    SafeAreaView, StyleSheet, Text, Image, View, ScrollView, Dimensions, TouchableOpacity, TextInput, StatusBar, Button, ActivityIndicator, FlatList, AsyncStorage,
    Picker, Animated, Platform
} from 'react-native';
import axios from 'axios';
import Spinner from 'react-native-loading-spinner-overlay';

const SelectPlan = ({ navigation,route }) => {
    const [loading, setLoading] = useState(false);
    const [token, setToken] = React.useState('');
    const [SELECTPLAN, setSELECTPLAN] = React.useState('');
    var arrow = "U+0279C";
    const historyDetails = () => {
        //alert(token);
        setLoading(true);
        // setLoding(false)
        // alert("hiiiiiiiii")
        axios.defaults.baseURL = 'https://appleskool.com/preview/appleskool_code/api';
        axios.defaults.headers.post['Content-Type'] =
            'application/json;charset=utf-8';
        axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';
        axios.defaults.headers.post['Access-Control-Allow-Headers'] = '*';
        const bodyParameters = {
            "jsonrpc": "2.0",
            "params": {
                "id": data2,
            }
        };
        axios
            .post(
                '/membership-view-plan',
                bodyParameters,
                {
                    headers: {
                        'Authorization': `Bearer ${token}`,
                    }
                }
            )
            .then((response) => {
                setLoading(false);
                //console.log(JSON.stringify(response));
                setSELECTPLAN(response.data.membership);
                //setMEMBERSHIP(response.data.memberships)
                //  alert(response.data.memberships[0].name)
            }, (error) => {
                setLoading(false);
                // alert('state ERROR', [
                //     { text: 'Okay' }
                // ]);
                console.log(error);
            });
    };

    const { data2 } = route.params
    //alert("data>>>>>>>>" + JSON.stringify(data2))

    useEffect(() => {
        historyDetails()
        AsyncStorage.getItem('UID123', (err, result) => {
            setToken(result);
            // setTimeout(()=>{
            //    
            // }, 3000);
        })
    }, [token])

    return (
        <View>
            <Spinner
                //visibility of Overlay Loading Spinner
                visible={loading}
                color={"black"}
                overlayColor={'rgba(255,255,255, 1)'}
                //Text with the Spinner
                textContent={'Loading...'}
            //Text style of the Spinner Text
            //textStyle={styles.spinnerTextStyle}
            />
            <View style={{ backgroundColor: '#cfcfce', height: 60, }}>
                <View style={{ height: 20, paddingTop: 20 }}>
                    <Text style={{ fontSize: 20, fontWeight: 'bold', textAlign: 'center' }}>Membership : {SELECTPLAN.name}</Text>
                </View>

            </View>
            <View style={{alignItems:'center'}}>
            <Text style={{fontSize:17,fontWeight:'bold',marginTop:15}}>Total Payble Amount :{SELECTPLAN.amount}</Text>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop:10 }}>
                    
                    <View style={{ marginTop: 12, marginRight: '2%' }}>
                        <TouchableOpacity
                            style={{
                                height: 35, width: 80, paddingLeft: 6, paddingTop: 7, backgroundColor: '#d73c5e', borderRadius: 5,
                            }}
                            //onPress={() => navigation.navigate("VideoHelp")}
                        >
                            <View style={{}}>
                                <Text style={{ fontSize: 13, color: 'white', fontWeight: 'bold',marginLeft:'30%' }}>Pay</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity style={{
                            height: 35, justifyContent: 'center', alignItems: 'center', width: '20%',
                            padding: 1,
                            backgroundColor: '#fffff', borderRadius: 5, marginLeft: 30, borderRadius: 5, borderWidth: 1, borderColor: '#8c56c7', marginRight: 15, marginLeft: 30,marginTop: 12, marginRight: '2%'
                        }} onPress={() => navigation.navigate('Membership')}>
                            <Text style={{ fontSize: 13, color: '#8c56c7', fontWeight: 'bold' }}>Cancel</Text>
                        </TouchableOpacity>
                </View>
        </View>
    )
}

export default SelectPlan

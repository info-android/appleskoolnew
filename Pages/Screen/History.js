import React, { useState, useEffect } from 'react'
import {
    SafeAreaView, StyleSheet, Text, Image, View, ScrollView, Dimensions, TouchableOpacity, TextInput, StatusBar, Button, ActivityIndicator, FlatList, AsyncStorage,
    Picker, Animated, Platform
} from 'react-native';
import axios from 'axios';
import Spinner from 'react-native-loading-spinner-overlay';
import Moment from 'moment';
import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';

const History = ({ navigation }) => {
    const [loading, setLoading] = useState(false);
    const [token, setToken] = React.useState('');
    const [HISTORYS, setHISTORYS] = React.useState([]);
    const regex = /(<([^>]+123456789)>)/ig;
    const [coloue, setColour] = React.useState({
        blue: '#ffffff',
    });
    const [tableHead, settableHead] = React.useState(['Head', 'Head2', 'Head3', 'Head4']);
    const historyDetails = () => {
        //alert(token);
        setLoading(true);
        // setLoding(false)
        // alert("hiiiiiiiii")
        axios.defaults.baseURL = 'https://appleskool.com/preview/appleskool_code/api';
        axios.defaults.headers.post['Content-Type'] =
            'application/json;charset=utf-8';
        axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';
        axios.defaults.headers.post['Access-Control-Allow-Headers'] = '*';
        const bodyParameters = {
        };
        axios

            .post(
                '/membership/history',
                bodyParameters,
                {
                    headers: {
                        'Authorization': `Bearer ${token}`,
                    }
                }
            )
            .then((response) => {

                setLoading(false);
                // alert(JSON.stringify(response.data.membership_historys.data[0].id));
                setHISTORYS(response.data.membership_historys.data);
                //setMEMBERSHIP(response.data.memberships)
                //  alert(response.data.memberships[0].name)
            }, (error) => {
                setLoading(false);
                // alert('state ERROR', [
                //     { text: 'Okay' }
                // ]);
                console.log(error);
            });
    };
    Moment.locale('en');
    let DETAILSMAP = HISTORYS.map((myValue, myIndex) => {
        //  console.log('myValue: ' + myValue.city_name)
        return (
            <>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginLeft: 5, marginRight: 5, alignSelf: 'stretch', }}>
                    <View style={{ flex: 1, alignSelf: 'stretch' }}>
                        <Text >{HISTORYS[myIndex].get_membership.name}</Text>
                    </View>
                    <View style={{ flex: 1, alignSelf: 'stretch', marginLeft: '5%' }}>
                        <Text>{HISTORYS[myIndex].get_membership.duration}</Text>
                    </View>
                    <View style={{ flex: 1, alignSelf: 'stretch', marginRight: '-6%' }}>
                        <Text>{HISTORYS[myIndex].get_membership.amount}</Text>
                    </View>
                    <View>
                        <Text>{Moment(myValue.created_at).format('DD-MM-Y')}</Text>
                    </View>
                </View>
                <View style={styles.lineStyle} />
            </>
        )
        // myIndex=myIndex+1

    });

    useEffect(() => {
        historyDetails()
        AsyncStorage.getItem('UID123', (err, result) => {
            setToken(result);
            // setTimeout(()=>{
            //    
            // }, 3000);
        })
    }, [token])
    return (
        <View>
            <Spinner
                //visibility of Overlay Loading Spinner
                visible={loading}
                color={"black"}
                overlayColor={'rgba(255,255,255, 1)'}
                //Text with the Spinner
                textContent={'Loading...'}
            //Text style of the Spinner Text
            //textStyle={styles.spinnerTextStyle}
            />
            <View style={{ backgroundColor: '#cfcfce', height: 60, }}>
                <View style={{ height: 20, paddingTop: 20 }}>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', textAlign: 'center' }}>Membership History</Text>
                </View>

            </View>
            <View style={{ backgroundColor: '#5a287d', height: 40, flexDirection: 'row', justifyContent: 'space-between', textAlign: 'center', paddingLeft: 5, paddingRight: 5 }}>
                <Text style={{ marginTop: '2.3%',color:'#ffffff' }}>name</Text>
                <Text style={{ marginTop: '2.3%',color:'#ffffff' }}>Duration</Text>
                <Text style={{ marginTop: '2.3%',color:'#ffffff' }}>Amount</Text>
                <Text style={{ marginTop: '2.3%',color:'#ffffff' }}>Purchase Date</Text>

            </View>
            {DETAILSMAP}
            {/* <View style={{ flexDirection: 'row', backgroundColor: '#ffffff', height: 150, width: 250 }}>
                      
                        <View>
                            <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Training ID:</Text>
                            <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Name:</Text>
                            <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Email:</Text>
                            <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Phone No:</Text>
                            <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Gender:</Text>
                            <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Age:</Text>
                            <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Status:</Text>
                        </View>
                    </View> */}
            {/* <View style={{ flex: 1, margin:9, paddingTop: 30, }}>
                <Table borderStyle={{ borderWidth: 2, borderColor: '#900' }}>
                    <Row data={tableHead} style={{ height: 40, backgroundColor: '#f1f8ff', textAlign: 'center' }} textStyle={{textAlign: 'center'}} />
                    <Rows  data={HISTORYS[myIndex].get_membership.name} textStyle={{textAlign: 'center'}}/>
                </Table>
            </View> */}
        </View>
    )
}

export default History

const styles = StyleSheet.create({
    outerView: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        alignContent: 'center',
        //     flexDirection: 'row',
        //     alignItems: 'center',
        //     justifyContent: 'center',
        paddingTop: 10,
        paddingBottom: 20,
    },
    container: {
        flex: 1,
        backgroundColor: 'white',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 5,
        //paddingBottom: 10,
        // },
    },
    lineStyle: {
        borderWidth: .3,
        borderColor: 'black',
        marginTop: 3,
        //marginTop: 25
    },
    action: {
        //flexDirection: 'row',
        marginRight: 10,
        marginLeft: 10,
        borderWidth: 0.6,
        borderRadius: 5,
        borderColor: '#505050',
        //padding: 5
    },
    textInput: {
        //flex: 1,
        //marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        color: '#05375a',
        height: 40
    },
    phonestyle: {
        height: 20,
        width: 100
    },
    // textContainerStyle{
    //     height:20
    // }
    circle: {
        height: 20,
        width: 20,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#ACACAC',
        alignItems: 'center', // To center the checked circle…
        justifyContent: 'center',
        marginHorizontal: 10
    },
    checkedCircle: {
        width: 14,
        height: 14,
        borderRadius: 7,
        backgroundColor: '#fa0562' // You can set it default or with yours one…
    },
    action2: {
        //flexDirection: 'row',
        marginRight: 10,
        marginLeft: 10,
        borderWidth: 0.6,
        borderRadius: 5,
        borderColor: '#505050',
        marginBottom: 5
        //padding: 5
    },
});
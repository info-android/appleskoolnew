import React, { useState, useEffect } from 'react'
import {
    SafeAreaView, StyleSheet, Text, Image, View, ScrollView, Dimensions, TouchableOpacity, TextInput, StatusBar, Button, ActivityIndicator, FlatList, AsyncStorage,
    Picker, Animated, Platform
} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';

import axios from 'axios';
import IconIcon from 'react-native-vector-icons/FontAwesome';
import IconIconIcon from 'react-native-vector-icons/FontAwesome5';
import Icon from 'react-native-vector-icons/Ionicons';
import Card from './Components/Card'
import CardSection from './Components/CardSection'
const Membership = ({ navigation }) => {
    const [loading, setLoading] = useState(false);
    const [token, setToken] = React.useState('');
    const [USER, setUSER] = useState('');
    const [color,setcolor ] = useState(['#5e10b1','#82b46d','#5e10b1','#e6a000','#82b46d','#5e10b1',])
    const [MEMBERSHIP, setMEMBERSHIP] = useState([]);
    const regex = /(<([^>]+)>)/ig;

 
var ColorCode = 'rgb(' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ')';
 

   
    const membershipDetails = () => {
        //alert(token);
        setLoading(true);
        // setLoding(false)
        // alert("hiiiiiiiii")
        axios.defaults.baseURL = 'https://appleskool.com/preview/appleskool_code/api';
        axios.defaults.headers.post['Content-Type'] =
            'application/json;charset=utf-8';
        axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';
        axios.defaults.headers.post['Access-Control-Allow-Headers'] = '*';
        const bodyParameters = {
        };
        axios

            .post(
                '/membership',
                bodyParameters,
                {
                    headers: {
                        'Authorization': `Bearer ${token}`,
                    }
                }
            )
            .then((response) => {

                setLoading(false);
                // alert(JSON.stringify(response.data.user));
                setUSER(response.data.user);
                setMEMBERSHIP(response.data.memberships)
                //  alert(response.data.memberships[0].name)
            }, (error) => {
                setLoading(false);
                // alert('state ERROR', [
                //     { text: 'Okay' }
                // ]);
                console.log(error);
            });
    };
    let Qual = MEMBERSHIP.map((myValue, myIndex) => {
        //  console.log('myValue: ' + myValue.city_name)
        
        return (
            <>

                <View>
                    <View style={{ height: 80, backgroundColor: '#'+Math.floor(Math.random()*16777215).toString(16), paddingTop: '10%' }}>
                        <Text style={{ textAlign: 'center' }} >₹{myValue.amount}</Text>
                        <Text style={{ textAlign: 'center' }} >{myValue.name}</Text>
                    </View>
                    <View style={{ height: 80, backgroundColor: '#ffffff', paddingTop: '7%' }}>
                        <Text style={{ paddingTop: 20, backgroundColor: '#ffffff', textAlign: 'center' }}>{myValue.description.replace(regex, '')}</Text>
                    </View>
                    <View style={{ height: 80, backgroundColor: '#ffffff', paddingTop: '6%' }}>
                        <View style={{ marginTop: '13%', marginLeft: '17%' }}>
                            <TouchableOpacity
                                style={{
                                    height: 35, width: 80, paddingLeft: 6, paddingTop: 7, backgroundColor: '#d73c5e', borderRadius: 5, "&:hover": {
                                        backgroundColor: "#900"
                                    },
                                }}

                                onPress={() => navigation.navigate('SelectPlan', { data2: myValue.id })}
                            >
                                <View style={{}}>
                                    <Text style={{ fontSize: 13, color: 'white', fontWeight: 'bold' }}>Select Plan</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <View style={{ margin: 10 }}>
                </View >
            </>
        )
    });
    useEffect(() => {
        membershipDetails()
        AsyncStorage.getItem('UID123', (err, result) => {
            setToken(result);
            // setTimeout(()=>{
            //    
            // }, 3000);
        })
    }, [token])
    return (
        <>
            <View style={styles.drawerContent}>
                <Spinner
                    //visibility of Overlay Loading Spinner
                    visible={loading}
                    color={"black"}
                    overlayColor={'rgba(255,255,255, 1)'}
                    //Text with the Spinner
                    textContent={'Loading...'}
                //Text style of the Spinner Text
                //textStyle={styles.spinnerTextStyle}
                />

                <View style={{ flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#cfcfce', height: 60, }}>
                    <View style={{ height: 20, paddingTop: 20 }}>
                        <Text style={{ fontSize: 18, fontWeight: 'bold', marginLeft: '2.5%' }}>Membership</Text>
                    </View>
                    <View style={{ marginTop: 12, marginLeft: '20%' }}>
                        <TouchableOpacity
                            style={{
                                height: 35, width: 80, paddingLeft: 6, paddingTop: 7, backgroundColor: '#d73c5e', borderRadius: 5,
                            }}
                            onPress={() => navigation.navigate("VideoHelp")}
                        >
                            <View style={{}}>
                                <Text style={{ fontSize: 13, color: 'white', fontWeight: 'bold' }}>Video Help</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={{ marginTop: 12, paddingRight: '2%' }}>
                        <TouchableOpacity
                            style={{
                                height: 35, width: 80, paddingLeft: 6, paddingTop: 7, backgroundColor: '#d73c5e', borderRadius: 5, marginRight: '0.8%'
                            }}
                            onPress={() => navigation.navigate("History")}
                        >
                            <View style={{}}
                            >
                                <Text style={{ fontSize: 13, color: 'white', fontWeight: 'bold', paddingLeft: '20%' }}>History</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
                <ScrollView>
                    <View style={{ paddingBottom: 8, marginRight: 5, marginLeft: 5, marginTop: 10 }}>
                        <TouchableOpacity
                            style={{
                                height: 35, justifyContent: 'center', alignItems: 'center', width: '100%', padding: 1, backgroundColor: '#f9f9f9', borderRadius: 2, borderColor: '#d7663f', borderWidth: 1,
                                borderRedius: 2,
                                borderColor: '#ddd',
                                borderBottomWidth: 0,
                                shadowColor: '#ff00b8',
                                shadowOffset: { width: 10, height: 20 },
                                shadowOpacity: 0.4,
                                shadowRadius: 2,
                                elevation: 10,
                            }}
                        >
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ paddingRight: 5, paddingLeft: 5 }}>
                                    <IconIconIcon name="wallet" size={20} color='#d7663f' />
                                </View>
                                <Text style={{ fontSize: 13, color: '#d7663f', fontWeight: 'bold', paddingLeft: 5, }}>Current Membership :</Text>
                                <Text style={{ fontSize: 13, color: '#5e10b1', fontWeight: 'bold', paddingLeft: 5, }}>Paid.</Text>
                                <Text style={{ fontSize: 13, color: '#d7663f', fontWeight: 'bold', paddingLeft: 5 }}>Expire on :</Text>
                                <Text style={{ fontSize: 13, color: '#5e10b1', fontWeight: 'bold', paddingLeft: 5, paddingRight: 5 }}>24 Jun, 2021</Text>
                            </View>
                        </TouchableOpacity>
                    </View>

                    {/* <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginBottom: 8, marginTop: 10 }}>
                        <View style={{
                            flex: 0.3, backgroundColor: '#ffffff', borderWidth: 1,
                            borderRedius: 2,
                            borderColor: '#ddd',
                            borderBottomWidth: 0,
                            shadowColor: '#ff00b8',
                            shadowOffset: { width: 10, height: 20 },
                            shadowOpacity: 0.4,
                            shadowRadius: 2,
                            elevation: 10,
                        }}>
                            {Qual}
                        </View>
                    </View> */}

                    {/* <FlatList
                        data={MEMBERSHIP}
                        renderItem={({ item }) => (
                            <View tyle={{ flex: 0.3, justifyContent: 'space-between', }} >
                                <View style={{ height: 80, backgroundColor: '#5e10b1', }}>
                                    <Text style={{ textAlign: 'center' }} >₹{item.amount}</Text>
                                    <Text style={{ textAlign: 'center' }} >{item.name}</Text>
                                </View>

                                <View style={{ height: 80, backgroundColor: '#ffffff', }}>
                                    <View style={{}}>
                                        <TouchableOpacity
                                            style={{
                                                height: 35, width: 80, backgroundColor: '#d73c5e', borderRadius: 5,
                                            }}
                                            onPress={() => navigation.navigate('SelectPlan', { data2: item.id })}
                                        >
                                            <View style={{}}>
                                                <Text style={{ fontSize: 13, color: 'white', fontWeight: 'bold' }}>Select Plan</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        )}
                        numColumns={3}
                        keyExtractor={(item, index) => index.toString()}
                    /> */}

                    <View style={{
                       justifyContent: 'space-around',
                       flex:1,
                       marginLeft:'8%'
                    }}>
                        <FlatList
                            data={MEMBERSHIP}
                            
                            renderItem={({ item}) => (
                                <View style={{
                                    justifyContent: 'space-around',
                                    flex:.3,
                                    margin:5,
                                    backgroundColor: '#ffffff', borderWidth: 1,
                            borderRedius: 2,
                            borderColor: '#ddd',
                            borderBottomWidth: 0,
                            shadowColor: '#ff00b8',
                            shadowOffset: { width: 10, height: 20 },
                            shadowOpacity: 0.4,
                            shadowRadius: 2,
                            elevation: 10,
                                    //paddingLeft:'5%'
                                    //alignItems: 'center',
                                }} >
                                    <View style={{ backgroundColor: color[item.id],height:80,paddingTop:'18%' }}>
                                        <Text style={{ textAlign: 'center' }} >₹{item.amount}</Text>
                                        <Text style={{ textAlign: 'center' }} >{item.name}</Text>
                                    </View>
                                    <View style={{ height: 80, backgroundColor: '#ffffff', paddingTop: '7%' }}>
                                        <Text style={{ paddingTop: 20, backgroundColor: '#ffffff', textAlign: 'center' }}>{item.description.replace(regex, '')}</Text>
                                    </View>
                                    <View style={{ marginTop: 12,alignSelf:'center',marginBottom:'15%' }}>
                        <TouchableOpacity
                            style={{
                                height: 35, width: 80, paddingLeft: 6, paddingTop: 7, backgroundColor: '#d73c5e', borderRadius: 5, marginRight: '0.8%'
                            }}
                            onPress={() => navigation.navigate('SelectPlan', { data2: item.id })}
                        >
                            <View style={{}}
                            >
                                <Text style={{ fontSize: 13, color: 'white', fontWeight: 'bold' }}>Select Plan</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                                </View>
                            )}
                            numColumns={3}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </View>
                    {/* <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginBottom: 8, marginTop: 10 }}>
                        <View style={{
                            flex: 0.3, backgroundColor: '#ffffff', borderWidth: 1,
                            borderRedius: 2,
                            borderColor: '#ddd',
                            borderBottomWidth: 0,
                            shadowColor: '#ff00b8',
                            shadowOffset: { width: 10, height: 20 },
                            shadowOpacity: 0.4,
                            shadowRadius: 2,
                            elevation: 10,
                        }}                        >
                            <View style={{ height: 80, backgroundColor: '#5e10b1', paddingTop: '10%' }}>
                                <Text style={{ textAlign: 'center' }} >₹ 0</Text>
                                <Text style={{ textAlign: 'center' }} >FREE</Text>
                            </View>
                            <View style={{ height: 80, backgroundColor: '#ffffff', paddingTop: '7%' }}>
                                <Text style={{ paddingTop: 20, backgroundColor: '#ffffff', textAlign: 'center' }}>This Plan for Free Members</Text>
                            </View>
                            <View style={{ height: 80, backgroundColor: '#ffffff', paddingTop: '6%' }}>
                                <View style={{ marginTop: '13%', marginLeft: '17%' }}>
                                    <TouchableOpacity
                                        style={{
                                            height: 35, width: 80, paddingLeft: 6, paddingTop: 7, backgroundColor: '#d73c5e', borderRadius: 5, "&:hover": {
                                                backgroundColor: "#900"
                                            },
                                        }}
                                        onPress={() => navigation.navigate("SelectPlan")}
                                    >
                                        <View style={{}}>
                                            <Text style={{ fontSize: 13, color: 'white', fontWeight: 'bold' }}>Select Plan</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                        <View style={{
                            flex: 0.3, backgroundColor: '#ffffff', borderWidth: 1,
                            borderRedius: 2,
                            borderColor: '#ddd',
                            borderBottomWidth: 0,
                            shadowColor: '#ff00b8',
                            shadowOffset: { width: 10, height: 20 },
                            shadowOpacity: 0.4,
                            shadowRadius: 2,
                            elevation: 10,
                        }}>
                            <View style={{ height: 80, backgroundColor: '#82b46d', paddingTop: '10%' }}>
                                <Text style={{ textAlign: 'center' }} >₹ 299</Text>
                                <Text style={{ textAlign: 'center' }} >THREE MONTHS</Text>
                            </View>
                            <View style={{ height: 80, backgroundColor: '#ffffff', paddingTop: '7%' }}>
                                <Text style={{ paddingTop: 20, backgroundColor: '#ffffff', textAlign: 'center' }}>User can use maximum 90 days</Text>
                            </View>
                            <View style={{ height: 80, backgroundColor: '#ffffff', paddingTop: '6%' }}>
                                <View style={{ marginTop: '13%', marginLeft: '17%' }}>
                                    <TouchableOpacity
                                        style={{
                                            height: 35, width: 80, paddingLeft: 6, paddingTop: 7, backgroundColor: '#d73c5e', borderRadius: 5, hover: '#900'
                                        }}
                                    >
                                        <View style={{}}>
                                            <Text style={{ fontSize: 13, color: 'white', fontWeight: 'bold' }}>Select Plan</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                        <View style={{
                            flex: 0.3, backgroundColor: '#ffffff', borderWidth: 1,
                            borderRedius: 2,
                            borderColor: '#ddd',
                            borderBottomWidth: 0,
                            shadowColor: '#ff00b8',
                            shadowOffset: { width: 10, height: 20 },
                            shadowOpacity: 0.4,
                            shadowRadius: 2,
                            elevation: 10,
                        }}>
                            <View style={{ height: 80, backgroundColor: '#e6a000', paddingTop: '10%' }}>
                                <Text style={{ textAlign: 'center' }} >₹ 399</Text>
                                <Text style={{ textAlign: 'center' }} >THREE MONTHS</Text>
                            </View>
                            <View style={{ height: 80, backgroundColor: '#ffffff', paddingTop: '7%' }}>
                                <Text style={{ paddingTop: 20, backgroundColor: '#ffffff', textAlign: 'center' }}>User can use maximum 30 daysSelect Plan</Text>
                            </View>
                            <View style={{ height: 80, backgroundColor: '#ffffff', paddingTop: '6%' }}>
                                <View style={{ marginTop: '13%', marginLeft: '17%' }}>
                                    <TouchableOpacity
                                        style={{
                                            height: 35, width: 80, paddingLeft: 6, paddingTop: 7, backgroundColor: '#d73c5e', borderRadius: 5,
                                        }}
                                    >
                                        <View style={{}}>
                                            <Text style={{ fontSize: 13, color: 'white', fontWeight: 'bold' }}>Select Plan</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </View> */}
                    {/* <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginBottom: 8, marginTop: 10 }}>
                        <View style={{
                            flex: 0.3, backgroundColor: '#ffffff', borderWidth: 1,
                            borderRedius: 2,
                            borderColor: '#ddd',
                            borderBottomWidth: 0,
                            shadowColor: '#ff00b8',
                            shadowOffset: { width: 10, height: 20 },
                            shadowOpacity: 0.4,
                            shadowRadius: 2,
                            elevation: 10,
                        }}>
                            <View style={{ height: 80, backgroundColor: '#5e10b1', paddingTop: '10%' }}>
                                <Text style={{ textAlign: 'center' }} >₹ 599</Text>
                                <Text style={{ textAlign: 'center' }} >HALF YEARLY</Text>
                            </View>
                            <View style={{ height: 80, backgroundColor: '#ffffff', paddingTop: '7%' }}>
                                <Text style={{ paddingTop: 20, backgroundColor: '#ffffff', textAlign: 'center' }}>User can use maximum 180 days
                            </Text>
                            </View>
                            <View style={{ height: 80, backgroundColor: '#ffffff', paddingTop: '6%' }}>
                                <View style={{ marginTop: '13%', marginLeft: '17%' }}>
                                    <TouchableOpacity
                                        style={{
                                            height: 35, width: 80, paddingLeft: 6, paddingTop: 7, backgroundColor: '#d73c5e', borderRadius: 5,
                                        }}
                                    >
                                        <View style={{}}>
                                            <Text style={{ fontSize: 13, color: 'white', fontWeight: 'bold' }}>Select Plan</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                        <View style={{
                            flex: 0.3, backgroundColor: '#ffffff', borderWidth: 1,
                            borderRedius: 2,
                            borderColor: '#ddd',
                            borderBottomWidth: 0,
                            shadowColor: '#ff00b8',
                            shadowOffset: { width: 10, height: 20 },
                            shadowOpacity: 0.4,
                            shadowRadius: 2,
                            elevation: 10,
                        }}>
                            <View style={{ height: 80, backgroundColor: '#5e10b1', paddingTop: '10%' }}>
                                <Text style={{ textAlign: 'center' }} >₹ 999</Text>
                                <Text style={{ textAlign: 'center' }} >YEARLY</Text>
                            </View>
                            <View style={{ height: 80, backgroundColor: '#ffffff', paddingTop: '7%' }}>
                                <Text style={{ paddingTop: 20, backgroundColor: '#ffffff', textAlign: 'center' }}>User can use maximum 1 year</Text>
                            </View>
                            <View style={{ height: 80, backgroundColor: '#ffffff', paddingTop: '6%' }}>
                                <View style={{ marginTop: '13%', marginLeft: '17%' }}>
                                    <TouchableOpacity
                                        style={{
                                            height: 35, width: 80, paddingLeft: 6, paddingTop: 7, backgroundColor: '#d73c5e', borderRadius: 5,
                                        }}
                                    >
                                        <View style={{}}>
                                            <Text style={{ fontSize: 13, color: 'white', fontWeight: 'bold' }}>Select Plan</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </View> */}
                </ScrollView>
            </View>
        </>
    )
}
export default Membership;

const styles = StyleSheet.create({
    drawerContent: {
        flex: 1,
        backgroundColor: '#ffffff'
    },


})
import React, { Component, useState, useEffect } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    Text,
    Image,
    View,
    ScrollView,
    Dimensions,
    TouchableOpacity,
    AsyncStorage,
    TextInput,
    StatusBar
} from 'react-native';

import axios from 'axios';
import Snackbar from 'react-native-snackbar';
//import PhoneInput from 'react-native-phone-input'
//import 'react-phone-number-input/style.css'
//import PhoneInput from 'react-phone-number-input'
import { RadioButton } from 'react-native-paper';
import IconIcon from 'react-native-vector-icons/FontAwesome';
import IconIconIcon from 'react-native-vector-icons/FontAwesome5';
import Icon from 'react-native-vector-icons/Ionicons';
import Spinner from 'react-native-loading-spinner-overlay';
//import Picker from '../Components/Picker'
import { Dropdown } from 'react-native-material-dropdown';
import MultiSelect from 'react-native-multiple-select';
import PhoneInput from "react-native-phone-number-input";
import DateTimePickerModal from "react-native-datepicker";
import ImagePicker from 'react-native-image-crop-picker';



const icon1 = '<IconIcon name="check" size={20} color="#ffffff" />'


const StudentEditProfile = ({ navigation }) => {

    const [checked, setChecked] = React.useState('first');
    const [imagepath, setImagepath] = useState('')
    const [videopath, setVideopath] = useState('');
    const [Date, setDate] = useState('');
    const [loading, setLoading] = useState(false);
    const [loading4, setLoading4] = useState(true)
    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
    const [token, setToken] = React.useState('');
    const [key, setKey] = useState('male'); // const [gender, setGender] = useState(‘male’);
    const [option1, setOption1] = useState(true) // const [maleCheck, setMaleCheck) = useState(true);
    const [femaleCheck, setFemaleCheck] = useState(false);
    const [LanguageSpoken2, setLanguageSpoken2] = useState([]);

    const showDatePicker = () => {
        setDatePickerVisibility(true);
    };

    const hideDatePicker = () => {
        setDatePickerVisibility(false);
    };

    const handleConfirm = (date) => {
        console.warn("A date has been picked: ", date);
        setDate(date)
        hideDatePicker();

    };

    const takePhotoFromGallery = () => {
        // alert('take from gallery')
        ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true,


        }).then(image => {
            setLoading4(false)
            setImagepath(image.path)
            console.log(imagepath);
        });
    }
    const [data2, setSelectLabel] = useState({
        selectLabel: '',
    })
    const [data3, setSelectLabel3] = useState({
        selectLabel3: '',
    })
    const [data4, setSelectLabel4] = useState({
        selectLabel4: '',
    })
    const [data5, setSelectLabel5] = useState({
        selectLabel5: '',
    })
    const [data6, setSelectLabel6] = useState({
        selectLabel6: '',
    })
    const [data7, setSelectLabel7] = useState({
        selectLabel7: '',
    })

    const [ShowLanguageSpoken, setShowLanguageSpoken] = useState([]);
    const [Fname, setFname] = useState('');
    const [Lname, setLname] = useState('');
    const [Nickname, setNickname] = useState('');
    const [PhoneNo, setPhoneNo] = useState('');
    const [Street, setStreet] = useState('');
    const [Pin, setPin] = useState('');
    const [Country, setCountry] = useState('');
    const [phoneCountryCode, setPhoneCountryCode] = useState('');
    const [resdata, setData2] = useState('');
    const [resdata3, setData3] = useState([]);
    const [resdata4, setData4] = useState([]);
    const [resdata5, setData5] = useState([]);
    const [Gender, setGender] = useState([]);
    const [Occupation, setOccu] = useState([]);
    const [Educat, setEdu] = useState([]);
    const [Industr, setIndus] = useState([]);

    const [LanguageSpoken, setLanguageSpoken] = useState('');
    var langnew = [];


    const creatprofilebasicinfo = () => {
        //alert(token);
        setLoading(true);
        // setLoding(false)
        // alert(data.email+data.password)
        axios.defaults.baseURL = 'https://appleskool.com/preview/appleskool_code/api';
        axios.defaults.headers.post['Content-Type'] =
            'application/json;charset=utf-8';
        axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';
        axios.defaults.headers.post['Access-Control-Allow-Headers'] = '*';
        //alert(global.userToken)
        const bodyParameters = {
        };
        axios
            .post('https://appleskool.com/preview/appleskool_code/api/student/edit-profile',
                bodyParameters,
                {
                    headers: {
                        Authorization: `Bearer ${token}`,
                        // Authorization: `Bearer  ${userToken}`,global.userToken
                    },
                },
            )
            .then(


                res => {
                    setLoading(false);
                    //alert(JSON.stringify(res.data.result.user.fname));
                    //global.Me = me.data.firstname + me.data.lastname;


                    setData2(res.data.result.user);
                    setFname(res.data.result.user.fname)
                    //alert(JSON.stringify(Fname));
                    setLname(res.data.result.user.lname)
                    setNickname(res.data.result.user.nick_name)
                    setPhoneNo(res.data.result.user.phone_no)
                    setPhoneCountryCode(res.data.result.user.phone_country_code)

                    setStreet(res.data.result.user.street)
                    setCountry(res.data.result.user.country_id)
                    setPin(res.data.result.user.pin)

                    setGender(res.data.result.user.gender)



                    setData3(res.data.result.state);
                    setLanguageSpoken(res.data.result.interest_skills)
                    setOccu(res.data.result.occupations)
                    setEdu(res.data.result.educations)
                    setIndus(res.data.result.industries)

                    setShowLanguageSpoken(res.data.result.user.student_skills)


                    setData5(res.data.result.interest_skills)
                    //setIMage(res.data.result.user.profile_picture)

                    setDate(res.data.result.user.dob)
                    //alert(resdata.dob)

                    setSelectLabel({ selectLabel: res.data.result.user.state })
                    setSelectLabel3({ selectLabel3: res.data.result.user.city })
                    if (res.data.result.user.occupation_id === null) {
                        setSelectLabel4({ selectLabel4: res.data.result.occupations[0].id })
                    } else {
                        setSelectLabel4({ selectLabel4: res.data.result.user.occupation_id })
                    }

                    if (res.data.result.user.education_id === null) {
                        setSelectLabel5({ selectLabel5: res.data.result.educations[0].id })
                    } else {
                        setSelectLabel5({ selectLabel5: res.data.result.user.education_id })
                    }
                    if (res.data.result.user.industry_id === null) {
                        setSelectLabel6({ selectLabel6: res.data.result.industries[0].id })
                    } else {
                        setSelectLabel6({ selectLabel6: res.data.result.user.industry_id })
                    }


                    setSelectLabel7({ selectLabel7: res.data.result.user.experience })
                    if (res.data.result.user.gender == "male") {

                        setFemaleCheck(false); //for avoiding multiple checks at a time;
                        setKey('male');
                        setOption1(true);

                    } else {

                        setFemaleCheck(true); //for avoiding multiple checks at a time;
                        setKey('female');
                        setOption1(false);

                    }

                    selectState(res.data.result.user.state)

                    selectSkiil(res.data.result.user.student_skills)

                },
                error => {
                    setLoading(false);
                    // alert("Create profile info api error")
                },
            );
    };
    // if (data.isLoading) {
    //     return (
    //         <View style={{ alignItems: 'center', justifyContent: 'center' }}>
    //             <ActivityIndicator size="large" color="#900" />
    //         </View>
    //     )
    // }
    const selectState = (id) => {
        //alert(id);
        setSelectLabel({ selectLabel: id })
        axios.defaults.baseURL = 'https://appleskool.com/preview/appleskool_code/api';
        axios.defaults.headers.post['Content-Type'] =
            'application/json;charset=utf-8';
        axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';
        axios.defaults.headers.post['Access-Control-Allow-Headers'] = '*';
        axios
            .post(
                'https://appleskool.com/preview/appleskool_code/api/get-city', {
                "jsonrpc": "2.0",
                "params": {
                    "state_id": id,
                }
            }
            )
            .then((response) => {

                //  alert(JSON.stringify(response.data.error));
                if (response.data.error) {
                    alert(JSON.stringify(response.data))
                }
                else if (response.data.result) {
                    setData4(response.data.result.city_list);
                    //alert(JSON.stringify(resdata4))
                    //navigation.navigate('ScreenCreatProfileSubjectSkill')
                }
            }, (error) => {
                // alert('state ERROR', [
                //     { text: 'Okay' }
                // ]);
                // console.log(error);
            });
        //alert("State ERROR")
    }

    const selectState2 = (id) => {
        //alert(id);
        setSelectLabel({ selectLabel: id })
        axios.defaults.baseURL = 'https://appleskool.com/preview/appleskool_code/api';
        axios.defaults.headers.post['Content-Type'] =
            'application/json;charset=utf-8';
        axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';
        axios.defaults.headers.post['Access-Control-Allow-Headers'] = '*';
        axios
            .post(
                'https://appleskool.com/preview/appleskool_code/api/get-city', {
                "jsonrpc": "2.0",
                "params": {
                    "state_id": id,
                }
            }
            )
            .then((response) => {
                setData({
                    ...data,
                    isLoading: false,
                });
                //  alert(JSON.stringify(response.data.error));
                if (response.data.error) {
                    // alert(JSON.stringify(response.data))
                }
                else if (response.data.result) {
                    setData4(response.data.result.city_list);
                    setSelectLabel3({ selectLabel3: response.data.result.city_list[0].id })
                    //alert(JSON.stringify(resdata4))
                    //navigation.navigate('ScreenCreatProfileSubjectSkill')
                }
            }, (error) => {
                // alert('state ERROR', [
                //     { text: 'Okay' }
                // ]);
                // console.log(error);
            });
        //alert("State ERROR")
    }
    const [selectedItems, setSelectedItems] = useState([]);
    const onSelectedItemsChange = (selectedItems) => {
        // Set Selected Items
        setSelectedItems(selectedItems);

        alert(JSON.stringify(selectedItems))
    };

    var bodyFormData = new FormData();

    //bodyFormData.append('fname', 'sorry')
    bodyFormData.append('fname', Fname)

    //bodyFormData.append('lname', 'sorry')
    bodyFormData.append('lname', Lname)

    //bodyFormData.append('nick_name', 'Nickname')
    bodyFormData.append('nick_name', Nickname)

    //bodyFormData.append('phone_no', '3216549871')
    bodyFormData.append('phone_no', PhoneNo)

    //bodyFormData.append('phone_country_code', '+91')
    bodyFormData.append('phone_country_code', phoneCountryCode)

    //bodyFormData.append('street', 'sasasass')
    bodyFormData.append('street', Street)

    //bodyFormData.append('state', '1')
    bodyFormData.append('state', data2.selectLabel)

    //bodyFormData.append('city', '1394')
    bodyFormData.append('city', data3.selectLabel3)

    //bodyFormData.append('pin', '123456')
    bodyFormData.append('pin', Pin)

    //bodyFormData.append('country_id', '4')
    bodyFormData.append('country_id', Country)

    //bodyFormData.append('gender', 'male')
    bodyFormData.append('gender', key)

    //bodyFormData.append('experience', '2')
    bodyFormData.append('experience', data7.selectLabel7)

    //bodyFormData.append('language', '[2, 3, 4, 5, 6]')
    bodyFormData.append('education', data5.selectLabel5)

    //bodyFormData.append('about_me', 'sasasasa')
    bodyFormData.append('industry', data6.selectLabel6)



    //bodyFormData.append('dob','')
    bodyFormData.append('dob', Date)

    //bodyFormData.append('youtube_link', 'Youtube')
    bodyFormData.append('occupation', data4.selectLabel4)

    //bodyFormData.append('twitter_link', 'Twiter')

    bodyFormData.append('interest_skill', JSON.stringify(selectedItems))



    // bodyFormData.append('profile_picture', '')
    if (imagepath == '') {
        bodyFormData.append('image', '')
    } else {
        bodyFormData.append('image', {
            uri: imagepath,
            name: 'selfie.jpg',
            type: 'image/jpg'
        })
    }

    //bodyFormData.append('Content-Type', 'image/jpeg')



    const sendGetRequest = async () => {
        try {
            const resp = await axios.get('https://jsonplaceholder.typicode.com/posts');
            console.log(resp.data);
        } catch (err) {
            // Handle Error Here
            console.error(err);
        }
    };


    const creatprofilebasicinfoupdate = async () => {
        try {

            setLoading(true);
            // alert(token)
            console.log(JSON.stringify(selectedItems))
            // alert(Fname + "--" + Lname + "--" + Nickname + "--" + PhoneNo + "--" + "--" + Gender + "--" + JSON.stringify(LanguageSpoken2) + "--" + Street + "-" + Pin + "" + data2.selectLabel + "" + data3.selectLabel3 + "" + resdata11 + "" + Aboutmeshort + "" + Aboutmeshort + "" + Experience + "" + Youtube + "" + Twiter + "" + Facebook + "" + Blog + "" + Website + JSON.stringify(imagepath) + "" + JSON.stringify(videopath))
            console.log(JSON.stringify(imagepath))
            //alert(Authorization)
            await axios({
                method: 'post',
                url: 'https://appleskool.com/preview/appleskool_code/api/update-student-profile',
                data: bodyFormData,
                headers: {
                    'Content-Type': 'multipart/form-data',
                    'Authorization': 'Bearer' + token
                }
            }).then(function (response) {
                //handle success
                console.log(response);
                setLoading(false);
                setTimeout(() => {
                    Snackbar.show({
                        text: 'Profile updated successfully',
                        duration: Snackbar.LENGTH_SHORT,
                        textColor: 'green',
                    });
                }, 1000);
                //  alert(JSON.stringify(response.data))
                //navigation.navigate("ScreenCreatProfileSubjectSkill")
            })
                .catch(function (response) {
                    setLoading(false);
                    //handle error
                    // navigation.navigate("ScreenCreatProfileSubjectSkill")
                    //alert(JSON.stringify(response.data.error))
                    console.log(response);
                });

        } catch (err) {
            console.log(err)

        }


    }

    const selectSkiil = async (arrayLis) => {
        arrayLis.map((myValue, myIndex) => {

            langnew.push(myValue.id);

        });

        setSelectedItems(langnew)
        console.log(langnew + "   " + selectedItems)
    }


    useEffect(() => {
        if (token) {
            //alert("Useeffect If Condition")
            creatprofilebasicinfo()
            //selectState()
        } else {
            // alert("Please Login Again")
            AsyncStorage.getItem('UID123', (err, result) => {
                setToken(result);
                // setTimeout(()=>{
                //    
                // }, 3000);
            })
        }
    }, [token])
    const textnumber = (val) => {
        if (val.length !== 0) {
            setData({
                ...data,
                phonenumber: val,
                isValiedphonenumber: true,
            });
        } else {
            setData({
                ...data,
                isValiedphonenumber: null,
            });
        }
    }
    const RadioButton = props => {
        return (
            <TouchableOpacity style={styles.circle} onPress={props.onPress}>
                {props.checked ? (<View style={styles.checkedCircle} />) : (<View />)}
            </TouchableOpacity>
        )
    };
    const radioHandler = () => {
        //logic goes here
        if (femaleCheck) {
            setFemaleCheck(false); //for avoiding multiple checks at a time;
            setKey('male');
            setOption1(true);
            //setData11('male')
            // value=
        } else {
            setOption1(false);
            setFemaleCheck(true)
            setKey('female');
            //setData11('female')

        }
    }
    const radioHandler2 = () => {
        if (option1) {
            setOption1(false);
            setKey('female');
            setFemaleCheck(true);
            //setData11('female')
            // value=
        } else {
            setKey('male');
            setFemaleCheck(false);
            setOption1(true)
            //setData11('male')

        }
    }
    var pickerItem2 = [];

    let pickerss = resdata4.map((myValue, myIndex) => {
        //  console.log('myValue: ' + myValue.state)
        pickerItem2.push({ label: myValue.city_name, value: myValue.id })

    });
    var pickerItem = [];

    let pickers = resdata3.map((myValue, myIndex) => {
        //  console.log('myValue: ' + myValue.state)
        pickerItem.push({ label: myValue.state, value: myValue.id })

    });

    var pickerItem3 = [];

    let pickersss = Occupation.map((myValue, myIndex) => {
        //  console.log('myValue: ' + myValue.state)
        pickerItem3.push({ label: myValue.occupation_name, value: myValue.id })

    });
    var pickerItem4 = [];

    let pickersssss = Educat.map((myValue, myIndex) => {
        //  console.log('myValue: ' + myValue.state)
        pickerItem4.push({ label: myValue.education_name, value: myValue.id })

    });
    var pickerItem5 = [];

    let pickersssssss = Industr.map((myValue, myIndex) => {
        //  console.log('myValue: ' + myValue.state)
        pickerItem5.push({ label: myValue.industry_name, value: myValue.id })

    });

    var pickerItem6 = [{ label: 1, value: 1 }, { label: 2, value: 2 }, { label: 3, value: 3 }, { label: 4, value: 4 }, { label: 5, value: 5 }, { label: 6, value: 6 }, { label: 7, value: 7 }]
    var langSpacific2 = [];
    let langua = resdata5.map((myValue, myIndex) => {
        // console.log('myValue: ' + myValue.name)
        langSpacific2.push({
            id: myValue.id,
            name: myValue.skill_name
        });
        //  return (
        //      <Picker.Item label={myValue.name} value={myValue.id} key={myIndex} />
        //  )
    });


    return (
        <View style={styles.container}>
            <Spinner
                //visibility of Overlay Loading Spinner
                visible={loading}
                color={"black"}
                overlayColor={'rgba(255,255,255, 1)'}
                //Text with the Spinner
                textContent={'Loading...'}
            //Text style of the Spinner Text
            //textStyle={styles.spinnerTextStyle}
            />
            <StatusBar backgroundColor='#961b37' barStyle="light-content" />

            <ScrollView>
                {/* <Text>icon1</Text> */}
                <View style={{ padding: 15, }}>
                    <Text style={{ fontSize: 22, fontWeight: 'bold' }}>Edit Profile</Text>
                </View>

                <View style={{ margin: 10 }}>
                    <Text>First Name</Text>
                </View>
                <View style={styles.action}>
                    <TextInput
                        placeholder="First name"
                        style={styles.textInput}
                        value={Fname}
                        autoCapitalize="none"
                        onChangeText={(val) => setFname(val)}
                    />
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Last Name</Text>
                </View>
                <View style={styles.action}>
                    <TextInput
                        placeholder="Last name"
                        style={styles.textInput}
                        value={Lname}
                        autoCapitalize="none"
                        onChangeText={(val) => setLname(val)}
                    />
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Nick Name(Max 15 Latter)</Text>
                </View>
                <View style={styles.action}>
                    <TextInput
                        placeholder="Nick Name"
                        style={styles.textInput}
                        value={Nickname}
                        autoCapitalize="none"
                        onChangeText={(val) => setNickname(val)}
                    />
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Phone No</Text>
                </View>
                <View style={{ borderWidth: 0.6, borderRadius: 1.5, borderColor: '#505050', marginHorizontal: 10, height: 55 }}>
                    <TextInput
                        keyboardType='numeric'
                        value={PhoneNo}
                        onChangeText={(text) => {
                            setPhoneNo(text);
                        }}
                        placeholder={resdata.phone_no}

                    />


                </View>
                <View style={{ margin: 10 }}>
                    <Text>Email</Text>
                </View>
                <View style={styles.action}>
                    <TextInput
                        placeholder="abcd@gmail.com"
                        style={styles.textInput}
                        autoCapitalize="none"
                        value={resdata.email}
                    // onChangeText={(val) => textFirstName(val)}
                    />
                </View>
                <View style={{ margin: 10 }}>
                    <Text>D.O.B</Text>
                </View>
                <View style={{ margin: 10 }}>
                    <TouchableOpacity style={{
                        // height: 45, width: '100%',
                        // padding: 5,
                        // // marginRight: 10,
                        // // marginLeft: 10,
                        // borderWidth: 0.6,
                        // borderRadius: 5,
                        // borderColor: '#505050',
                    }}>
                        <View style={{ flexDirection: 'row', alignItems: 'stretch', justifyContent: 'space-between' }}>
                            <Text style={{ fontSize: 13, color: '#898b8a', alignItems: 'flex-start', justifyContent: 'flex-start', paddingTop: 8, paddingLeft: 5 }}>

                                {/* {resdata.dob} */}

                            </Text>
                            <View styles={{ paddingTop: 10, }}
                                onPress={showDatePicker}
                            >
                                <DateTimePickerModal
                                    isVisible={isDatePickerVisible}
                                    style={{ width: 375 }}
                                    date={Date} // Initial date from state
                                    mode="date" // The enum of date, datetime and time
                                    // placeholder="select date"
                                    format="DD-MM-YYYY"
                                    confirmBtnText="Confirm"
                                    cancelBtnText="Cancel"
                                    onDateChange={(date) => {
                                        handleConfirm(date);
                                    }}

                                />
                                {/* <Icon name="calendar" size={23} color="#898b8a"
                                    onPress={showDatePicker}
                                /> */}
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Gender</Text>
                </View>
                {/* <View style={{ flexDirection: 'column' }}>
                        <RadioButton.Group style={{ flexDirection: 'column' }}>
                            <View style={{ flexDirection: 'row', paddingLeft: 50 }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <RadioButton value="Male" />
                                    <Text style={{ paddingTop: 8 }}>First</Text>
                                </View>
                                <View style={{ flexDirection: 'row' }}>
                                    <RadioButton value="Female" />
                                    <Text style={{ paddingTop: 8 }}>Second</Text>
                                </View>
                            </View>
                        </RadioButton.Group>
                    </View> */}
                <View style={{ flexDirection: 'row', paddingLeft: 50 }}>
                    <Text>Male:</Text>
                    <RadioButton checked={option1} onPress={radioHandler} />
                    <Text>Female:</Text>
                    <RadioButton checked={femaleCheck} onPress={radioHandler2} />
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Interested Skill (Max 3)</Text>
                </View>
                <MultiSelect
                    hideTags
                    items={langSpacific2}
                    uniqueKey="id"
                    onSelectedItemsChange={onSelectedItemsChange}
                    selectedItems={selectedItems}
                    selectText="Interested Skill (Max 3)"
                    searchInputPlaceholderText="Interested Skill..."
                    onChangeInput={(text) => (text)}
                    // languageSpokenName
                    tagRemoveIconColor="#CCC"
                    tagBorderColor="#CCC"

                    tagTextColor="#CCC"
                    selectedItemTextColor="#dc2359"
                    selectedItemIconColor="#CCC"
                    itemTextColor="#900"
                    displayKey="name"
                    searchInputStyle={{ color: '#fff' }}
                    submitButtonColor="#dc2359"
                    submitButtonText="Submit"
                    hideSubmitButton='true'
                />



                <View style={styles.lineStyle} />
                <View style={{ margin: 10 }}>
                    <Text style={{ fontSize: 17, fontWeight: 'bold' }}>Address Information</Text>
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Full Address</Text>
                </View>
                <View style={styles.action}>
                    <TextInput
                        // placeholder="Your First name"
                        style={styles.textInput}
                        autoCapitalize="none"
                        value={Street}
                        onChangeText={(val) => setStreet(val)}
                    />
                </View>
                <View style={{ margin: 10 }}>
                    <Text>State</Text>
                </View>
                <View style={{ height: 45, borderRadius: 5, borderColor: "#131313", borderWidth: .5, color: "#131313", marginVertical: 10, marginHorizontal: 10 }}>
                    <Dropdown
                        style={{ paddingStart: 10, height: 25, marginTop: -20 }}
                        data={pickerItem}
                        value={parseInt(data2.selectLabel)}
                        baseColor={'#ffffff'}
                        labelFontSize={0}
                        onChangeText={
                            (dropdownitemvalue) => {
                                selectState2(dropdownitemvalue)
                            }}

                    />

                </View>
                <View style={{ margin: 10 }}>
                    <Text>City</Text>
                </View>
                <View style={{ height: 45, borderRadius: 5, borderColor: "#131313", borderWidth: .5, color: "#131313", marginVertical: 10, marginHorizontal: 10 }}>
                    <Dropdown
                        style={{ paddingStart: 10, height: 25, marginTop: -20 }}
                        data={pickerItem2} baseColor={'#ffffff'}
                        labelFontSize={0}
                        onChangeText={
                            (dropdownitemvalue) => {
                                setSelectLabel3({ selectLabel3: dropdownitemvalue })
                            }}
                        value={parseInt(data3.selectLabel3)}
                    />

                </View>
                <View style={{ margin: 10 }}>
                    <Text>Pin</Text>
                </View>
                <View style={styles.action}>
                    <TextInput
                        // placeholder="Your First name"
                        style={styles.textInput}
                        autoCapitalize="none"
                        value={Pin}
                        onChangeText={(val) => setPin(val)}
                    />
                </View>


                <View style={{ margin: 10 }}>
                    <Text>Country</Text>
                </View>
                <View style={styles.action}>
                    <TextInput
                        placeholder="Your First name"
                        style={styles.textInput}
                        autoCapitalize="none"
                        value={Country}
                        onChangeText={(val) => setCountry(val)}
                    />
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Highest Education</Text>
                </View>
                <View style={{ height: 45, borderRadius: 5, borderColor: "#131313", borderWidth: .5, color: "#131313", marginVertical: 10, marginHorizontal: 10 }}>
                    <Dropdown
                        style={{ paddingStart: 10, height: 25, marginTop: -20 }}
                        data={pickerItem4}
                        value={parseInt(data5.selectLabel5)}
                        baseColor={'#ffffff'}
                        labelFontSize={0}
                        onChangeText={
                            (dropdownitemvalue) => {
                                setSelectLabel5({ selectLabel5: dropdownitemvalue })
                            }}

                    />

                </View>
                <View style={{ margin: 10 }}>
                    <Text>Current Occupation</Text>
                </View>
                <View style={{ height: 45, borderRadius: 5, borderColor: "#131313", borderWidth: .5, color: "#131313", marginVertical: 10, marginHorizontal: 10 }}>
                    <Dropdown
                        style={{ paddingStart: 10, height: 25, marginTop: -20 }}
                        data={pickerItem3}
                        value={parseInt(data4.selectLabel4)}
                        baseColor={'#ffffff'}
                        labelFontSize={0}
                        onChangeText={
                            (dropdownitemvalue) => {
                                setSelectLabel4({ selectLabel4: dropdownitemvalue })
                            }}

                    />

                </View>
                <View style={{ margin: 10 }}>
                    <Text>Work Experience(In Years)</Text>
                </View>
                <View style={{ height: 45, borderRadius: 5, borderColor: "#131313", borderWidth: .5, color: "#131313", marginVertical: 10, marginHorizontal: 10 }}>
                    <Dropdown
                        style={{ paddingStart: 10, height: 25, marginTop: -20 }}
                        data={pickerItem6}
                        value={parseInt(data7.selectLabel7)}
                        baseColor={'#ffffff'}
                        labelFontSize={0}
                        onChangeText={
                            (dropdownitemvalue) => {
                                setSelectLabel7({ selectLabel7: dropdownitemvalue })
                            }}

                    />

                </View>
                <View style={{ margin: 10 }}>
                    <Text>Industry</Text>
                </View>
                <View style={{ height: 45, borderRadius: 5, borderColor: "#131313", borderWidth: .5, color: "#131313", marginVertical: 10, marginHorizontal: 10 }}>
                    <Dropdown
                        style={{ paddingStart: 10, height: 25, marginTop: -20 }}
                        data={pickerItem5}
                        value={parseInt(data6.selectLabel6)}
                        baseColor={'#ffffff'}
                        labelFontSize={0}
                        onChangeText={
                            (dropdownitemvalue) => {
                                setSelectLabel6({ selectLabel6: dropdownitemvalue })
                            }}

                    />

                </View>

                <View style={styles.lineStyle} />
                <View style={{ margin: 10 }}>
                    <Text>Profile Photo</Text>
                </View>
                <View >
                    <TouchableOpacity>
                        <View style={{}}>

                            {loading4 == true ? <Image visible={loading4} source={{ uri: 'https://appleskool.com/preview/appleskool_code/storage/app/public/profile_picture/' + resdata.profile_picture }} style={{ height: 110, width: 110, margin: 5 }} /> : <Image source={{ uri: imagepath }} style={{ height: 110, width: 110, margin: 5 }} />}
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={{ margin: 10 }}>
                    <TouchableOpacity style={{
                        height: 45, width: '100%',
                        padding: 5,
                        backgroundColor: '#e6a000', borderRadius: 5
                    }}
                        onPress={takePhotoFromGallery}
                    //value={IMage}
                    //  value={imagepath}
                    >
                        <View style={{ flexDirection: 'row', alignItems: 'stretch', justifyContent: 'space-between' }}>
                            <Text style={{ fontSize: 13, color: 'white', alignItems: 'flex-start', justifyContent: 'flex-start', paddingTop: 8 }}>
                                {/* {imagepath} */}
                                Upload Profile Picture
                            </Text>
                            <View style={{ paddingTop: 6, paddingRight: 10 }}>
                                <IconIcon name="upload" size={23} color="#ffffff" />
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={{ margin: 15 }}>
                    <TouchableOpacity style={{
                        height: 35, justifyContent: 'center', alignItems: 'center', width: '33%', padding: 1, backgroundColor: '#d51c6d', borderRadius: 5
                    }}
                        //onPress={() => navigation.navigate("ScreenCreatProfileSubjectSkill")}
                        onPress={() => creatprofilebasicinfoupdate()}
                    >
                        <Text style={{ fontSize: 13, color: 'white', fontWeight: 'bold' }}>Save & Continue</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        </View>
        // {part2 ? <View><Text style={{ color:"#5a00b5" }}>_________________</Text><Text style={{ color:"#5a00b5" }}>abhijit</Text></View> : <View><Text style={{ color:"#2cd383" }}>______________</Text></View>}
        // {part3 ? <View><Text style={{ color:"#fffeff" }}>______________</Text></View> : <View><Text style={{ color:"#749182" }}>________________</Text><Text style={{ color:"#749182" }}>abhijit</Text></View>}
    );
};
export default StudentEditProfile;

const styles = StyleSheet.create({
    outerView: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        alignContent: 'center',
        //     flexDirection: 'row',
        //     alignItems: 'center',
        //     justifyContent: 'center',
        paddingTop: 10,
        paddingBottom: 20,
    },
    container: {
        flex: 1,
        backgroundColor: 'white',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 5,
        //paddingBottom: 10,
        // },
    },
    lineStyle: {
        borderWidth: .4,
        borderColor: 'black',
        margin: 15,
        marginTop: 25
    },
    action: {
        //flexDirection: 'row',
        marginRight: 10,
        marginLeft: 10,
        borderWidth: 0.6,
        borderRadius: 5,
        borderColor: '#505050',
        //padding: 5
    },
    textInput: {
        //flex: 1,
        //marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        color: '#05375a',
        height: 40
    },

    circle: {
        height: 20,
        width: 20,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#ACACAC',
        alignItems: 'center', // To center the checked circle…
        justifyContent: 'center',
        marginHorizontal: 10
    },
    checkedCircle: {
        width: 14,
        height: 14,
        borderRadius: 7,
        backgroundColor: '#fa0562' // You can set it default or with yours one…
    },
    // textContainerStyle{
    //     height:20
    // }
});
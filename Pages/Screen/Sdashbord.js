import React, { Component, useState, useEffect } from 'react';
import {
    StyleSheet,
    Text,
    View, Dimensions, AsyncStorage,
    TextInput,
    TouchableOpacity,
    TouchableHighlight,
    Image, ScrollView, StatusBar
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { Card } from "react-native-elements";
import Spinner from 'react-native-loading-spinner-overlay';
import axios from 'axios';
const screenWidth = Math.round(Dimensions.get('window').width);

const StudentDashboard = ({ navigation }) => {
    const [token, setToken] = React.useState('');

    const [loading, setLoading] = useState(false);
    const [images, setImages] = React.useState([
        require('../../icon/banner1.png'),
        require('../../icon/banner2.png'),
        require('../../icon/banner3.png'),
    ])
    const [traing, setTraing] = React.useState([
        require('../../icon/featured-training-img.png'),
        require('../../icon/featured-training-img2.png'),
        require('../../icon/featured-training-img3.png'),
    ])
    const [instructorImage, setInstructorImage] = React.useState([
        require('../../icon/instructor1.png'),
        require('../../icon/instructor2.png'),
        require('../../icon/instructor3.png'),
    ])

    const [userdata, setDataUser] = useState('');
    const [userState, setDataState] = useState('');
    const [userCity, setDataCity] = useState('');
    useEffect(() => {
        if (token) {
            HomeApi()
        } else {
            AsyncStorage.getItem('UID123', (err, result) => {
                setToken(result);
                // setTimeout(()=>{
                //    
                // }, 3000);
            })
        }
    }, [token])
    const HomeApi = () => {

        setLoading(true);
        // setLoding(false)
        // alert(data.email+data.password)
        axios.defaults.baseURL = 'https://appleskool.com/preview/appleskool_code/api';
        axios.defaults.headers.post['Content-Type'] =
            'application/json;charset=utf-8';
        axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';
        axios.defaults.headers.post['Access-Control-Allow-Headers'] = '*';
        //alert(global.userToken)
        const bodyParameters = {
        };
        axios
            .post('https://appleskool.com/preview/appleskool_code/api/student/dashboard',
                bodyParameters,
                {
                    headers: {
                        Authorization: `Bearer ${token}`,
                        // Authorization: `Bearer  ${userToken}`,global.userToken
                    },
                },
            )
            .then(


                res => {
                    setLoading(false);
                    console.log(res.data.user.user_state.state);
                    //global.Me = me.data.firstname + me.data.lastname;

                    setDataUser(
                        res.data.user
                    );
                    setDataState(res.data.user.user_state.state);
                    setDataCity(res.data.user.user_city.city_name);

                },
                error => {
                    setLoading(false);
                    //  alert("Something went wrong. Please try again")
                },
            );
    };

    return (
        <View style={styles.drawerContent}>
            <Spinner
                //visibility of Overlay Loading Spinner
                visible={loading}
                color={"black"}
                overlayColor={'rgba(255,255,255, 1)'}
                //Text with the Spinner
                textContent={'Loading...'}
            //Text style of the Spinner Text
            //textStyle={styles.spinnerTextStyle}
            />
            <ScrollView>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#cecece' }}>
                    <View>
                        <Text style={{ fontSize: 26, margin: 10, }}>Dashboard</Text>
                    </View>


                </View>
                <View style={{ margin: 10 }}>
                    <Text style={{ fontSize: 19, fontWeight: 'bold' }}>Welcome {userdata.fname}</Text>
                    <View>
                        <Text style={{ marginTop: 10, fontSize: 17, fontWeight: 'bold' }}>Basic Information</Text>
                        <View style={{ flexDirection: 'column', flex: 0.3 }}>
                            <View style={{ margin: 10 }}>
                                <Text style={{ fontSize: 16 }}>Full Name</Text>
                                <Text>{userdata.fname + " " + userdata.lname}</Text>
                            </View>
                            <View style={{ margin: 10 }}>
                                <Text style={{ fontSize: 16 }}>Nick Name</Text>
                                <Text>{userdata.nick_name}</Text>
                            </View>
                            <View style={{ margin: 10 }}>
                                <Text >Email</Text>
                                <Text>{userdata.email}</Text>
                            </View>
                        </View>
                    </View>
                    <View >
                        <Text style={{ fontSize: 16, fontWeight: 'bold' }}>Address Information</Text>
                        <View style={{ margin: 10 }}>
                            <Text>Full Address</Text>
                            <Text>{userdata.street}</Text>
                        </View>
                        <View style={{ flexDirection: 'column' }}>
                            <View style={{ margin: 10 }}>
                                <Text>State</Text>
                                <Text>{userState}</Text>
                            </View>
                            <View style={{ margin: 10 }}>
                                <Text>City</Text>
                                <Text>{userCity}</Text>
                            </View>
                            <View style={{ margin: 10 }}>
                                <Text>Pincode</Text>
                                <Text>{userdata.pin}</Text>
                            </View>
                            <View style={{ margin: 10 }}>
                                <Text>Country</Text>
                                <Text>{userdata.country_id}</Text>
                            </View>
                        </View>
                    </View><Text style={{ fontSize: 16, fontWeight: 'bold' }}>Profile Picture</Text>
                    <View style={{ flexDirection: 'column' }}>
                        <Image source={{ uri: 'https://appleskool.com/preview/appleskool_code/storage/app/public/profile_picture/' + userdata.profile_picture }} style={styles.cardImageALLCourses} />

                    </View>
                </View>
            </ScrollView>
        </View>
    );
};
export default StudentDashboard;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff"
        //justifyContent: 'center',
        //alignItems: 'center',
        // width:Math.round(Dimensions.get('window').width),
    },
    cardImage: {
        height: '100%',
        width: '100%',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#d73c5e00',
        marginRight: 15,
    },
    cardImageALLCourses: {
        height: 115,
        width: 115,
        borderColor: 'transparent',
        borderWidth: 1,
        borderRadius: 10
    },
    cardImage2: {
        height: '100%',
        width: '99%',
        borderRadius: 15,
        borderWidth: 1,
        borderColor: '#d73c5e00',
        marginRight: 15,
    },
    banner1stText: {
        position: 'absolute',
        margin: 7,
        fontSize: 16,
        fontWeight: 'bold',
        marginTop: 10,
        textAlign: 'center',
        justifyContent: 'center',
        fontFamily: 'roboto-regular',
        marginLeft: 12
    },
    banner2stText: {
        position: 'absolute',
        margin: 7,
        fontSize: 11,
        marginTop: 36,
        marginRight: 10,
        color: '#6c6763',
        fontFamily: 'roboto-light',
        marginLeft: 12
    },
    banner3stText: {
        // position: 'absolute', 
        // marginTop:80,
        fontSize: 13,
        textAlign: 'center',
        justifyContent: 'center',
        color: 'white',
        backgroundColor: '#d73c5e',
        marginTop: 80,
        width: '50%',
        height: 28,
        marginLeft: 12,
        paddingTop: 5,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#d73c5e',
        fontFamily: 'roboto-bold',
    },
    instructorcardImage: {
        height: 150,
        width: '100%',
        borderRadius: 4,
        marginTop: 5,
        marginBottom: 15
    },
    overlay: {
        ...StyleSheet.absoluteFillObject,
        backgroundColor: 'rgba(255,255,255,.8)',
        height: '70%',
        width: '80%',
        alignContent: 'center',
        alignItems: 'baseline',
        borderBottomEndRadius: 5,
        borderTopEndRadius: 5,
        borderWidth: 1,
        borderColor: '#fff',
        marginTop: 40,
        marginBottom: 10
    },
    category: {
        height: 45,
        width: '95%',
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'space-between',
        //  backgroundColor:'red',
        marginTop: 15,
        marginLeft: 5
    },
    category2: {
        height: 45,
        width: '95%',
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'space-between',
        //  backgroundColor:'red',
        marginLeft: 5
    },
    buttonContainer: {
        height: 35,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20,
        paddingLeft: 10,
        paddingRight: 10,
        borderRadius: 30,
        marginTop: 10,
        marginLeft: 15,
        borderColor: '#d73c5e',
        borderWidth: 1
    },
    inputIcon: {
        width: 25,
        height: 25,
        marginLeft: 1,
        marginTop: 4,
        marginRight: 2,
        justifyContent: 'center'
    },
    cardshadow: {
        marginRight: 12,
        // borderTopWidth: 0,
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 2,
        //  borderWidth: 1,
        borderRadius: 3,
        marginLeft: 10
    },
    SplashScreen_RootView:
    {
        justifyContent: 'center',
        flex: 1,
        backgroundColor: '#db4f6e',
        position: 'absolute',
        width: '100%',
        height: '100%',
    },
    SplashScreen_ChildView:
    {
        ...StyleSheet.absoluteFillObject,
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
});
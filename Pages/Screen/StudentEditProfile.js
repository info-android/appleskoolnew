import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    Text,
    Image,
    View,
    ScrollView,
    Dimensions,
    TouchableOpacity,
    TextInput,
    StatusBar
} from 'react-native';
import PhoneInput from "react-native-phone-number-input";

//import PhoneInput from 'react-native-phone-input'
//import 'react-phone-number-input/style.css'
//import PhoneInput from 'react-phone-number-input'
import { RadioButton } from 'react-native-paper';
import IconIcon from 'react-native-vector-icons/FontAwesome';
import IconIconIcon from 'react-native-vector-icons/FontAwesome5';
import Icon from 'react-native-vector-icons/Ionicons';
import Picker from '../Components/Picker'

const icon1 = '<IconIcon name="check" size={20} color="#ffffff" />'


const StudentEditProfile = ({ navigation }) => {
    const [checked, setChecked] = React.useState('first');
    //const [value, setValue] = React.useState()

    // constructor(props) {
    //     super(props);
    //     //global.Currentstate = navigation.state.routeName;
    //     // this.state = { setData: '' };
    //     this.state = {
    //         // token: '',
    //         productArray: [],
    //         isLoading: true

    //       }
    // }
    const textnumber = (val) => {
        if (val.length !== 0) {
            setData({
                ...data,
                phonenumber: val,
                isValiedphonenumber: true,
            });
        } else {
            setData({
                ...data,
                isValiedphonenumber: null,
            });
        }
    }

    return (
        <View style={styles.container}>
            <StatusBar backgroundColor='#961b37' barStyle="light-content" />

            <ScrollView>
                {/* <Text>icon1</Text> */}
                <View style={{ padding: 15, }}>
                    <Text style={{ fontSize: 22, fontWeight: 'bold' }}>Edit Profile</Text>
                </View>

                <View style={{ margin: 10 }}>
                    <Text>First Name</Text>
                </View>
                <View style={styles.action}>
                    <TextInput
                        placeholder="First name"
                        style={styles.textInput}
                        autoCapitalize="none"
                    // onChangeText={(val) => textFirstName(val)}
                    />
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Last Name</Text>
                </View>
                <View style={styles.action}>
                    <TextInput
                        placeholder="Last name"
                        style={styles.textInput}
                        autoCapitalize="none"
                    // onChangeText={(val) => textFirstName(val)}
                    />
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Nick Name(Max 15 Latter)</Text>
                </View>
                <View style={styles.action}>
                    <TextInput
                        placeholder="Nick Name"
                        style={styles.textInput}
                        autoCapitalize="none"
                    // onChangeText={(val) => textFirstName(val)}
                    />
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Phone No</Text>
                </View>
                <View style={{ borderWidth: 0.6, borderRadius: 1.5, borderColor: '#505050', marginHorizontal: 10, height: 55 }}>
                    <PhoneInput style={styles.phonestyle}
                        // ref={phoneInput}
                        // defaultValue={value}
                        // defaultCode="DM"
                        // onChangeText={(text) => {
                        //   setValue(text);
                        // }}
                        // onChangeFormattedText={(number) => {
                        //     textnumber(number)
                        // }}
                        placeholder="0123456789"
                    //textContainerStyle="height=10"
                    />

                    {/* <PhoneInput /> */}

                    {/* <PhoneInput
                        placeholder="Enter phone number"
                        value={value}
                        onChange={setValue} /> */}
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Email</Text>
                </View>
                <View style={styles.action}>
                    <TextInput
                        placeholder="abcd@gmail.com"
                        style={styles.textInput}
                        autoCapitalize="none"
                    // onChangeText={(val) => textFirstName(val)}
                    />
                </View>
                <View style={{ margin: 10 }}>
                    <Text>D.O.B</Text>
                </View>
                <View style={styles.action}>
                    <TextInput
                        placeholder="10/07/1997"
                        style={styles.textInput}
                        autoCapitalize="none"
                    // onChangeText={(val) => textFirstName(val)}
                    />
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Gender</Text>
                </View>
                {/* <View style={{ flexDirection: 'column' }}>
                        <RadioButton.Group style={{ flexDirection: 'column' }}>
                            <View style={{ flexDirection: 'row', paddingLeft: 50 }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <RadioButton value="Male" />
                                    <Text style={{ paddingTop: 8 }}>First</Text>
                                </View>
                                <View style={{ flexDirection: 'row' }}>
                                    <RadioButton value="Female" />
                                    <Text style={{ paddingTop: 8 }}>Second</Text>
                                </View>
                            </View>
                        </RadioButton.Group>
                    </View> */}
                <View style={{ flexDirection: 'row', paddingLeft: 50 }}>
                    <View style={{ flexDirection: 'row' }}>
                        <RadioButton
                            value="first"
                            status={checked === 'first' ? 'checked' : 'unchecked'}
                            onPress={() => setChecked('first')}
                        // style={{ colour:'#900' }}
                        />
                        <Text style={{ paddingTop: 8 }}>Male</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <RadioButton
                            value="second"
                            status={checked === 'second' ? 'checked' : 'unchecked'}
                            onPress={() => setChecked('second')}
                        />
                        <Text style={{ paddingTop: 8 }}>Female</Text>
                    </View>
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Interested Skiil (Max 3)</Text>
                </View>
                <View style={styles.action}>
                    <TextInput
                        placeholder="Ex-English"
                        style={styles.textInput}
                        autoCapitalize="none"
                    // onChangeText={(val) => textFirstName(val)}
                    />
                </View>



                <View style={styles.lineStyle} />
                <View style={{ margin: 10 }}>
                    <Text style={{ fontSize: 17, fontWeight: 'bold' }}>Address Information</Text>
                </View>
                <View style={{ margin: 10 }}>
                    <Text>Full Address</Text>
                </View>
                <View style={styles.action}>
                    <TextInput
                        // placeholder="Your First name"
                        style={styles.textInput}
                        autoCapitalize="none"
                    // onChangeText={(val) => textFirstName(val)}
                    />
                </View>
                <View style={{ margin: 10 }}>
                    <Text>State</Text>
                </View>
                <Picker />
                <View style={{ margin: 10 }}>
                    <Text>City</Text>
                </View>
                <Picker />
                <View style={{ margin: 10 }}>
                    <Text>Pin</Text>
                </View>
                <View style={styles.action}>
                    <TextInput
                        // placeholder="Your First name"
                        style={styles.textInput}
                        autoCapitalize="none"
                    // onChangeText={(val) => textFirstName(val)}
                    />
                </View>


                <View style={{ margin: 10 }}>
                    <Text>Country</Text>
                </View>
                <View style={styles.action}>
                    <TextInput
                        placeholder="Your First name"
                        style={styles.textInput}
                        autoCapitalize="none"
                    // onChangeText={(val) => textFirstName(val)}
                    />
                </View>
                <View style={styles.lineStyle} />
                <View style={{ margin: 10 }}>
                    <Text>Profile Photo</Text>
                </View>
                <View style={{ margin: 10 }}>
                    <TouchableOpacity style={{
                        height: 45, width: '100%',
                        padding: 5,
                        backgroundColor: '#e6a000', borderRadius: 5
                    }}>
                        <View style={{ flexDirection: 'row', alignItems: 'stretch', justifyContent: 'space-between' }}>
                            <Text style={{ fontSize: 13, color: 'white', alignItems: 'flex-start', justifyContent: 'flex-start', paddingTop: 8 }}>Click hear Uplode Photo</Text>
                            <View style={{ paddingTop: 6 }}>
                                <IconIcon name="upload" size={23} color="#ffffff" />
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        </View>
        // {part2 ? <View><Text style={{ color:"#5a00b5" }}>_________________</Text><Text style={{ color:"#5a00b5" }}>abhijit</Text></View> : <View><Text style={{ color:"#2cd383" }}>______________</Text></View>}
        // {part3 ? <View><Text style={{ color:"#fffeff" }}>______________</Text></View> : <View><Text style={{ color:"#749182" }}>________________</Text><Text style={{ color:"#749182" }}>abhijit</Text></View>}
    );
};
export default StudentEditProfile;

const styles = StyleSheet.create({
    outerView: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        alignContent: 'center',
        //     flexDirection: 'row',
        //     alignItems: 'center',
        //     justifyContent: 'center',
        paddingTop: 10,
        paddingBottom: 20,
    },
    container: {
        flex: 1,
        backgroundColor: 'white',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 5,
        //paddingBottom: 10,
        // },
    },
    lineStyle: {
        borderWidth: .4,
        borderColor: 'black',
        margin: 15,
        marginTop: 25
    },
    action: {
        //flexDirection: 'row',
        marginRight: 10,
        marginLeft: 10,
        borderWidth: 0.6,
        borderRadius: 5,
        borderColor: '#505050',
        //padding: 5
    },
    textInput: {
        //flex: 1,
        //marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        color: '#05375a',
        height: 40
    },
    phonestyle: {
        height: 20,
        width: 100
    },
    // textContainerStyle{
    //     height:20
    // }
});
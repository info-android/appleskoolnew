import React, { useState, useEffect } from 'react';
import { View, Text, Button, StyleSheet, TextInput, TouchableOpacity, ScrollView, StatusBar, AsyncStorage, FlatList } from 'react-native';
import Picker from './Components/Picker'
import { RadioButton } from 'react-native-paper';
import Spinner from 'react-native-loading-spinner-overlay';
import axios from 'axios';
import Icon from 'react-native-vector-icons/Ionicons';
import IconIcon from 'react-native-vector-icons/FontAwesome';
import IconIconIcon from 'react-native-vector-icons/FontAwesome5';
import Moment from 'moment';

const MyTraining = ({ navigation }) => {
    const [checked, setChecked] = React.useState('first');
    const [token, setToken] = React.useState('');
    const [loading, setLoading] = useState(false);
    const [MyTraining, setMyTraining] = useState([]);
    const myTrainingTOKEN = () => {

        setLoading(true);
        //alert(token);

        // setLoding(false)
        // alert(data.email+data.password)
        axios.defaults.baseURL = 'https://appleskool.com/preview/appleskool_code/api/my-training';
        axios.defaults.headers.post['Content-Type'] =
            'application/json;charset=utf-8';
        axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';
        axios.defaults.headers.post['Access-Control-Allow-Headers'] = '*';
        //alert(global.userToken)
        const bodyParameters = {
            "jsonrpc": "2.0",
            "params": {
                "from_date": "",
                "to_date": "",
                "status": ""
            }
        };
        axios
            .post('https://appleskool.com/preview/appleskool_code/api/my-training',
                bodyParameters,
                {
                    headers: {
                        Authorization: `Bearer ${token}`,
                        // Authorization: `Bearer  ${userToken}`,global.userToken
                    },
                },
            )
            .then(
                res => {
                    setLoading(false);

                    //alert(JSON.stringify(res.data.myTrainings[1].topic));
                    //global.Me = me.data.firstname + me.data.lastname;
                    setMyTraining(res.data.myTrainings);
                    //setMaster_skills(res.data.master_skills);

                    // setdata(res.data.teacher_skills.skills.skill_name);
                    //alert(JSON.stringify(res.data.master_skills[3].skill_name))
                    // alert(JSON.stringify(res.data.categories[1].name))
                },
                error => {
                    setLoading(false);
                   // alert("myTrainingTOKEN api error")
                },
            );
    };

    let BASICDATA1 = MyTraining.map((myValue, myIndex) => {
        //  console.log('myValue: ' + myValue.city_name)
        return (
            <>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginLeft: 5, marginRight: 5, alignSelf: 'stretch', }}>
                    <View style={{ flex: 1, alignSelf: 'stretch', marginRight: '5%' }}>
                        <Text style={{ fontSize: 16 }} >{myValue.topic}</Text>
                    </View>
                    <View style={{ flex: 1, alignSelf: 'stretch', marginLeft: '5%' }}>
                        <Text>{myValue.status}</Text>
                    </View>
                    <View style={{ flex: 1, alignSelf: 'stretch', flexDirection: 'row' }}>
                        <View
                        //style={{ height: 33, width: 33, backgroundColor: "#b5b5b5", borderRadius: 50, paddingLeft: 6.5, paddingTop: 6.5 }}

                        >
                            <IconIcon name="check" size={20} color="#900" /></View>
                        <View>
                            <IconIcon name="check" size={20} color="#900" /></View>
                        <View>
                            <IconIcon name="check" size={20} color="#900" /></View>
                        <View>
                            <IconIcon name="check" size={20} color="#900" /></View>
                        <View>
                            <IconIcon name="check" size={20} color="#900" /></View>
                        <View>
                            <IconIcon name="check" size={20} color="#900" /></View>
                    </View>
                </View>
                <View style={styles.lineStyle} />
            </>
        )
        // myIndex=myIndex+1

    });
    let BASICDATA2 = MyTraining.map((myValue, myIndex) => {
        //  console.log('myValue: ' + myValue.city_name)
        return (
            <>
                <View style={{ flexDirection: 'row', backgroundColor: '#ffb4f3', height: 200, width: 200 }}>

                    <View>
                        <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>{myValue.id}</Text>
                        <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>{myValue.topic}</Text>
                        <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>{myValue.fees}</Text>
                        <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>{myValue.status}</Text>
                        <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>{myValue.topic}</Text>
                        <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Age:</Text>
                        <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Status:</Text>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ marginRight: 5 }}>
                                <IconIcon name="check" size={20} color="#900" />
                            </View>
                            <View style={{ marginRight: 5 }}>
                                <IconIcon name="check" size={20} color="#900" />
                            </View>
                            <View style={{ marginRight: 5 }}>
                                <IconIcon name="check" size={20} color="#900" />
                            </View>
                            <View style={{ marginRight: 5 }}>
                                <IconIcon name="check" size={20} color="#900" />
                            </View>
                            <View style={{ marginRight: 5 }}>
                                <IconIcon name="check" size={20} color="#900" />
                            </View>
                            <View style={{ marginRight: 5 }}>
                                <IconIcon name="check" size={20} color="#900" />
                            </View>
                            <View >
                                <IconIcon name="check" size={20} color="#900" />
                            </View>
                        </View>
                    </View>
                </View>
            </>
        )
        // myIndex=myIndex+1

    });
    useEffect(() => {
        if (token) {
            myTrainingTOKEN()
            // findSubCatagory()
        } else {
            AsyncStorage.getItem('UID123', (err, result) => {
                setToken(result);
                // setTimeout(()=>{
                //    
                // }, 3000);
            })
        }
        // submitSubjectSkillPage()
    }, [token])
    Moment.locale('en');

    return (
        <View style={styles.container}>
            <Spinner
                //visibility of Overlay Loading Spinner
                visible={loading}
                color={"black"}
                overlayColor={'rgba(255,255,255, 1)'}
                //Text with the Spinner
                textContent={'Loading...'}
            //Text style of the Spinner Text
            //textStyle={styles.spinnerTextStyle}
            />
            <StatusBar backgroundColor='#961b37' barStyle="light-content" />

            <ScrollView>
                <View style={{ padding: 15, }}>
                    <Text style={{ fontSize: 22, fontWeight: 'bold' }}>My Training</Text>
                </View>
                <View>
                    <View style={{ margin: 10 }}>
                        <Text>Start Date</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={styles.action}>
                            <TextInput
                                placeholder="From"
                                style={styles.textInput}
                                autoCapitalize="none"
                            // onChangeText={(val) => textFirstName(val)}
                            />
                        </View>
                        <View style={styles.action}>
                            <TextInput
                                placeholder="To"
                                style={styles.textInput}
                                autoCapitalize="none"
                            // onChangeText={(val) => textFirstName(val)}
                            />
                        </View>
                    </View>
                    <View style={{ margin: 10 }}>
                        <Text>Status</Text>
                    </View>
                    <Picker />
                    {/* <View style={{ flexDirection: 'column' }}>
                        <RadioButton.Group style={{ flexDirection: 'column' }}>
                            <View style={{ flexDirection: 'row', paddingLeft: 50 }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <RadioButton value="Male" />
                                    <Text style={{ paddingTop: 8 }}>Past Training</Text>
                                </View>
                                <View style={{ flexDirection: 'row' }}>
                                    <RadioButton value="Female" />
                                    <Text style={{ paddingTop: 8 }}>Upcoming Training</Text>
                                </View>
                            </View>
                        </RadioButton.Group>
                    </View> */}
                    <View style={{ flexDirection: 'row', paddingLeft: 50 }}>
                        <View style={{ flexDirection: 'row' }}>
                            <RadioButton
                                value="first"
                                status={checked === 'first' ? 'checked' : 'unchecked'}
                                onPress={() => setChecked('first')}
                            />
                            <Text style={{ paddingTop: 8 }}>Past Training</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <RadioButton
                                value="second"
                                status={checked === 'second' ? 'checked' : 'unchecked'}
                                onPress={() => setChecked('second')}
                            />
                            <Text style={{ paddingTop: 8 }}>Upcoming Training</Text>
                        </View>
                    </View>
                    <TouchableOpacity style={{
                        height: 35, justifyContent: 'center', alignItems: 'center', width: '25%',
                        padding: 1,
                        backgroundColor: '#fffff', borderRadius: 5, marginLeft: 10, marginTop: 10, marginBottom: 5, borderRadius: 5, backgroundColor: '#d73c5e'
                    }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={{ fontSize: 13, color: '#ffffff', fontWeight: 'bold' }}>Search</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                {/* <View style={{ backgroundColor: '#5a287d', height: 40, flexDirection: 'row', justifyContent: 'space-between', textAlign: 'center', paddingLeft: 5, paddingRight: 5 }}>
                    <Text style={{ marginTop: '2.3%', color: '#ffffff' }}>Training Title</Text>
                    <Text style={{ marginTop: '2.3%', color: '#ffffff' }}>Status</Text>
                    <Text style={{ marginTop: '2.3%', color: '#ffffff' }}>Action</Text>
                    <Text style={{ marginTop: '2.3%',color:'#ffffff' }}>Purchase Date</Text>
                </View> */}
                {/* {BASICDATA1} */}
                {/* {BASICDATA2} */}
                {/* <View style={{ flexDirection: 'row' }}>
                    <View style={{ flexDirection: 'row', backgroundColor: '#ffffff', height: 150, width: 250 }}>
                        <View >
                            <Icon name="contact" size={20} color="#9700e3" />
                        </View>
                        <View>
                            <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Training ID:</Text>
                            <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Name:</Text>
                            <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Email:</Text>
                            <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Phone No:</Text>
                            <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Gender:</Text>
                            <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Age:</Text>
                            <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Status:</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', backgroundColor: '#ffffff', height: 150, width: 250 }}>
                        <View >
                            <Icon name="contact" size={20} color="#9700e3" />
                        </View>
                        <View>
                            <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Training ID:</Text>
                            <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Name:</Text>
                            <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Email:</Text>
                            <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Phone No:</Text>
                            <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Gender:</Text>
                            <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Age:</Text>
                            <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Status:</Text>
                        </View>
                    </View>
                </View> */}

                <View style={{
                    // justifyContent: 'space-around',
                    flex: 1,
                    margin: '1%'
                }}>
                    <FlatList
                        data={MyTraining}
                        renderItem={({ item }) => (
                            <View style={{
                                //backgroundColor: '#ffb4f3',
                                height: 180, width: 180,
                                //justifyContent: 'space-around',
                                flex: .5,
                                margin: 4,
                                backgroundColor: '#ffffff', borderWidth: 1,
                                borderRedius: 2,
                                borderColor: '#ddd',
                                borderBottomWidth: 0,
                                shadowColor: '#ff00b8',
                                shadowOffset: { width: 10, height: 20 },
                                shadowOpacity: 0.4,
                                shadowRadius: 2,
                                elevation: 10,
                            }}>
                                <View style={{ marginTop: '5%' }}>
                                    <Text style={{ fontSize: 13, color: '#000000', fontWeight: 'bold', textAlign: 'center' }}>Id:{item.id}</Text>
                                    <Text style={{ fontSize: 13, color: '#000000', fontWeight: 'bold', textAlign: 'center' }}>Topic: {item.topic}</Text>
                                    <Text style={{ fontSize: 13, color: '#000000', fontWeight: 'bold', textAlign: 'center' }}>Fees: {item.fees}</Text>
                                    <Text style={{ fontSize: 13, color: '#000000', fontWeight: 'bold', textAlign: 'center' }}>Status: {item.status}</Text>
                                    <Text style={{ fontSize: 13, color: '#000000', fontWeight: 'bold', textAlign: 'center' }}>Topic: {item.topic}</Text>
                                    <Text style={{ fontSize: 13, color: '#000000', fontWeight: 'bold', textAlign: 'center' }}>Date: {Moment(item.date).format('DD-MM-YY')}</Text>
                                    <Text style={{ fontSize: 13, color: '#000000', fontWeight: 'bold', textAlign: 'center' }}>No Of Sessions: {item.no_of_sessions}</Text>
                                </View>
                                <View style={{ flexDirection: 'row', marginTop: '6%', justifyContent: 'space-evenly' }}>
                                    <View style={{ marginRight: 7 }}>
                                        <TouchableOpacity>
                                            <IconIcon name="eye" size={20} color="#5d5f5e" />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ marginRight: 7 }}>
                                        <TouchableOpacity>
                                            <IconIcon name="copy" size={20} color="#5d5f5e" />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ marginRight: 7 }}>
                                        <TouchableOpacity>
                                            <IconIcon name="pencil" size={20} color="#5d5f5e" />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ marginRight: 7 }}>
                                        <TouchableOpacity>
                                            <IconIconIcon name="clock" size={20} color="#5d5f5e" />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ marginRight: 7 }}>
                                        <TouchableOpacity>
                                            <IconIcon name="file" size={20} color="#5d5f5e" />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ marginRight: 7 }}>
                                        <TouchableOpacity>
                                            <IconIconIcon name="file-alt" size={20} color="#5d5f5e" />
                                        </TouchableOpacity>
                                    </View>
                                    <View >
                                        <TouchableOpacity>
                                            <IconIcon name="check-circle" size={20} color="#5d5f5e" />
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        )}
                        numColumns={2}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </View>

                {/* <View style={{ flexDirection: 'row', backgroundColor: '#ffb4f3', height: 200, width: 200 }}>

                    <View>
                        <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Training ID:</Text>
                        <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Name:</Text>
                        <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Email:</Text>
                        <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Phone No:</Text>
                        <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Gender:</Text>
                        <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Age:</Text>
                        <Text style={{ fontSize: 13, color: '#9700e3', fontWeight: 'bold' }}>Status:</Text>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ marginRight: 5 }}>
                                <IconIcon name="check" size={20} color="#900" />
                            </View>
                            <View style={{ marginRight: 5 }}>
                                <IconIcon name="check" size={20} color="#900" />
                            </View>
                            <View style={{ marginRight: 5 }}>
                                <IconIcon name="check" size={20} color="#900" />
                            </View>
                            <View style={{ marginRight: 5 }}>
                                <IconIcon name="check" size={20} color="#900" />
                            </View>
                            <View style={{ marginRight: 5 }}>
                                <IconIcon name="check" size={20} color="#900" />
                            </View>
                            <View style={{ marginRight: 5 }}>
                                <IconIcon name="check" size={20} color="#900" />
                            </View>
                            <View >
                                <IconIcon name="check" size={20} color="#900" />
                            </View>
                        </View>
                    </View>
                </View> */}
            </ScrollView>
        </View >
    );
};

export default MyTraining;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        //     flexDirection: 'row',
        //     alignItems: 'center',
        //     justifyContent: 'center',
        paddingTop: 5,
        paddingBottom: 15,
    },
    action: {
        flexDirection: 'row',
        marginRight: 10,
        marginLeft: 10,
        borderWidth: 0.6,
        borderRadius: 5,
        borderColor: '#505050',
        //padding: 5
        width: 170
    },
    textInput: {
        //flex: 1,
        //marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        color: '#05375a',
        height: 40
    },
    lineStyle: {
        borderWidth: .3,
        borderColor: 'black',
        marginTop: 3,
        //marginTop: 25
    },
});

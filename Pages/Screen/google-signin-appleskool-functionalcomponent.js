import React, { useEffect} from "react";
import { SafeAreaView, StyleSheet, ScrollView, View, Text, StatusBar, Button, Image,} from 'react-native';
import { Header, LearnMoreLinks, Colors, DebugInstructions, ReloadInstructions,} from 'react-native/Libraries/NewAppScreen';
import { GoogleSignin, GoogleSigninButton, statusCodes } from 'react-native-google-signin';
import axios from 'axios';
//import { AuthContext } from '../../Context/context'
//const { signIn } = React.useContext(AuthContext);
 const LoginControllerfunction= ({ navigation }) => {
  // constructor(props) {
  //   super(props);
  //   this.state = {
  //     pushData: [],
  //     loggedIn: false
  //   }
  // }
  const [data, setData] = React.useState({
      loggedIn: false,
     //  foundgoogle:'',

  });
      useEffect(() => {
        GoogleSignin.configure({
          webClientId: '732849576767-hn0j500r2uf266rnp4ae77i8drt44kql.apps.googleusercontent.com', 
          offlineAccess: true, 
          hostedDomain: '', 
          forceConsentPrompt: true, 
        });     
       }, []);
      
  // componentDidMount() {
  //   GoogleSignin.configure({
  //     webClientId: '732849576767-hn0j500r2uf266rnp4ae77i8drt44kql.apps.googleusercontent.com', 
  //     offlineAccess: true, 
  //     hostedDomain: '', 
  //     forceConsentPrompt: true, 
  //   });
    
  // }

 const _signIn = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      setData({
        ...data,
        userInfo: userInfo,
        loggedIn: true
      });
      //alert(userInfo);
      console.log(JSON.stringify(userInfo));
      axios.defaults.baseURL = 'https://appleskool.com/preview/appleskool_code/api';
      axios.defaults.headers.post['Content-Type'] ='application/json;charset=utf-8';
      axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';
      axios.defaults.headers.post['Access-Control-Allow-Headers'] = '*';
          axios.post('/social-login', {
          "params": {
              "fname":userInfo.user.givenName,
              "lname":userInfo.user.familyName,
              "email":userInfo.user.email,
              "phone_no":"1331131331",
              "provider_id":userInfo.user.id,
              "provider_name":"google",
              "device_id":"100",
              "device_type":"2",
              "interest_skill":{
                  "1":"3"
              }
          }
      }).then(
        (response) => 
      // {
      //   alert(userInfo);
      //   this.state.navigation.navigate('Login')
      // }
      {
        //  alert(JSON.stringify(response.data.error));
       // global.userToken = response.data.userInfo.result.userdata.id;
        if (response.data.error) {
         // alert(JSON.stringify(response.data))
         // navigation.navigate('Registration')
         // signInGoogle(data.found);

        }
         else if(response.data.result){
          // alert(JSON.stringify(response.data.result.userdata))
          // signIn(data.foundgoogle);

        //  // this.state.navigation.navigate('Login')
      navigation.navigate('Registration')
        //   //signIn(this.setState.data.found);

         }
       
      },
        error => {
         // Alert.alert('Enter valid details!! ');
          console.log(error);
        },
      );
      //this.setState({ userInfo: userInfo, loggedIn: true });
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (f.e. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
      } else {
        // some other error happened
      }
    }
  };

  

 const signOut = async () => {
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
      setData({
        ...data,
        user: null,
        loggedIn: false
      });
     // this.setState({ user: null, loggedIn: false }); // Remember to remove the user from your app's state as well
    } catch (error) {
      console.error(error);
    }
  };


    return (
          <ScrollView
            contentInsetAdjustmentBehavior="automatic"
            style={styles.scrollView}>
            {/* <Header /> */}
            {global.HermesInternal == null ? null : (
              <View style={styles.engine}>
                <Text style={styles.footer}>Engine: Hermes</Text>
              </View>
            )}
            <View style={styles.body}>
              <View style={styles.sectionContainer}>
                <GoogleSigninButton
                  style={{ width: '95%', height: 60 }}
                  size={GoogleSigninButton.Size.Wide}
                  color={GoogleSigninButton.Color.Dark}
                  onPress={_signIn}
                  //disabled={isSigninInProgress}
                   />
              </View>
              {/* <View style={styles.buttonContainer}>
                {!loggedIn && <Text>You are currently logged out</Text>}
                {loggedIn && <Button onPress={signOut}
                  title="Signout"
                  color="#841584">
                </Button>}
              </View> */}

              {/* {!data.loggedIn && <LearnMoreLinks />} */}
              {/* {data.loggedIn 
              && 
              <View>
                <View style={styles.listHeader}>
                  <Text>User Info</Text>
                </View>
                <View style={styles.dp}>
                  <Image
                    style={{ width: 100, height: 100 }}
                    source={{ uri: userInfo && userInfo.user && userInfo.user.photo }}
                  />
                </View>
                <View style={styles.detailContainer}>
                  <Text style={styles.title}>Name</Text>
                  <Text style={styles.message}>{userInfo & userInfo.user && userInfo.user.name}</Text>
                </View>
                <View style={styles.detailContainer}>
                  <Text style={styles.title}>Email</Text>
                  <Text style={styles.message}>{userInfo && userInfo.user && userInfo.user.email}</Text>
                </View>
                <View style={styles.detailContainer}>
                  <Text style={styles.title}>ID</Text>
                  <Text style={styles.message}>{userInfo && userInfo.user && userInfo.user.id}</Text>
                </View>
              </View>} */}
            </View>
          </ScrollView>
      
    );
  };
export default LoginControllerfunction;

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  listHeader: {
    backgroundColor: '#eee',
    color: "#222",
    height: 44,
    padding: 12
  },
  detailContainer: {
    paddingHorizontal: 20
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
    paddingTop: 10
  },
  message: {
    fontSize: 14,
    paddingBottom: 15,
    borderBottomColor: "#ccc",
    borderBottomWidth: 1
  },
  dp:{
    marginTop: 32,
    paddingHorizontal: 24,
    flexDirection: 'row',
    justifyContent: 'center'
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 10,
    paddingHorizontal: 24,
    flexDirection: 'row',
    justifyContent: 'center'
  },
  buttonContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
    flexDirection: 'row',
    justifyContent: 'center'
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});